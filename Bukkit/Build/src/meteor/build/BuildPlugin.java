package meteor.build;

import meteor.build.commands.*;
import meteor.cosmetics.UltraCosmetics;
import meteor.util.Rank;
import meteor.util.account.AccountManager;
import meteor.util.chat.ChatManager;
import meteor.util.command.CommandManager;
import meteor.util.constants.F;
import meteor.util.entity.utils.UtilPlayer;
import meteor.util.recharge.Recharge;
import meteor.util.update.UpdateEvent;
import meteor.util.update.UpdateType;
import meteor.util.update.Updater;
import meteor.util.utils.UtilCore;
import meteor.util.utils.UtilWorld;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Creeplays on 15.06.2016.
 */
public class BuildPlugin extends JavaPlugin implements Listener{

    public void writeLog(Object str){
        System.out.println("[BUILD] "+str);
    }

    @Override
    public void onEnable() {

        CommandManager.initialize(this);
        Recharge.initialize(this);
        UtilCore.initialize(this);
        ChatManager.initialize(this);
        Updater.initialize(this);
        AccountManager.initialize(this);
        //UltraCosmetics.initialize(this);

        UtilCore.registerListener(this);

        CommandManager.instance.addCommand(new cmd_author(this));
        CommandManager.instance.addCommand(new cmd_compile(this));
        CommandManager.instance.addCommand(new cmd_goto(this));
        CommandManager.instance.addCommand(new cmd_hub(this));
        CommandManager.instance.addCommand(new cmd_list(this));
        CommandManager.instance.addCommand(new cmd_listloaded(this));
        CommandManager.instance.addCommand(new cmd_name(this));
        CommandManager.instance.addCommand(new cmd_save(this));
        CommandManager.instance.addCommand(new cmd_unloadunused(this));
        CommandManager.instance.addCommand(new cmd_buildstring(this));

        writeLog("Started!");
    }

    @Override
    public void onDisable() {
        writeLog("Stopped!");
    }

    @EventHandler
    public void playerJoin(PlayerJoinEvent event){
        event.setJoinMessage(F.sys("Вошёл",Rank.getRank(event.getPlayer()).getColor()+event.getPlayer().getName()));
        UtilPlayer.sendUsage(event.getPlayer(),"Картодел","Телепортация в главный мир...");

        WorldCreator wc = new WorldCreator(UtilWorld.getSourceWorld("Hub","BuildHub"));
        wc.type(WorldType.FLAT);
        wc.environment(World.Environment.NORMAL);
        wc.generateStructures(false);
        wc.generatorSettings("3;minecraft:air");
        wc.createWorld();
        World w = Bukkit.getServer().getWorld(UtilWorld.getSourceWorld("Hub","BuildHub"));
        event.getPlayer().teleport(new Location(w, 0.5, 52, 0.5));
    }
    @EventHandler
    public void playerLeave(PlayerQuitEvent event){
        event.setQuitMessage(F.sys("Вышел",Rank.getRank(event.getPlayer()).getColor()+event.getPlayer().getName()));

    }
    @EventHandler
    public void updateEvent(UpdateEvent event){
        if(!event.getType().equals(UpdateType.FAST))
            return;
        for (Player player : Bukkit.getServer().getOnlinePlayers()) {
            if(Rank.getRank(player).equals(Rank.ALL))
                player.setGameMode(GameMode.ADVENTURE);
            else
                player.setGameMode(GameMode.CREATIVE);
        }
    }
    @EventHandler
    public void alwaysSun(UpdateEvent event){
        if(!event.getType().equals(UpdateType.SLOW))
            return;
        for (World world : Bukkit.getWorlds()) {
            world.setTime(6000);
            world.setWeatherDuration(0);
            world.setStorm(false);
        }
    }
}
