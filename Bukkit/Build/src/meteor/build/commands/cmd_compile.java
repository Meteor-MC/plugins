package meteor.build.commands;

import com.google.gson.Gson;
import meteor.util.*;
import meteor.util.command.bases.CommandBase;
import meteor.util.constants.F;
import meteor.util.utils.UtilFile;
import meteor.util.utils.UtilWorld;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Creeplays on 15.06.2016.
 */
public class cmd_compile extends CommandBase {

    public cmd_compile(Plugin plugin) {
        super(plugin,"Картодел","/compile <радиус> <имена блоков через запятую>",Rank.ADMIN,new Rank[]{Rank.BUILDER},"compile","c");
    }

    @SuppressWarnings("deprecation")
    @Override
    public void Execute(Player caller, String[] args) {
        if(args==null){
            printUsage(caller);
            return;
        }
        if(args.length!=2) {
            printUsage(caller);
            return;
        }
        World world=caller.getWorld();
        if(world.getName().contains("Build")) {
            answerPlayer(caller,"Нельзя использовать в хабе строителей!");
            return;
        }
        String[] blocksToFind = args[1].split(",");
        Integer radius = Integer.parseInt(args[0], 10);

        answerPlayer(caller,"Радиус: "+ F.elem(radius));
        if (radius > 200){
            answerPlayer(caller,"Радиус не может быть больше 200!");
            return;
        }

        answerPlayer(caller, "Компиляция мира... (Поиск " + blocksToFind.length + " различных блоков)");
        answerPlayer(caller, "Всего " + (256*((radius*2)+1)*((radius*2)+1)) + " блоков.");
        Integer found = 0;
        Integer total = 0;

        MapInfo mi=new MapInfo();

        mi.author= UtilWorld.getAuthorName(world);
        mi.name= UtilWorld.getMapName(world);
        mi.radius=radius;

        WorldRestorer wr = new WorldRestorer(world);

        for (Integer y = 0; y < world.getMaxHeight(); y++) {
            for (Integer x = -radius; x <= radius; x++) {
                for (Integer z = -radius; z <= radius; z++) {
                    Block block = world.getBlockAt(new Location(world, x, y, z));
                    if (!block.getChunk().isLoaded())
                        block.getChunk().load();
                    total++;
                    for (String btf : blocksToFind) {
                        if (btf.equals(block.getType().name().toLowerCase())) {
                            found++;
                            if(!mi.blocks.containsKey(btf))
                                mi.blocks.put(btf,new ArrayList<>());
                            mi.blocks.get(btf).add(SimpleLocation.toSimpleLocation(block.getLocation()));
                        }
                    }
                    if (block.getType().equals(Material.WOOL)) {
                        Byte data = block.getData();
                        Block upBlock = block.getRelative(BlockFace.UP);
                        if (data != 0) {
                            //Colored wool
                            if (upBlock.getType().equals(Material.GOLD_PLATE)) {
                                answerPlayer(caller, "Найден спаун команды " + data);
                                wr.setBlock(upBlock.getLocation(), Material.AIR);
                                wr.setBlock(block.getLocation(), Material.AIR);

                                try {
                                    if(!mi.teamSpawn.containsKey(data))
                                        mi.teamSpawn.put(data,new ArrayList<>());
                                    mi.teamSpawn.get(data).add(SimpleLocation.toSimpleLocation(block.getLocation()));
                                } catch (Exception e) {
                                    answerPlayer(caller, "Необходим всего один спаун!");
                                }
                            }
                        } else {
                            //White Wool
                            wr.setBlock(upBlock.getLocation(), Material.AIR);
                            wr.setBlock(block.getLocation(), Material.AIR);
                        }
                    }

                    if (total % 10000 == 0)
                        answerPlayer(caller, "Просканировано " + total + "/" + (256*((radius*2)+1)*((radius*2)+1)));
                }
            }
        }
        caller.getWorld().save();
        answerPlayer(caller, "Компиляция завершена!");
        answerPlayer(caller, "Всего блоков: " + total);
        answerPlayer(caller, "Из них найдено запрошенных: " + found);
        answerPlayer(caller, "Сохранение мира...");


        Gson gson = new Gson();
        UtilWorld.setWorldString(world, "info", gson.toJson(mi));


        world.save();
        answerPlayer(caller, "Копирование мира...");
        try {
            UtilFile.copy(world.getWorldFolder(), new File(world.getWorldFolder().getPath() , "./../../../Compiled/" + world.getName()));
        }catch (IOException e){
            e.printStackTrace();
            answerPlayer(caller, "Ошибка копирования мира!");
        }

        answerPlayer(caller, "Восстановление мира...");
        wr.restore();
        answerPlayer(caller, "Сохранение мира...");
        world.save();
        answerPlayer(caller, "Удаляем лишние файлы...");
        UtilWorld.clearWorld(world);
        answerPlayer(caller, "Готово!");
    }
}

//export PATH="$PATH:/c/Users/Creeplays/Desktop/MG1.9/Bin/apache-maven-3.3.9/bin:/c/Program Files/Java/jdk1.7.0_05/bin"
