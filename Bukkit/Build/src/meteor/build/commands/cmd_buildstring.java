package meteor.build.commands;

import meteor.util.Rank;
import meteor.util.command.bases.CommandBase;
import meteor.util.utils.UtilBlockText;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * Created by Creeplays on 30.06.2016.
 */
public class cmd_buildstring extends CommandBase {
    public cmd_buildstring(Plugin plugin) {
        super(plugin,"Картодел","/buildstring <текст> <n/e/s/w> <id> <data> <l/c/r>",Rank.ADMIN,new Rank[]{Rank.BUILDER},"buildstring","bs");
    }

    @Override
    public void Execute(Player caller, String[] args) {
        if(args==null){
            printUsage(caller);
            return;
        }
        if(args.length!=5){
            printUsage(caller);
            return;
        }
        BlockFace blockFace=getBlockFaceById(args[1]);
        if(blockFace==null){
            answerPlayer(caller,"Неверное значение стороны света!");
            return;
        }
        UtilBlockText.TextAlign textAlign=getTextAlignById(args[4]);
        if(textAlign==null){
            answerPlayer(caller,"Неверное значение выравнивания текста!");
            return;
        }
        int id;
        byte data;
        try {
            id=Integer.parseInt(args[2]);
            data=(byte)Integer.parseInt(args[3]);
        }catch (Exception e){
            answerPlayer(caller,"Неверное значение id/data!");
            return;
        }
        UtilBlockText.MakeText(args[0],caller.getLocation(),blockFace,id,data,textAlign,false);
    }

    public static UtilBlockText.TextAlign getTextAlignById(String id){
        id=id.toLowerCase();
        switch (id){
            case "c":
                return UtilBlockText.TextAlign.CENTER;
            case "r":
                return UtilBlockText.TextAlign.RIGHT;
            case "l":
                return UtilBlockText.TextAlign.LEFT;
            default:
                return null;
        }
    }
    public static BlockFace getBlockFaceById(String id){
        id=id.toLowerCase();
        switch (id){
            case "n":
                return BlockFace.NORTH;
            case "e":
                return BlockFace.EAST;
            case "s":
                return BlockFace.SOUTH;
            case "w":
                return BlockFace.WEST;
            default:
                return null;
        }
    }
}
