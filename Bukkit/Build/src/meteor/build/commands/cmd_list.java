package meteor.build.commands;

import meteor.util.Rank;
import meteor.util.command.bases.CommandBase;
import meteor.util.utils.UtilWorld;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.*;

/**
 * Created by Creeplays on 15.06.2016.
 */
public class cmd_list extends CommandBase {
    public cmd_list(Plugin plugin) {
        super(plugin,"Картодел","/list",Rank.ALL,"list","l");
    }

    @Override
    public void Execute(Player caller, String[] args) {
        if (args!=null) {
            printUsage(caller);
            return;
        }
        answerPlayer(caller,"Список карт:");

        Map<String,Collection<String>> compiledMaps=UtilWorld.getCompiledMaps();
        Map<String,Collection<String>> sourceMaps=UtilWorld.getSourceMaps();

        for (String gameName : sourceMaps.keySet()) {
            String out="";
            boolean listColor=false;
            out+= ChatColor.DARK_AQUA+gameName+ChatColor.MAGIC+"|"+ChatColor.RESET+" ";
            for (String mapName : sourceMaps.get(gameName)) {
                boolean compiled=true;
                if(!compiledMaps.containsKey(gameName))
                    compiled=false;
                else if(compiledMaps.get(gameName).contains(mapName))
                    compiled=true;
                else
                    compiled=false;
                out+=compiled?(listColor?ChatColor.DARK_GREEN:ChatColor.GREEN):(listColor?ChatColor.DARK_RED:ChatColor.RED);
                listColor=!listColor;
                out+=mapName+" ";
            }
            answerPlayer(caller,out);
        }
    }
}
