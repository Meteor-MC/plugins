package meteor.build.commands;

import meteor.util.Rank;
import meteor.util.utils.UtilWorld;
import meteor.util.command.bases.CommandBase;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * Created by Creeplays on 15.06.2016.
 */
public class cmd_name extends CommandBase {
    public cmd_name(Plugin plugin) {
        super(plugin,"Картодел","/name <название карты>",Rank.ADMIN,new Rank[]{Rank.BUILDER},"name","n");
    }

    @Override
    public void Execute(Player caller, String[] args) {
        if(args==null){
            printUsage(caller);
            return;
        }
        if (args.length != 1) {
            printUsage(caller);
            return;
        }
        World world=caller.getWorld();
        if(world.getName().contains("Build")){
            answerPlayer(caller,"Нельзя использовать в хабе строителей!");
            return;
        }
        UtilWorld.setMapName(world, args[0]);
        answerPlayer(caller,"Название задано!");
    }
}
