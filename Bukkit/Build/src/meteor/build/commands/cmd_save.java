package meteor.build.commands;

import meteor.util.Rank;
import meteor.util.command.bases.CommandBase;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * Created by Creeplays on 15.06.2016.
 */
public class cmd_save extends CommandBase {
    public cmd_save(Plugin plugin) {
        super(plugin,"Картодел","/save",Rank.ADMIN,new Rank[]{Rank.BUILDER},"save","s");
    }

    @Override
    public void Execute(Player caller, String[] args) {
        if(args!=null){
            printUsage(caller);
            return;
        }
        World world=caller.getWorld();
        if(world.getName().contains("Build")){
            answerPlayer(caller,"Нельзя использовать в хабе строителей!");
            return;
        }
        answerPlayer(caller,"Сохранение мира...");
        caller.getWorld().save();
        answerPlayer(caller,"Сохранение завершено!");
    }
}
