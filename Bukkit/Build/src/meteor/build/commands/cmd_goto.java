package meteor.build.commands;

import meteor.util.Rank;
import meteor.util.utils.UtilMap;
import meteor.util.utils.UtilWorld;
import meteor.util.command.bases.CommandBase;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * Created by Creeplays on 15.06.2016.
 */
public class cmd_goto extends CommandBase {

    public cmd_goto(Plugin plugin) {
        super(plugin,"Картодел","/goto <игра> <карта>",Rank.ALL,"goto","go");
    }

    @Override
    public void Execute(Player caller, String[] args) {
        if(args==null){
            printUsage(caller);
            return;
        }
        if (args.length != 2) {
            printUsage(caller);
            return;
        }

        if (args[0].contains("/") || args[0].contains("\\") || args[0].contains(".") || args[1].contains("/") || args[1].contains("\\") || args[1].contains(".")){
            answerPlayer(caller,"Недопустимые символы в команде!");
            return;
        }

        answerPlayer(caller, "Начата загрузка мира!");
        WorldCreator wc = new WorldCreator(UtilWorld.getSourceWorld(args[0],args[1]));
        wc.type(WorldType.FLAT);
        wc.environment(World.Environment.NORMAL);
        wc.generateStructures(false);
        wc.generatorSettings("3;minecraft:air");
        wc.createWorld();
        answerPlayer(caller, "Загрузка завершена, телепортация!");


        World w = Bukkit.getServer().getWorld(UtilWorld.getSourceWorld(args[0],args[1]));
        caller.teleport(new Location(w, 0.5, 52, 0.5));
        w.getChunkAt(w.getBlockAt(0, 51, 0)).load();
        WorldBorder border = w.getWorldBorder();
        border.setSize(400.0);
        border.setCenter(0.0, 0.0);

        if (w.getBlockAt(0, 51, 0).getType().equals(Material.AIR)) {
            answerPlayer(caller, "Это первый заход в этот мир, генерация платформы!");
            for (Integer x = -3; x <= 3; x++) {
                for (Integer z = -3; z <= 3; z++) {
                    UtilMap.ChunkBlockChange(w,x,51,z,1,(byte)0,true);
                }
            }
            UtilMap.ChunkBlockChange(w,0,51,0,4,(byte)0,true);
            UtilWorld.setAuthorName(w, "F6CF");
            UtilWorld.setMapName(w, "WIP");
        }

        answerPlayer(caller, "Информация о карте: ");
        answerPlayer(caller, "" + ChatColor.DARK_GREEN + ChatColor.BOLD + "Название: " + ChatColor.GRAY + ChatColor.BOLD + UtilWorld.getMapName(w));
        answerPlayer(caller, "" + ChatColor.DARK_GREEN + ChatColor.BOLD + "Автор: " + ChatColor.GRAY + ChatColor.BOLD + UtilWorld.getAuthorName(w));
    }
}
