package meteor.build.commands;

import meteor.util.Rank;
import meteor.util.command.bases.CommandBase;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * Created by Creeplays on 15.06.2016.
 */
public class cmd_listloaded extends CommandBase {
    public cmd_listloaded(Plugin plugin) {
        super(plugin,"Картодел","/listloaded",Rank.ADMIN,new Rank[]{Rank.BUILDER},"listloaded","ll");
    }

    @Override
    public void Execute(Player caller, String[] args) {
        if (args!=null) {
            printUsage(caller);
            return;
        }
        answerPlayer(caller,"Список загруженных миров:");
        for (World world : Bukkit.getServer().getWorlds()) {
            if(world.getName().contains("Build"))
                continue;
            answerPlayer(caller,world.getName());
        }
    }
}
