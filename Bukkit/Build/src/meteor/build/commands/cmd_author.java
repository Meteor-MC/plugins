package meteor.build.commands;

import meteor.util.Rank;
import meteor.util.utils.UtilWorld;
import meteor.util.command.bases.CommandBase;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * Created by Creeplays on 15.06.2016.
 */
public class cmd_author extends CommandBase {
    public cmd_author(Plugin plugin){
        super(plugin,"Картодел","/author <имя автора>",Rank.ADMIN,new Rank[]{Rank.BUILDER},"author","a");
    }

    @Override
    public void Execute(Player caller, String[] args) {
        if(args==null){
            printUsage(caller);
            return;
        }
        if(args.length!=1) {
            printUsage(caller);
            return;
        }
        World world=caller.getWorld();
        if(world.getName().contains("Build")) {
            answerPlayer(caller,"Нельзя использовать в хабе строителей!");
            return;
        }
        UtilWorld.setAuthorName(world, args[0]);
        answerPlayer(caller, "Автор задан!");
    }
}
