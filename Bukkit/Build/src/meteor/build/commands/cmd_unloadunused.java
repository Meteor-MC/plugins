package meteor.build.commands;

import meteor.util.Rank;
import meteor.util.command.bases.CommandBase;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * Created by Creeplays on 15.06.2016.
 */
public class cmd_unloadunused extends CommandBase {
    public cmd_unloadunused(Plugin plugin) {
        super(plugin,"Картодел","/unloadunused",Rank.ADMIN,new Rank[]{Rank.BUILDER},"unloadunused","uu");
    }

    @Override
    public void Execute(Player caller, String[] args) {
        if(args!=null){
            printUsage(caller);
            return;
        }
        answerPlayer(caller,"Начинаем отгрузку миров...");
        for (World world : Bukkit.getServer().getWorlds()) {
            if(world.getPlayers().isEmpty()) {
                if(world.getName().contains("Build"))
                    continue;
                answerPlayer(caller, "Отгружаем мир " + world.getName());
                world.save();
                Bukkit.getServer().unloadWorld(world,false);
            }
        }
    }
}
