package meteor.build.commands;

import meteor.util.Rank;
import meteor.util.command.bases.CommandBase;
import meteor.util.utils.UtilWorld;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * Created by Creeplays on 15.06.2016.
 */
public class cmd_hub extends CommandBase {
    public cmd_hub(Plugin plugin){
        super(plugin,"Картодел","/hub",Rank.ALL,"hub","spawn");
    }

    @Override
    public void Execute(Player caller, String[] args) {
        if (args!=null) {
            printUsage(caller);
            return;
        }
        answerPlayer(caller, "Телепортация в главный мир...");

        WorldCreator wc = new WorldCreator(UtilWorld.getSourceWorld("Hub","BuildHub"));
        wc.type(WorldType.FLAT);
        wc.environment(World.Environment.NORMAL);
        wc.generateStructures(false);
        wc.generatorSettings("3;minecraft:air");
        wc.createWorld();
        World w = Bukkit.getServer().getWorld(UtilWorld.getSourceWorld("Hub","BuildHub"));
        caller.teleport(new Location(w, 0.5, 52, 0.5));
    }
}
