package net.citizensnpcs.api.ai.flocking;

import net.citizensnpcs.api.npc.NPC;
import org.bukkit.util.Vector;

import java.util.Collection;

public interface FlockBehavior {
    Vector getVector(NPC npc, Collection<NPC> nearby);
}
