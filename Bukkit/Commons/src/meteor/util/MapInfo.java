package meteor.util;

import com.google.gson.annotations.SerializedName;
import meteor.util.enums.EnumColor;

import java.util.ArrayList;
import java.util.HashMap;

public class MapInfo {
    @SerializedName("author")
    public String author="";
    @SerializedName("name")
    public String name="";
    @SerializedName("radius")
    public Integer radius=0;
    @SerializedName("teamNames")
    public HashMap<Byte,String> teamNames=new HashMap<>();
    @SerializedName("teamSpawn")
    public HashMap<Byte,ArrayList<SimpleLocation>> teamSpawn=new HashMap<>();
    @SerializedName("blocks")
    public HashMap<String,ArrayList<SimpleLocation>> blocks=new HashMap<>();

    public EnumColor[] getColors(){
        Byte[] colors= teamSpawn.keySet().toArray(new Byte[teamSpawn.keySet().size()]);
        EnumColor[] output=new EnumColor[colors.length];
        for(Integer i=0;i<colors.length;i++)
            output[i]=EnumColor.getByDyeData(colors[i]);
        return output;
    }
}
