package meteor.util.items;// Written by Creeplays on 21.06.2016.

import meteor.util.utils.UtilMath;

public enum ItemClass {
    COMMON("ОБЫЧНЫЙ",0,40),
    RARE("РЕДКИЙ",41,60),
    EPIC("ЭПИЧЕСКИЙ",61,90),
    LEGENDARY("ЛЕГЕНДАРНЫЙ",91,100);

    Integer from;
    Integer to;
    String prefix;

    ItemClass(String prefix, Integer from, Integer to){
        this.prefix=prefix;
        this.from=from;
        this.to=to;
    }

    static ItemClass getRandomClass(){
        Integer r=UtilMath.r(100);
        for (ItemClass itemClass : ItemClass.values()) {
            if(itemClass.from<=r&&itemClass.to>=r)
                return itemClass;
        }
        return COMMON;
    }
}
