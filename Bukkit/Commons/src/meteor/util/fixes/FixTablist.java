package meteor.util.fixes;// Written by Creeplays on 20.06.2016.

import meteor.util.MiniPlugin;
import meteor.util.entity.utils.UtilPlayer;
import meteor.util.utils.UtilServer;
import net.minecraft.server.v1_10_R1.PacketPlayOutPlayerInfo;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;

public class FixTablist extends MiniPlugin
{
    public static FixTablist instance;
    public static void initialize(Plugin plugin){
        instance=new FixTablist(plugin);
    }

    private FixTablist(Plugin plugin)
    {
        super("Tablist Fix", plugin);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onJoin(PlayerJoinEvent event) {
        final Player player = event.getPlayer();
        runSyncLater(new Runnable()
        {
            @Override
            public void run()
            {
                if (!player.isOnline())
                    return;

                PacketPlayOutPlayerInfo packetPlayOutPlayerInfo=new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.UPDATE_DISPLAY_NAME);

                UtilPlayer.sendPacket(player, packetPlayOutPlayerInfo);

                for (Player other : UtilServer.getPlayers())
                {
                    if (other.equals(player) || !other.canSee(player))
                        continue;

                    UtilPlayer.sendPacket(other, packetPlayOutPlayerInfo);
                }
            }
        }, 20L);
    }

    @Override
    public void enable() {

    }
    @Override
    public void disable() {

    }
    @Override
    public void addCommands() {

    }
}
