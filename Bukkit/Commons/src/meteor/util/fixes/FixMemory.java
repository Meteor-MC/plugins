package meteor.util.fixes;// Written by Creeplays on 20.06.2016.

import meteor.util.MiniPlugin;
import meteor.util.update.UpdateEvent;
import meteor.util.update.UpdateType;
import net.minecraft.server.v1_10_R1.CraftingManager;
import net.minecraft.server.v1_10_R1.IInventory;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_10_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.plugin.Plugin;

import java.util.Iterator;

public class FixMemory extends MiniPlugin
{
    public static FixMemory instance;
    public static void initialize(Plugin plugin){
        instance=new FixMemory(plugin);
    }

    private FixMemory(Plugin plugin)
    {
        super("Фикс утечки памяти", plugin);
    }

    @EventHandler
    public void fixInventoryLeaks(UpdateEvent event){
        if (event.getType() != UpdateType.SLOW)
            return;

        for (World world : Bukkit.getWorlds())
        {
            for (Object tileEntity : ((CraftWorld)world).getHandle().tileEntityList)
            {
                if (tileEntity instanceof IInventory)
                {
                    Iterator<HumanEntity> entityIterator = ((IInventory)tileEntity).getViewers().iterator();

                    while (entityIterator.hasNext())
                    {
                        HumanEntity entity = entityIterator.next();

                        if (entity instanceof CraftPlayer && !((CraftPlayer)entity).isOnline())
                        {
                            entityIterator.remove();
                        }
                    }
                }
            }
        }

        CraftingManager.getInstance().lastCraftView = null;
        CraftingManager.getInstance().lastRecipe = null;
    }

    @Override
    public void enable() {

    }
    @Override
    public void disable() {

    }
    @Override
    public void addCommands() {

    }
}