package meteor.util.enums;

/**
 * Created by Creeplays on 24.06.2016.
 */
public enum EnumResourcePack {
	//Store all on minepack.net
	DEFAULT(
			"https://download.nodecdn.net/containers/nodecraft/minepack/bc985b224d37fac8488ed231c64fa047.zip",
			"09d6d856fd3c311757dc7e33ac33dbb6e7aab3c1",
			"Empty"
	),
	SKYWARS(
			"https://download.nodecdn.net/containers/nodecraft/minepack/9ffd60d083bc1c19ef75e9df2998bb75.zip",
			"9a17a77408fbfe286bd0ea2d06c8b7a6013f18ea",
			"Got from CubeCraft"
	);


	private String url;
	private String hash;

	EnumResourcePack(String url,String hash,String comment) {
		this.url=url;
		this.hash=hash;
	}

	public String getHash() {
		return hash;
	}
	public String getUrl() {
		return url;
	}
}
