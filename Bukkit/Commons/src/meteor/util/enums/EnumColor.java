package meteor.util.enums;// Written by Creeplays on 21.06.2016.

import net.md_5.bungee.api.ChatColor;
import org.bukkit.DyeColor;

public enum EnumColor {
    BLACK(DyeColor.BLACK, ChatColor.BLACK,0,"", "Чёрные"),
    DARK_GREY(DyeColor.GRAY, ChatColor.DARK_GRAY,1,"", "Тёмно-серые"),
    LIGHT_GREY(DyeColor.SILVER, ChatColor.GRAY,2,"shadowblack","Светло-серые"),
    WHITE(DyeColor.WHITE, ChatColor.WHITE,3,"cy1337","Белые"),
    DARK_BLUE(DyeColor.BLUE, ChatColor.DARK_BLUE,4,"fancypjs","Тёмно-синие"),
    BROWN(DyeColor.BROWN, ChatColor.BLUE,5,"","Бурые"),
    DARK_CYAN(DyeColor.CYAN, ChatColor.DARK_AQUA,6,"","Тёмно-бирюзовые"),
    LIGHT_CYAN(DyeColor.LIGHT_BLUE, ChatColor.AQUA,7,"","Светло-бирюзовые"),
    DARK_GREEN(DyeColor.GREEN, ChatColor.DARK_GREEN,8,"","Тёмно-зелёные"),
    LIGHT_GREEN(DyeColor.LIME, ChatColor.GREEN,9,"","Светло-зелёные"),
    YELLOW(DyeColor.YELLOW, ChatColor.YELLOW,10,"jarjenny","Жёлтые"),
    ORANGE(DyeColor.ORANGE, ChatColor.GOLD,11,"wulfric17","Оранжевые"),
    DARK_RED(DyeColor.RED, ChatColor.DARK_RED,12,"","Тёмно-красные"),
    LIGHT_RED(DyeColor.PINK, ChatColor.RED,13,"","Светло-красные"),
    DARK_PURPLE(DyeColor.PURPLE, ChatColor.DARK_PURPLE,14,"","Темно-пурпурные"),
    LIGHT_PURPLE(DyeColor.MAGENTA, ChatColor.LIGHT_PURPLE, 15,"","Светло-пурпурные");

    private DyeColor dyeColor;
    private ChatColor chatColor;
    private Integer id;
    private String teamName;
    private String skinName;

    EnumColor(DyeColor dyeColor,ChatColor chatColor,Integer id,String skinName,String teamName){
        this.dyeColor=dyeColor;
        this.chatColor=chatColor;
        this.id=id;
        this.teamName=teamName;
        this.skinName=skinName;
    }

    @Deprecated
    public String getSkinName() {
        return skinName;
    }
    public String getTeamName(){
        return this.teamName;
    }
    public String getChatColor(){
        return this.chatColor.toString();
    }
    public DyeColor getDyeColor(){
        return this.dyeColor;
    }
    public Integer getId() {
        return id;
    }

    public static EnumColor getById(Integer id){
        for (EnumColor enumColor : EnumColor.values()) {
            if(enumColor.id.equals(id))
                return enumColor;
        }
        return BLACK;
    }

    @SuppressWarnings("deprecation")
    public static EnumColor getByDyeData(byte id){
        for (EnumColor enumColor : EnumColor.values()) {
            if(enumColor.dyeColor.getData()==id)
                return enumColor;
        }
        return BLACK;
    }
























}
