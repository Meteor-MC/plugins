package meteor.util.events;

import org.bukkit.entity.Player;

/**
 * Created by Creeplays on 25.06.2016.
 */
public class PlayerPreloadEvent extends BaseEvent {
    private Player player;

    public PlayerPreloadEvent(Player player){
        this.player=player;
    }

    public Player getPlayer() {
        return player;
    }
}
