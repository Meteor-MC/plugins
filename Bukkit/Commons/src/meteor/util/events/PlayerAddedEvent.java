package meteor.util.events;

import org.bukkit.entity.Player;

/**
 * Created by Creeplays on 25.06.2016.
 */
public class PlayerAddedEvent extends BaseEvent {
    private Player player;

    public PlayerAddedEvent(Player player){
        this.player=player;
    }

    public Player getPlayer() {
        return player;
    }
}
