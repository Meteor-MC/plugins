package meteor.util.events;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

/**
 * Created by Creeplays on 25.06.2016.
 */
public class AnyInventoryEvent extends BaseEvent {
    private Player player;
    private Inventory inventory;

    public AnyInventoryEvent(Player player, Inventory inventory){
        this.player=player;
        this.inventory=inventory;
    }

    public Player getPlayer() {
        return player;
    }
    public Inventory getInventory() {
        return inventory;
    }
}
