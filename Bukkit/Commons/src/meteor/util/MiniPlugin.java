package meteor.util;// Written by Creeplays on 20.06.2016.

import meteor.util.command.CommandManager;
import meteor.util.command.bases.ICommand;
import meteor.util.constants.F;
import meteor.util.utils.UtilTime;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.scheduler.BukkitScheduler;

public abstract class MiniPlugin implements Listener
{
    public static MiniPlugin instance;

    private String pluginName = "Default";
    private Plugin plugin;
    private boolean registered=false;
    private WrappedHashMap<String, ICommand> commandHashMap;

    //@EventHandler
    public void onEnable(){
        long epoch = System.currentTimeMillis();
        log("Инициализация...");
        enable();
        addCommands();
        log("Включён через " + UtilTime.convertString(System.currentTimeMillis() - epoch, 1, UtilTime.TimeUnit.FIT) + ".");
    }
    //@EventHandler
    public void onDisable(){
        disable();

        log("Disabled.");
    }



    protected MiniPlugin(String moduleName, Plugin plugin) {

        this.pluginName = moduleName;
        this.plugin = plugin;
        this.commandHashMap = new WrappedHashMap<>();

        onEnable();
        registerEvents(this);
        registered=true;
    }

    public PluginManager getPluginManager()
    {
        return plugin.getServer().getPluginManager();
    }
    public BukkitScheduler getScheduler()
    {
        return plugin.getServer().getScheduler();
    }
    public Plugin getPlugin()
    {
        return plugin;
    }
    public void registerEvents(Listener listener){
        plugin.getServer().getPluginManager().registerEvents(listener, plugin);
    }


    public void registerSelf()
    {
        if(registered)
            throw new Error("Already registered!");
        registerEvents(this);
        registered=true;
    }
    public void deregisterSelf()
    {
        HandlerList.unregisterAll(this);
        registered=false;
    }

    public void callEvent(Event event){
        plugin.getServer().getPluginManager().callEvent(event);
    }

    public final String getName()
    {
        return pluginName;
    }
    public final void addCommand(ICommand command)
    {
        CommandManager.instance.addCommand(command);
    }
    public final void removeCommand(ICommand command)
    {
        CommandManager.instance.removeCommand(command);
    }
    public void log(String message)
    {
        System.out.println(F.main(pluginName, message));
    }
    public void runAsync(Runnable runnable){
        plugin.getServer().getScheduler().runTaskAsynchronously(plugin, runnable);
    }
    public void runSync(Runnable runnable)
    {
        plugin.getServer().getScheduler().runTask(plugin, runnable);
    }
    public void runSyncLater(Runnable runnable, long delay){
        plugin.getServer().getScheduler().runTaskLater(plugin, runnable, delay);
    }

    //public static abstract void initialize();  //Should save instance in instance field. Should be static
    public abstract void enable();               //OnEnable hook
    public abstract void disable();              //OnDisable hook
    public abstract void addCommands();          //...
}
