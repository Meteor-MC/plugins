package meteor.util.entity.metadata;

import net.minecraft.server.v1_10_R1.DataWatcher;
import org.bukkit.entity.Entity;

import static meteor.util.entity.utils.UtilEntity.getHandle;

public class MyDataWatcher {
    private final DataWatcher handle;
    private final Object lock = new Object();

    public MyDataWatcher(Entity entity){
        this.handle=new DataWatcher(getHandle(entity));
    }
    public MyDataWatcher(net.minecraft.server.v1_10_R1.Entity entity){
        this.handle=new DataWatcher(entity);
    }
    public MyDataWatcher(DataWatcher dataWatcher){
        this.handle=dataWatcher;
    }

    public <T> T get(MetadataKey<T> key) {
        Object obj = handle.get(key.getHandle());
        return key.cast(obj);
    }
    public <T> void set(MetadataKey<T> key, T value) {
        handle.set(key.getHandle(), key.cast(value));
    }
    public <T> void register(MetadataKey<T> key, T initialValue) {
        handle.register(key.getHandle(), key.cast(initialValue));
    }
}