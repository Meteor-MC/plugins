package meteor.util.entity.metadata;

/**
 * Created by Creeplays on 28.06.2016.
 */

import net.minecraft.server.v1_10_R1.DataWatcherObject;
import net.minecraft.server.v1_10_R1.DataWatcherRegistry;
import net.minecraft.server.v1_10_R1.DataWatcherSerializer;

public class MetadataKey<T> {
    private final DataWatcherObject<T> handle;
    private final MetadataType type;

    public MetadataKey(int index, MetadataType type) {
        handle = new DataWatcherObject<>(index, (DataWatcherSerializer<T>) DataWatcherRegistry.a(type.getIndex()));
        this.type = type;
    }
    public T cast(Object obj) {
        type.checkCast(obj);
        return (T) obj;
    }

    public int getIndex() {
        return handle.a();
    }
    public DataWatcherObject<T> getHandle(){
        return handle;
    }
    public MetadataType getType(){
        return type;
    }

    @Override
    public boolean equals(Object o) {
        return this == o
                || o != null
                && getClass() == o.getClass()
                && getHandle().equals(((MetadataKey) o).getHandle())
                && getType() == ((MetadataKey) o).getType();
    }
    @Override
    public int hashCode() {
        int result = getHandle().hashCode();
        result = 31 * result + getType().hashCode();
        return result;
    }
}
