package meteor.util.entity.utils;// Written by Creeplays on 17.06.2016.

import meteor.util.constants.C;
import meteor.util.constants.F;
import meteor.util.enums.EnumResourcePack;
import meteor.util.utils.UtilReflect;
import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_10_R1.*;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.lang.reflect.Field;

public class UtilPlayer {
    public static void sendCommandOutput(Player player, String name, String out) {
        String[] answerArr=out.split("\n");
        for (String s : answerArr) {
            player.sendMessage(F.main(name,s));
        }
    }
    public static void sendUsage(Player player,String name,String usage){
        usage=C.cGold+"Неправильный ввод! Использование: "+C.cWhite+usage;
        String[] answerArr=usage.split("\n");
        for (String s : answerArr) {
            player.sendMessage(F.main(C.cAqua+name,C.cWhite+s));
        }
    }

    public static void message(Player player, String msg){
        player.sendMessage(msg);
    }
    public static void messageAboveHotbar(Player player,String msg){
        CraftPlayer craftplayer = (CraftPlayer) player;
        IChatBaseComponent cbc = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + ChatColor.translateAlternateColorCodes('&', msg.replaceAll("%player%", player.getName())) + "\"}");
        PacketPlayOutChat packet = new PacketPlayOutChat(cbc, (byte) 2);
        craftplayer.getHandle().playerConnection.sendPacket(packet);
    }

    public static void sendPacket(Player player, Packet... packets) {
        PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;

        for (Packet packet : packets)
        {
            connection.sendPacket(packet);
        }
    }

    public static void teleport(Player player,Location location){
        player.teleport(location, PlayerTeleportEvent.TeleportCause.PLUGIN);
    }
    public static void teleport(Player player,double x,double y, double z){
        teleport(player, new Location(player.getWorld(),x,y,z));
    }

    public static void sendResourcePack(Player player,EnumResourcePack resourcePack){
        player.setResourcePack(resourcePack.getUrl());
    }

    public static void giveOrDrop(Player player,ItemStack itemStack){
        if (player.getInventory().firstEmpty() != -1) {
            player.getInventory().addItem(itemStack);
        } else {
            if (itemStack.getType() != Material.AIR) {
                player.getWorld().dropItem(player.getLocation(), itemStack);
            }
        }
    }

    public static ItemStack getSkull(String playername){
        ItemStack skull = new ItemStack(Material.SKULL, 1, (byte) SkullType.PLAYER.ordinal());
        SkullMeta meta = (SkullMeta) skull.getItemMeta();
        meta.setOwner(playername);
        skull.setItemMeta(meta);
        return skull;
    }
    public static void fakeBorder(Player player,int centerX,int centerZ,int size, int warningDistance){
        EntityPlayer nmsPlayer = ((CraftPlayer) player).getHandle();
        WorldBorder playerWorldBorder = nmsPlayer.world.getWorldBorder();
        PacketPlayOutWorldBorder worldBorder = new PacketPlayOutWorldBorder(playerWorldBorder, PacketPlayOutWorldBorder.EnumWorldBorderAction.SET_WARNING_BLOCKS);

        //1.10.2
        //From constructor
        //
        //this.a = var2;
        //this.c = var1.getCenterX();
        //this.d = var1.getCenterZ();
        //this.f = var1.getSize();
        //this.e = var1.j();
        //this.g = var1.i();
        //this.b = var1.l();
        //this.i = var1.getWarningDistance();
        //this.h = var1.getWarningTime();

        UtilReflect.setInt(worldBorder,"c",centerX);
        UtilReflect.setInt(worldBorder,"d",centerZ);
        UtilReflect.setInt(worldBorder,"f",size);
        UtilReflect.setInt(worldBorder,"i",warningDistance);
        nmsPlayer.playerConnection.sendPacket(worldBorder);
    }

    public static ItemStack getSkull(Player player){
        return getSkull(player.getName());
    }
}
