package meteor.util.entity.utils;// Written by Creeplays on 17.06.2016.

import com.google.common.collect.Sets;
import meteor.cosmetics.cosmetics.pets.Pet;
import meteor.cosmetics.cosmetics.pets.PlayerFollower;
import meteor.cosmetics.cosmetics.treasurechests.ChestType;
import meteor.cosmetics.cosmetics.treasurechests.TreasureChestDesign;
import meteor.cosmetics.util.PacketSender;
import meteor.cosmetics.util.Particles;
import meteor.cosmetics.util.UtilParticles;
import meteor.util.entity.metadata.MetadataKey;
import meteor.util.entity.metadata.MetadataType;
import meteor.util.entity.metadata.MyDataWatcher;
import meteor.util.entity.patchfinders.CustomPathFinderGoalPanic;
import meteor.util.utils.UtilCore;
import meteor.util.utils.UtilMath;
import meteor.util.utils.UtilRandom;
import net.minecraft.server.v1_10_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_10_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_10_R1.entity.*;
import org.bukkit.craftbukkit.v1_10_R1.inventory.CraftInventory;
import org.bukkit.craftbukkit.v1_10_R1.inventory.CraftItemStack;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wither;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;

import java.lang.reflect.Field;
import java.util.*;

public class UtilEntity {

    private static Map<Player, List<EntityArmorStand>> fakeArmorStandsMap = new HashMap<>();
    private static Map<Player, List<org.bukkit.entity.Entity>> cooldownJumpMap = new HashMap<>();

    public static void setData(org.bukkit.entity.Entity entity, Integer index, Object value, MetadataType type){
        new MyDataWatcher(entity).set(new MetadataKey<>(index,type),value);
    }
    public static Object getData(org.bukkit.entity.Entity e, Integer index, MetadataType metadataType){
        return new MyDataWatcher(e).get(new MetadataKey<>(index,metadataType));
    }
    public static PlayerFollower newPlayerFollower(Pet pet, Player player) {
        return new PlayerFollower(pet,player);
    }

    public static net.minecraft.server.v1_10_R1.Entity getHandle(org.bukkit.entity.Entity entity){
        return ((CraftEntity)entity).getHandle();
    }
    public static DataWatcher getDataWatcher(net.minecraft.server.v1_10_R1.Entity entity){
        return new DataWatcher(entity);
    }
    public static DataWatcher getDataWatcher(org.bukkit.entity.Entity entity){
        return getDataWatcher(getHandle(entity));
    }

    public static void setPassenger(org.bukkit.entity.Entity vehicle, org.bukkit.entity.Entity passenger) {
        vehicle.setPassenger(passenger);
    }

    public static void resetWitherSize(Wither wither) {
        ((CraftWither) wither).getHandle().g(600);
    }

    public static void setHorseSpeed(Horse horse, double speed) {
        ((CraftHorse) horse).getHandle().getAttributeInstance(GenericAttributes.MOVEMENT_SPEED).setValue(speed);
    }

    public static void removePathFinders(org.bukkit.entity.Entity entity) {
        net.minecraft.server.v1_10_R1.Entity nmsEntity = ((CraftEntity) entity).getHandle();
        try {
            Field bField = PathfinderGoalSelector.class.getDeclaredField("b");
            bField.setAccessible(true);
            Field cField = PathfinderGoalSelector.class.getDeclaredField("c");
            cField.setAccessible(true);
            bField.set(((EntityInsentient) nmsEntity).goalSelector, Sets.newLinkedHashSet());
            bField.set(((EntityInsentient) nmsEntity).targetSelector, Sets.newLinkedHashSet());
            cField.set(((EntityInsentient) nmsEntity).goalSelector, Sets.newLinkedHashSet());
            cField.set(((EntityInsentient) nmsEntity).targetSelector, Sets.newLinkedHashSet());
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public static void startHandChanging(final Plugin plugin){
        //setData(p,5,false,MetadataType.BOOLEAN); //No gravity
        //setData(p,10,20,MetadataType.VAR_INT);   //Arrow count
        //setData(p,14,(byte)0,MetadataType.BYTE);   //Main hand
        Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
            boolean hand=false;
            @Override
            public void run() {
                for (Player player : plugin.getServer().getOnlinePlayers()) {
                    setData(player,14,hand?0:1,MetadataType.BYTE);
                    hand=!hand;
                }
            }
        },40,40);
    }

    public static void sendBlizzard(final Player player, Location loc, boolean affectPlayers, Vector v) {
        if (!fakeArmorStandsMap.containsKey(player))
            fakeArmorStandsMap.put(player, new ArrayList<EntityArmorStand>());
        if (!cooldownJumpMap.containsKey(player))
            cooldownJumpMap.put(player, new ArrayList<org.bukkit.entity.Entity>());

        final List<EntityArmorStand> fakeArmorStands = fakeArmorStandsMap.get(player);
        final List<org.bukkit.entity.Entity> cooldownJump = cooldownJumpMap.get(player);

        final EntityArmorStand as = new EntityArmorStand(((CraftWorld) player.getWorld()).getHandle());
        as.setInvisible(true);
        as.setSmall(true);
        as.setNoGravity(true);
        as.setArms(true);
        as.setHeadPose(new Vector3f((float) (UtilRandom.nextInt(360)),
                (float) (UtilRandom.nextInt(360)),
                (float) (UtilRandom.nextInt(360))));
        as.setLocation(loc.getX() + UtilMath.randomDouble(-1.5, 1.5), loc.getY() + UtilMath.randomDouble(0, .5) - 0.75, loc.getZ() + UtilMath.randomDouble(-1.5, 1.5), 0, 0);
        fakeArmorStands.add(as);
        for (Player players : player.getWorld().getPlayers()) {
            PacketSender.send(players, new PacketPlayOutSpawnEntityLiving(as));
            PacketSender.send(players, new PacketPlayOutEntityEquipment(as.getId(), EnumItemSlot.HEAD, CraftItemStack.asNMSCopy(new org.bukkit.inventory.ItemStack(org.bukkit.Material.PACKED_ICE))));
        }
        UtilParticles.display(Particles.CLOUD, loc.clone().add(UtilMath.randomDouble(-1.5, 1.5), UtilMath.randomDouble(0, .5) - 0.75, UtilMath.randomDouble(-1.5, 1.5)), 2, 0.4f);
        Bukkit.getScheduler().runTaskLater(UtilCore.getPlugin(), new Runnable() {
            @Override
            public void run() {
                for (Player pl : player.getWorld().getPlayers())
                    PacketSender.send(pl, new PacketPlayOutEntityDestroy(as.getId()));
                fakeArmorStands.remove(as);
            }
        }, 20);
        if (affectPlayers)
            for (final org.bukkit.entity.Entity ent : as.getBukkitEntity().getNearbyEntities(0.5, 0.5, 0.5)) {
                if (!cooldownJump.contains(ent) && ent != player) {
                    UtilMath.applyVelocity(ent, new Vector(0, 1, 0).add(v));
                    cooldownJump.add(ent);
                    Bukkit.getScheduler().runTaskLater(UtilCore.getPlugin(), new Runnable() {
                        @Override
                        public void run() {
                            cooldownJump.remove(ent);
                        }
                    }, 20);
                }
            }
    }

    public static void clearBlizzard(Player player) {
        if (!fakeArmorStandsMap.containsKey(player)) return;

        for (EntityArmorStand as : fakeArmorStandsMap.get(player))
            for (Player pl : player.getWorld().getPlayers())
                PacketSender.send(pl, new PacketPlayOutEntityDestroy(as.getId()));
        fakeArmorStandsMap.remove(player);
        cooldownJumpMap.remove(player);
    }

    public static void clearPathfinders(org.bukkit.entity.Entity entity) {
        net.minecraft.server.v1_10_R1.Entity nmsEntity = ((CraftEntity) entity).getHandle();
        try {
            Field bField = PathfinderGoalSelector.class.getDeclaredField("b");
            bField.setAccessible(true);
            Field cField = PathfinderGoalSelector.class.getDeclaredField("c");
            cField.setAccessible(true);
            bField.set(((EntityInsentient) nmsEntity).goalSelector, Sets.newLinkedHashSet());
            bField.set(((EntityInsentient) nmsEntity).targetSelector, Sets.newLinkedHashSet());
            cField.set(((EntityInsentient) nmsEntity).goalSelector, Sets.newLinkedHashSet());
            cField.set(((EntityInsentient) nmsEntity).targetSelector, Sets.newLinkedHashSet());
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public static void makePanic(org.bukkit.entity.Entity entity) {
        EntityInsentient insentient = (EntityInsentient) ((CraftEntity) entity).getHandle();
        insentient.goalSelector.a(3, new CustomPathFinderGoalPanic((EntityCreature) insentient, 0.4d));
    }

    public static void sendDestroyPacket(Player player, org.bukkit.entity.Entity entity) {
        PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(((CraftEntity) entity).getHandle().getId());
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
    }

    public static void move(Creature creature, Location loc) {
        EntityCreature ec = ((CraftCreature) creature).getHandle();
        ec.P = 1;

        if (loc == null) return;

        ec.getNavigation().a(loc.getX(), loc.getY(), loc.getZ(), (1.0D + 2.0D * 0.5d) * 1.0D);
    }

    public static void moveDragon(Player player, Vector vector, org.bukkit.entity.Entity entity) {
        EntityEnderDragon ec = ((CraftEnderDragon) entity).getHandle();

        ec.hurtTicks = -1;

        ec.getBukkitEntity().setVelocity(vector);

        ec.pitch = player.getLocation().getPitch();
        ec.yaw = player.getLocation().getYaw() - 180;

        Vector v = ec.getBukkitEntity().getLocation().getDirection();
        Vector v1 = ec.getBukkitEntity().getLocation().getDirection().multiply(-1);
        ec.move(v1.getX(), v.getY(), v1.getZ());
    }

    public static void setClimb(org.bukkit.entity.Entity entity) {
        ((CraftEntity) entity).getHandle().P = 1;
    }

    public static void moveShip(Player player, org.bukkit.entity.Entity entity, Vector vector) {
        EntityBoat ec = ((CraftBoat) entity).getHandle();

        ec.getBukkitEntity().setVelocity(vector);

        ec.pitch = player.getLocation().getPitch();
        ec.yaw = player.getLocation().getYaw() - 180;

        ec.move(1, 0, 0);
    }

    public static void playChestAnimation(org.bukkit.block.Block b, boolean open, boolean isEnderChest) {
        Location location = b.getLocation();
        WorldServer world = ((CraftWorld) location.getWorld()).getHandle();
        BlockPosition position = new BlockPosition(location.getX(), location.getY(), location.getZ());
        if (isEnderChest) {
            TileEntityEnderChest tileChest = (TileEntityEnderChest) world.getTileEntity(position);
            world.playBlockAction(position, tileChest.getBlock(), 1, open ? 1 : 0);
        } else {
            TileEntityChest tileChest = (TileEntityChest) world.getTileEntity(position);
            world.playBlockAction(position, tileChest.getBlock(), 1, open ? 1 : 0);
        }
    }
    public static void playChestAnimation(org.bukkit.block.Block b, boolean open, TreasureChestDesign design){
        playChestAnimation(b,open,design.getChestType() == ChestType.ENDER);
    }

    public static org.bukkit.entity.Entity spawnItem(org.bukkit.inventory.ItemStack itemStack, Location blockLocation) {
        EntityItem ei = new EntityItem(
                ((CraftWorld) blockLocation.clone().add(0.5D, 1.2D, 0.5D).getWorld()).getHandle(),
                blockLocation.clone().add(0.5D, 1.2D, 0.5D).getX(),
                blockLocation.clone().add(0.5D, 1.2D, 0.5D).getY(),
                blockLocation.clone().add(0.5D, 1.2D, 0.5D).getZ(),
                CraftItemStack.asNMSCopy(itemStack)) {

            private boolean a(EntityItem entityitem) {
                return false;
            }
        };
        ei.getBukkitEntity().setVelocity(new Vector(0.0D, 0.25D, 0.0D));
        ei.pickupDelay = 2147483647;
        ei.getBukkitEntity().setCustomName(UUID.randomUUID().toString());
        ei.pickupDelay = 20;

        ((CraftWorld) blockLocation.clone().add(0.5D, 1.2D, 0.5D).getWorld()).getHandle().addEntity(ei);

        return ei.getBukkitEntity();
    }

    public static boolean isSameInventory(Inventory first, Inventory second) {
        return ((CraftInventory) first).getInventory().equals(((CraftInventory) second).getInventory());
    }

    public static void follow(org.bukkit.entity.Entity toFollow, org.bukkit.entity.Entity follower) {
        net.minecraft.server.v1_10_R1.Entity pett = ((CraftEntity) follower).getHandle();
        ((EntityInsentient) pett).getNavigation().a(2);
        Object petf = ((CraftEntity) follower).getHandle();
        Location targetLocation = toFollow.getLocation();
        PathEntity path;
        path = ((EntityInsentient) petf).getNavigation().a(targetLocation.getX() + 1, targetLocation.getY(), targetLocation.getZ() + 1);
        if (path != null) {
            ((EntityInsentient) petf).getNavigation().a(path, 1.05D);
            ((EntityInsentient) petf).getNavigation().a(1.05D);
        }
    }

    public static void chickenFall(Player player) {
        EntityPlayer entityPlayer = ((CraftPlayer) player).getHandle();
        if (!entityPlayer.onGround && entityPlayer.motY < 0.0D) {
            Vector v = player.getVelocity();
            player.setVelocity(v);
            entityPlayer.motY *= 0.85;
        }
    }

    public static void sendTeleportPacket(Player player, org.bukkit.entity.Entity entity) {
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutEntityTeleport(((CraftEntity) entity).getHandle()));
    }

}
