package meteor.util;// Written by Creeplays on 18.06.2016.

public interface Callback<T> {
    void call(T obj);
}
