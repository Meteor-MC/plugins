package meteor.util;// Written by Creeplays on 20.06.2016.

import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class WrappedHashMap<KeyType, ValueType>
{
    private HashMap<KeyType, ValueType> wrap = new HashMap<>();

    public boolean containsKey(KeyType key)
    {
        return wrap.containsKey(key);
    }

    public boolean containsValue(ValueType key)
    {
        return wrap.containsValue(key);
    }

    public Set<Entry<KeyType, ValueType>> entrySet()
    {
        return wrap.entrySet();
    }

    public Set<KeyType> keySet()
    {
        return wrap.keySet();
    }

    public Collection<ValueType> values()
    {
        return wrap.values();
    }

    public ValueType get(KeyType key)
    {
        return wrap.get(key);
    }

    public ValueType remove(KeyType key)
    {
        return wrap.remove(key);
    }

    public ValueType put(KeyType key, ValueType value)
    {
        return wrap.put(key, value);
    }

    public void clear()
    {
        wrap.clear();
    }

    public int size()
    {
        return wrap.size();
    }

    public boolean isEmpty()
    {
        return wrap.isEmpty();
    }
}

