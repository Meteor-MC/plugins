package meteor.util.account.commands;

import meteor.util.Rank;
import meteor.util.account.AccountManager;
import meteor.util.command.bases.CommandBase;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * Created by Creeplays on 30.06.2016.
 */
public class cmd_setrank extends CommandBase{

    public cmd_setrank(Plugin plugin) {
        super(plugin, "Ранг", "/setrank <ник> <ранг>", Rank.ADMIN, "setrank","sr");
    }

    @Override
    public void Execute(Player caller, String[] args) {
        if(args==null){
            printUsage(caller);
            return;
        }
        if(args.length!=2){
            printUsage(caller);
            return;
        }

        Player player= Bukkit.getPlayer(args[0]);
        if(player==null){
            answerPlayer(caller,"Игрок не найден!");
            return;
        }
        Rank rank;
        try {
            rank = Rank.valueOf(args[1]);
        }catch (Exception e){
            answerPlayer(caller,"Ранк не найден!");
            return;
        }

        AccountManager.instance.setRank(player.getName(),rank);
        answerPlayer(caller,"Ранк назначен!");
    }
}
