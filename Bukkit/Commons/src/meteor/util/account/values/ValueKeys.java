package meteor.util.account.values;

import meteor.util.account.valueTypes.AccountIntValue;

/**
 * Created by Creeplays on 30.06.2016.
 */
public class ValueKeys extends AccountIntValue {
    public ValueKeys() {
        super("keys", 3);
    }
}
