package meteor.util.account.values;

import meteor.util.account.valueTypes.AccountDoubleIntValue;

/**
 * Created by Creeplays on 01.07.2016.
 */
public class ValueGadgetInventory extends AccountDoubleIntValue {
    //ToDO: preloadlist
    public ValueGadgetInventory() {
        super("gadgetInventory", 0,new String[]{
                "batblaster",
                "chickenator",
                "colorbomb",
                "discoball",
                "etherealpearl",
                "fleshhook",
                "melonthrower",
                "blizzardblaster",
                "portalgun",
                "explosivesheep",
                "paintballgun",
                "thorhammer",
                "antigravity",
                "smashdown",
                "rocket",
                "blackhole",
                "tsunami",
                "tnt",
                "fungun",
                "parachute",
                "quakegun"
        });
    }
}
