package meteor.util.account.values;

import meteor.util.Rank;
import meteor.util.account.valueTypes.AccountValue;

/**
 * Created by Creeplays on 30.06.2016.
 */
public class ValueRank extends AccountValue<Rank> {
    public ValueRank() {
        super("rank", Rank.ALL);
    }

    @Override
    public String toString(Rank value) {
        return value.name();
    }

    @Override
    public Rank fromString(String value) {
        return Rank.valueOf(value);
    }
}
