package meteor.util.account.valueTypes;

import meteor.util.account.valueTypes.AccountDoubleValue;

/**
 * Created by Creeplays on 01.07.2016.
 */
public class AccountDoubleIntValue extends AccountDoubleValue<Integer> {
    public AccountDoubleIntValue(String name, int defaultValue, String[] preloadList) {
        super(name, defaultValue, preloadList);
    }

    @Override
    public String toString(Integer value) {
        return value.toString();
    }

    @Override
    public Integer fromString(String value) {
        return Integer.parseInt(value);
    }

    public void addFor(String owner, String what, int count){
        setFor(owner,what,getFor(owner,what)+count);
    }

    public void removeFor(String owner, String what, int count){
        setFor(owner,what,getFor(owner,what)-count);
    }
}
