package meteor.util.account.valueTypes;

import com.mongodb.client.result.UpdateResult;
import meteor.util.account.events.AccountPreloadEvent;
import meteor.util.database.Database;
import meteor.util.utils.UtilCore;
import org.bson.Document;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright 2016 Meteor-MC
 * All rights reserved
 * Source code for internal use only
 * Created for ${PACKAGE_NAME}
 * Created by Creeplays on 02/07/16.
 */

public abstract class AccountValue<T> implements Listener {

    private final T defaultValue;
    protected String name;
    private Map<String,T> cache;

    public AccountValue(String name,T defaultValue){
        this.name=name;
        this.defaultValue=defaultValue;
        this.cache=new HashMap<>();
        UtilCore.registerListener(this);
    }

    public T getCache(String owner){
        return cache.get(owner);
    }
    public void setCache(String owner,T value){
        cache.put(owner,value);
    }

    public T getFor(String owner){
        T cached=getCache(owner);
        if(cached!=null)
            return cached;
        else{
            //System.err.println("!!!NOT CACHED!!! = "+owner+"."+name);
            return defaultValue;
        }
    }

    public void setFor(String owner, T value){
        setCache(owner,value);
        Document find=new Document("name",owner);
        Database.getAccountCollection().updateOne(find,new Document("$set",new Document(name,toString(value))),(doc,throwable)->{
            if(throwable!=null)
                throwable.printStackTrace();
        });
    }
    public void setDefault(String owner){
        Document find=new Document("name",owner);
        Database.getAccountCollection().find(find).first((Document document, Throwable throwable)->{
            if(document!=null)
                Database.getAccountCollection().updateOne(find,find.append(name,defaultValue),(UpdateResult result, Throwable throwable1)->{
                    if(throwable!=null){
                        throwable.printStackTrace();
                    }
                });
        });
    }

    public T getDefaultValue() {
        return defaultValue;
    }
    public String getName() {
        return name;
    }

    public abstract String toString(T value);
    public abstract T fromString(String value);

    @EventHandler
    public void accountPreload(AccountPreloadEvent event){
        preload(event.getDocument());
    }

    /**
     * Preloads document into cache
     * @param document document to cache
     */
    public void preload(Document document){
        if(document==null){
            return;
        }
        Object data;
        try {
            data = document.get(name);
        }catch (Exception e){
            data=null;
            e.printStackTrace();
        }
        if(data==null){
            System.out.println("Player does not have data to preload "+name);
            setDefault(document.getString("name"));
            System.out.println(document);
            return;
        }else{
            System.out.println("Got player data "+name);
        }
        T decodedData=fromString((String) data);
        setCache(document.getString("name"),decodedData);
    }
}
