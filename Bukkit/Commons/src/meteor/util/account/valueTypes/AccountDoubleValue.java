package meteor.util.account.valueTypes;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Creeplays on 30.06.2016.
 */
public abstract class AccountDoubleValue<T> extends AccountValue<T> {
    private String Oname;
    private Map<String,T> Ocache;
    private String[] preloadList;

    public AccountDoubleValue(String Oname, T defaultValue, String[] preloadList) {
        super(Oname, defaultValue);
        this.Oname=Oname;
        this.Ocache=new HashMap<>();
        this.preloadList=preloadList;
    }

    @Override
    public T getCache(String owner){
        System.out.println(owner+"."+name);
        return Ocache.get(owner+"."+name);
    }

    @Override
    public void setCache(String owner,T value){
        Ocache.put(owner+"."+name,value);
    }

    public T getFor(String owner,String what) {
        name=Oname+"."+what;
        T res= getFor(owner);
        name=Oname;
        return res;
    }

    public void setFor(String owner,String what,T to) {
        name=Oname+"."+what;
        setFor(owner,to);
        name=Oname;
    }
}
