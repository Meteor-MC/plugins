package meteor.util.account.valueTypes;

import meteor.util.account.valueTypes.AccountValue;

/**
 * Created by Creeplays on 30.06.2016.
 */
public abstract class AccountIntValue extends AccountValue<Integer> {
    public AccountIntValue(String name, int defaultValue) {
        super(name, defaultValue);
    }

    @Override
    public String toString(Integer value) {
        return value.toString();
    }

    public void addFor(String owner, int count){
        setFor(owner,getFor(owner)+count);
    }

    public void removeFor(String owner, int count){
        setFor(owner,getFor(owner)-count);
    }

    @Override
    public Integer fromString(String value) {
        return Integer.parseInt(value);
    }
}
