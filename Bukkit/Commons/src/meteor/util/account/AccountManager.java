package meteor.util.account;

import meteor.util.MiniPlugin;
import meteor.util.Rank;
import meteor.util.account.commands.cmd_setrank;
import meteor.util.account.events.AccountPreloadEvent;
import meteor.util.account.values.ValueCoins;
import meteor.util.account.values.ValueGadgetInventory;
import meteor.util.account.values.ValueKeys;
import meteor.util.account.values.ValueRank;
import meteor.util.database.Database;
import org.bson.Document;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.plugin.Plugin;

/**
 * Created by Creeplays on 29.06.2016.
 */
public class AccountManager extends MiniPlugin{

    public static AccountManager instance;
    public static void initialize(Plugin plugin){
        instance=new AccountManager(plugin);
    }

    private AccountManager(Plugin plugin) {
        super("Менеджер аккаунтов", plugin);
    }

    @Override
    public void disable() {

    }
    @Override
    public void addCommands() {
        addCommand(new cmd_setrank(getPlugin()));
    }
    @Override
    public void enable() {
        valueRank=new ValueRank();
        valueCoins=new ValueCoins();
        valueKeys=new ValueKeys();
        valueGadgetInventory=new ValueGadgetInventory();
    }

    @EventHandler
    public void onJoin(PlayerLoginEvent event){
        System.out.println("onJoin AccountManager");
        Database.getAccountCollection().find(new Document("name",event.getPlayer().getName())).first((document, t) -> {
            System.out.println("found");
            System.out.println(document);
            System.out.println(t);
            if(t!=null) {
                t.printStackTrace();
                String o="";
                for (StackTraceElement stackTraceElement : t.getStackTrace()) {
                    o+=stackTraceElement.getClassName()+":"+stackTraceElement.getLineNumber()+"\n";
                }
                event.disallow(PlayerLoginEvent.Result.KICK_OTHER,"Внутренняя ошибка: "+t.getMessage()+"\n"+o);
                return;
            }
            if(document==null){
                System.out.println("onJoin document==null");
                final Document finDocument=new Document("name",event.getPlayer().getName());
                Database.getAccountCollection().insertOne(finDocument,(doc, throwable)->{
                    callEvent(new AccountPreloadEvent(finDocument));
                });
            }
            else
                callEvent(new AccountPreloadEvent(document));
        });
    }

    //Rank
    private ValueRank valueRank;
    public Rank getRank(String owner){
        return valueRank.getFor(owner);
    }
    public void setRank(String owner,Rank rank){
        valueRank.setFor(owner,rank);
    }

    //Coin
    private ValueCoins valueCoins;
    public int getCoins(String owner){
        return valueCoins.getFor(owner);
    }
    public void addCoins(String owner, int count){
        valueCoins.addFor(owner,count);
    }
    public void removeCoins(String owner, int count){
        valueCoins.addFor(owner,-count);
    }

    //Keys
    private ValueKeys valueKeys;
    public int getKeys(String owner){
        return valueKeys.getFor(owner);
    }
    public void addKeys(String owner,int count){
        valueKeys.addFor(owner,count);
    }
    public void removeKeys(String owner,int count){
        valueKeys.removeFor(owner,count);
    }

    //Gadget inventory
    private ValueGadgetInventory valueGadgetInventory;
    public int getGadgets(String owner,String gadget){
        return valueGadgetInventory.getFor(owner,gadget);
    }
    public void addGadgets(String owner,String gadget, int count){
        valueGadgetInventory.addFor(owner,gadget,count);
    }
    public void removeGadgets(String owner,String gadget, int count){
        valueGadgetInventory.removeFor(owner,gadget,count);
    }
}
