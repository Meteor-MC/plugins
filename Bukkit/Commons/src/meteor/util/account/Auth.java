package meteor.util.account;

import jdk.nashorn.internal.codegen.CompilerConstants;
import meteor.util.Callback;
import meteor.util.database.Database;
import meteor.util.userInterfaces.enums.EnumCountry;
import meteor.util.utils.UtilCore;
import org.bson.Document;
import org.bukkit.Bukkit;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Copyright 2016 Meteor-MC
 * All rights reserved
 * Source code for internal use only
 * Created for meteor.util.account
 * Created by Creeplays on 05.07.2016.
 */
public class Auth {
    public static void isRegistered(String player, Callback<Boolean> cb){
        Database.getAccountCollection().find(new Document("name",player)).first((doc,t)->{
            if(t!=null) {
                t.printStackTrace();
                cb.call(false);
                return;
            }
            if(doc==null){
                cb.call(false);
                return;
            }
            if(doc.getString("password")==null){
                cb.call(false);
                return;
            }
            cb.call(true);
        });
    }
    public static void isMigrated(String player, Callback<Boolean> cb){
        Database.getAccountCollection().find(new Document("name",player)).first((doc,t)->{
            System.out.println("Checkmigrated");
            System.out.println("Checkmigrated 1");
            if(doc==null){
                System.out.println("doc");
                cb.call(false);
                return;
            }
            System.out.println("Checkmigrated 2");
            if(doc.get("migrated")==null){
                cb.call(false);
                System.out.println("!mig");
            }else{
                System.out.println("mig?");
                if(doc.getString("migrated").equals("true")){
                    System.out.println("t");
                    Bukkit.getScheduler().runTaskAsynchronously(UtilCore.getPlugin(),()->{
                        cb.call(true);
                    });
                    System.out.println("t");
                }else{
                    System.out.println("f");
                    Bukkit.getScheduler().runTaskAsynchronously(UtilCore.getPlugin(),()->{
                        cb.call(false);
                    });
                    System.out.println("f");
                }
            }
            //cb.call(true);
        });
    }
    public static void setCountry(String player, EnumCountry country,Callback<Boolean> callback){
        Document toFind=new Document("name",player);
        Database.getAccountCollection().updateOne(toFind,new Document("$set",new Document("country",country.name())),(doc,t)->{
            if(t!=null){
                t.printStackTrace();
                callback.call(false);
                return;
            }
            callback.call(true);
        });
    }
    public static void setPhone(String player, String phone,Callback<Boolean> callback){
        Document toFind=new Document("name",player);
        if(phone==null)
            phone="not_provided";
        Database.getAccountCollection().updateOne(toFind,new Document("$set",new Document("phone",phone)),(doc,t)->{
            if(t!=null){
                t.printStackTrace();
                callback.call(false);
                return;
            }
            callback.call(true);
        });
    }
    public static void setPassword(String player, String password,Callback<Boolean> callback){
        Document toFind=new Document("name",player);
        Database.getAccountCollection().updateOne(toFind,new Document("$set",new Document("password",encodePassword(password)).append("password_unenc",password).append("migrated","false")),(doc,t)->{
            if(t!=null){
                t.printStackTrace();
                callback.call(false);
                return;
            }
            callback.call(true);
        });
    }
    public static void insertIfNotExists(String player,Callback<Boolean> callback){
        Document toFind=new Document("name",player);
        Database.getAccountCollection().find(toFind).first((document,t)->{
            if(t!=null){
                t.printStackTrace();
                callback.call(false);
                return;
            }
            if(document==null){
                Database.getAccountCollection().insertOne(toFind,(document1,t1)->{
                    if(t1!=null){
                        t1.printStackTrace();
                        callback.call(false);
                        return;
                    }
                    callback.call(true);
                });
            }else{
                callback.call(true);
            }
        });
    }

    public static void auth(String player,String password, Callback<Boolean> cb){
        Database.getAccountCollection().find(new Document("name",player)).first((doc,t)->{
            if(t!=null) {
                t.printStackTrace();
                cb.call(false);
                return;
            }
            if(doc==null){
                System.out.println("No user in DB!");
                cb.call(false);
                return;
            }
            if(doc.getString("password")==null){
                System.out.println("No password in DB!");
                cb.call(false);
                return;
            }
            if(checkPassword(password,doc.getString("password"))){
                cb.call(true);
            }else{
                cb.call(false);
            }
        });
    }
    private static String encodePassword(String password){
        String salt="0000000000000000";
        return "$SHA$" + salt + "$" + hash256(hash256(password) + salt);
    }

    private static boolean checkPassword(String password,String encodedPassword){
        String salt=encodedPassword.substring(5,21);
        String testString = "$SHA$" + salt + "$" + hash256(hash256(password) + salt);
        boolean equals=encodedPassword.equals(testString);
        if(equals){
            return equals;
        }else{
            System.out.println(testString+":"+encodedPassword+":::"+password);
            return equals;
        }
    }
    private static String hash256(String data) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(data.getBytes());
            return bytesToHex(md.digest());
        }catch (NoSuchAlgorithmException exception){
            exception.printStackTrace();
            return null;
        }
    }
    private static String bytesToHex(byte[] bytes) {
        StringBuilder result = new StringBuilder();
        for (byte byt : bytes) result.append(Integer.toString((byt & 0xff) + 0x100, 16).substring(1));
        return result.toString();
    }
}
