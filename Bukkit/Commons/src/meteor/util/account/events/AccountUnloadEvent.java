package meteor.util.account.events;// Written by Creeplays on 20.06.2016.

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class AccountUnloadEvent extends Event
{
    private static final HandlerList handlers = new HandlerList();

    private String name;

    public AccountUnloadEvent(String name)
    {
        this.name = name;
    }

    public String GetName()
    {
        return name;
    }

    public HandlerList getHandlers()
    {
        return handlers;
    }

    public static HandlerList getHandlerList()
    {
        return handlers;
    }
}
