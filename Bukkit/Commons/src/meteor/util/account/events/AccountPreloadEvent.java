package meteor.util.account.events;

import meteor.util.events.BaseEvent;
import org.bson.Document;

/**
 * Created by Creeplays on 25.06.2016.
 */
public class AccountPreloadEvent extends BaseEvent {
    private Document document;

    public AccountPreloadEvent(Document document){
        this.document=document;
    }

    public Document getDocument(){
        return document;
    }
}
