package meteor.util.update;// Written by Creeplays on 20.06.2016.

import org.bukkit.plugin.Plugin;

public class Updater implements Runnable
{
    public static Updater instance;
    public static void initialize(Plugin plugin){
        instance=new Updater(plugin);
    }

    private Plugin plugin;
    private Updater(Plugin plugin){
        this.plugin = plugin;
        this.plugin.getServer().getScheduler().scheduleSyncRepeatingTask(this.plugin, this, 0L, 1L);
    }

    @Override
    public void run(){
        for (UpdateType updateType : UpdateType.values())
        {
            if (updateType.Elapsed())
            {
                plugin.getServer().getPluginManager().callEvent(new UpdateEvent(updateType));
            }
        }
    }
}