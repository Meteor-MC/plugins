package meteor.util.recharge;// Written by Creeplays on 20.06.2016.

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class RechargeEvent extends Event
{
    private static final HandlerList handlers = new HandlerList();

    private Player player;
    private String ability;
    private long recharge;

    public RechargeEvent(Player player, String ability, long recharge)
    {
        this.player = player;
        this.ability = ability;
        this.recharge = recharge;
    }

    public HandlerList getHandlers()
    {
        return handlers;
    }

    public static HandlerList getHandlerList()
    {
        return handlers;
    }

    public Player GetPlayer()
    {
        return player;
    }

    public String GetAbility()
    {
        return ability;
    }

    public long GetRecharge()
    {
        return recharge;
    }

    public void SetRecharge(long time)
    {
        recharge = time;
    }
}

