package meteor.util.recharge;// Written by Creeplays on 20.06.2016.

import meteor.util.MiniPlugin;
import meteor.util.WrappedHashMap;
import meteor.util.account.events.AccountUnloadEvent;
import meteor.util.constants.F;
import meteor.util.entity.utils.UtilPlayer;
import meteor.util.update.UpdateEvent;
import meteor.util.update.UpdateType;
import meteor.util.utils.UtilServer;
import meteor.util.utils.UtilTime;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.plugin.Plugin;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.stream.Collectors;

public class Recharge extends MiniPlugin
{
    public static Recharge instance;
    public static void initialize(Plugin plugin){
        instance=new Recharge(plugin);
    }

    private HashSet<String> informSet = new HashSet<>();
    private WrappedHashMap<String, WrappedHashMap<String, RechargeData>> recharge = new WrappedHashMap<String, WrappedHashMap<String, RechargeData>>();

    private Recharge(Plugin plugin)
    {
        super("Перезарядка", plugin);
    }

    @EventHandler
    public void playerDeath(PlayerDeathEvent event)
    {
        get(event.getEntity().getName()).clear();
    }
    @EventHandler
    public void clearPlayer(AccountUnloadEvent event)
    {
        recharge.remove(event.GetName());
    }
    @EventHandler
    public void update(UpdateEvent event){
        if (event.getType() != UpdateType.TICK)
            return;

        recharge();
    }

    public WrappedHashMap<String, RechargeData> get(String name){
        if (!recharge.containsKey(name))
            recharge.put(name, new WrappedHashMap<String, RechargeData>());

        return recharge.get(name);
    }
    public WrappedHashMap<String, RechargeData> get(Player player)
    {
        return get(player.getName());
    }

    public void recharge() {
        for (Player cur : UtilServer.getPlayers())
        {
            LinkedList<String> rechargeList = new LinkedList<String>();

            //Check Recharged
            rechargeList.addAll(get(cur).keySet().stream().filter(ability -> get(cur).get(ability).Update()).collect(Collectors.toList()));

            //Inform Recharge
            for (String ability : rechargeList)
            {
                get(cur).remove(ability);

                //Event
                RechargedEvent rechargedEvent = new RechargedEvent(cur, ability);
                UtilServer.getServer().getPluginManager().callEvent(rechargedEvent);

                if (informSet.contains(ability))
                    UtilPlayer.message(cur, F.main("Перезарядка", "Ты можешь использовать " + F.skill(ability) + "."));
            }
        }
    }
    public void recharge(Player player, String ability)
    {
        get(player).remove(ability);
    }

    public boolean use(Player player, String ability, long recharge, boolean inform, boolean attachItem) {
        return use(player, ability, ability, recharge, inform, attachItem);
    }
    public boolean use(Player player, String ability, String abilityFull, long recharge, boolean inform, boolean attachItem) {
        return use(player, ability, abilityFull, recharge, inform, attachItem, false);
    }
    public boolean use(Player player, String ability, long recharge, boolean inform, boolean attachItem, boolean attachDurability) {
        return use(player, ability, ability, recharge, inform, attachItem, attachDurability);
    }
    public boolean use(Player player, String ability, String abilityFull, long recharge, boolean inform, boolean attachItem, boolean attachDurability) {
        if (recharge == 0)
            return true;

        //Ensure Expirey
        recharge();

        //Lodge Recharge Msg
        if (inform && recharge > 1000)
            informSet.add(ability);

        //Recharging
        if (get(player).containsKey(ability))
        {
            if (inform)
            {
                UtilPlayer.message(player, F.main("Перезарядка", "Ты не можешь использовать " + F.skill(abilityFull) + " " +
                        F.time(UtilTime.convertString((get(player).get(ability).GetRemaining()), 1, UtilTime.TimeUnit.FIT)) + "."));
            }

            return false;
        }

        //Insert
        useRecharge(player, ability, recharge, attachItem, attachDurability);

        return true;
    }

    public void useForce(Player player, String ability, long recharge)
    {
        useForce(player, ability, recharge, false);
    }
    public void useForce(Player player, String ability, long recharge, boolean attachItem) {
        useRecharge(player, ability, recharge, attachItem, false);
    }

    public boolean usable(Player player, String ability)
    {
        return usable(player, ability, false);
    }
    public boolean usable(Player player, String ability, boolean inform) {
        if (!get(player).containsKey(ability))
            return true;

        if (get(player).get(ability).GetRemaining() <= 0)
        {
            return true;
        }
        else
        {
            if (inform)
                UtilPlayer.message(player, F.main("Перезарядка", "Ты не можешь испольлзовать " + F.skill(ability) + " " +
                        F.time(UtilTime.convertString((get(player).get(ability).GetRemaining()), 1, UtilTime.TimeUnit.FIT)) + "."));

            return false;
        }
    }

    public void useRecharge(Player player, String ability, long recharge, boolean attachItem, boolean attachDurability) {
        //Event
        RechargeEvent rechargeEvent = new RechargeEvent(player, ability, recharge);
        UtilServer.getServer().getPluginManager().callEvent(rechargeEvent);

        get(player).put(ability, new RechargeData(this, player, ability, player.getInventory().getItemInMainHand(),
                rechargeEvent.GetRecharge(), attachItem, attachDurability));
    }

    public void setDisplayForce(Player player, String ability, boolean displayForce) {
        if (!recharge.containsKey(player.getName()))
            return;

        if (!recharge.get(player.getName()).containsKey(ability))
            return;

        recharge.get(player.getName()).get(ability).DisplayForce = displayForce;
    }

    public void setCountdown(Player player, String ability, boolean countdown){
        if (!recharge.containsKey(player.getName()))
            return;

        if (!recharge.get(player.getName()).containsKey(ability))
            return;

        recharge.get(player.getName()).get(ability).Countdown = countdown;
    }

    public void reset(Player player)
    {
        recharge.put(player.getName(), new WrappedHashMap<>());
    }
    public void reset(Player player, String stringContains) {
        WrappedHashMap<String, RechargeData> data = recharge.get(player.getName());

        if (data == null)
            return;

        Iterator<String> rechargeIterator = data.keySet().iterator();

        while (rechargeIterator.hasNext()) {
            String key = rechargeIterator.next();

            if (key.toLowerCase().contains(stringContains.toLowerCase())) {
                rechargeIterator.remove();
            }
        }
    }

    @Override
    public void enable() {

    }
    @Override
    public void disable() {

    }
    @Override
    public void addCommands() {

    }
}
