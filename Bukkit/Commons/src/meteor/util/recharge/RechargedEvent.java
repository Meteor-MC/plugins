package meteor.util.recharge;// Written by Creeplays on 20.06.2016.

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class RechargedEvent extends Event
{
    private static final HandlerList handlers = new HandlerList();

    private Player player;
    private String ability;

    public RechargedEvent(Player player, String ability)
    {
        this.player = player;
        this.ability = ability;
    }

    public HandlerList getHandlers()
    {
        return handlers;
    }

    public static HandlerList getHandlerList()
    {
        return handlers;
    }

    public Player GetPlayer()
    {
        return player;
    }

    public String GetAbility()
    {
        return ability;
    }
}
