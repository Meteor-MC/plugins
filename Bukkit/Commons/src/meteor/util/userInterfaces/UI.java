package meteor.util.userInterfaces;// Written by Creeplays on 16.06.2016.

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

public class UI implements Listener {

    private String name;
    private int size;
    private String postFix;
    private OptionClickEventHandler handler;
    private Plugin plugin;

    private String[] optionNames;
    private ItemStack[] optionIcons;
    private String[] privateNames;

    private boolean personal;
    private Player player;


    /**
     * For public UI
     * @param name
     * @param size
     * @param handler
     * @param plugin
     */
    public UI(String name, int size, OptionClickEventHandler handler, Plugin plugin) {
        this.name = name;
        this.size = size;
        this.postFix="";
        this.handler = handler;
        this.plugin = plugin;
        this.optionNames = new String[size];
        this.optionIcons = new ItemStack[size];
        this.privateNames=new String[size];
        this.personal=false;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * For personal ui
     * @param name
     * @param size
     * @param handler
     * @param plugin
     * @param player
     */
    public UI(String name, int size, OptionClickEventHandler handler, Plugin plugin, Player player) {
        this.name = name;
        this.size = size;
        this.postFix="";
        this.handler = handler;
        this.plugin = plugin;
        this.optionNames = new String[size];
        this.optionIcons = new ItemStack[size];
        this.privateNames=new String[size];
        this.personal=true;
        this.player=player;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * Set string that will show after ui name
     * @param name
     */
    public void setPost(String name){
        this.postFix=name;
    }

    /**
     * add button
     * @param position
     * @param icon
     */
    public void setOption(int position, ItemStack icon) {
        optionNames[position] = name;
        optionIcons[position] = icon;
    }

    /**
     *
     * @param position
     * @param icon
     * @param privateName will return in handler
     */
    public void setOption(int position, ItemStack icon,String privateName) {
        optionNames[position] = name;
        optionIcons[position] = icon;
        privateNames[position]=privateName;
    }

    /**
     * Open ui for a player
     * @param player
     */
    public void open(Player player) {
        if(personal&&!player.equals(this.player))
            player=this.player;
        Inventory inventory = Bukkit.createInventory(player, size, name+" "+postFix);
        for (int i = 0; i < optionIcons.length; i++) {
            if (optionIcons[i] != null) {
                inventory.setItem(i, optionIcons[i]);
            }
        }
        player.openInventory(inventory);
    }
    public void open(){
        if(!personal)
            throw new Error("Calling ui as personal!");
        open(player);
    }

    public void update(){
        if(personal)
            if(this.player.getOpenInventory().getTitle().startsWith(this.name))
                open();
        else Bukkit.getOnlinePlayers().stream().filter(
                    player -> player.getOpenInventory().getTitle().startsWith(this.name))
                    .forEach(this::open);
    }

    public void destroy() {
        HandlerList.unregisterAll(this);
        handler = null;
        plugin = null;
        optionNames = null;
        optionIcons = null;
    }

    @EventHandler(priority=EventPriority.MONITOR)
    void onInventoryClick(InventoryClickEvent event) {
        if(personal&&!event.getWhoClicked().equals(player))
            return;
        if (event.getInventory().getTitle().startsWith(name)) {
            event.setCancelled(true);
            ClickType clickType=event.getClick();
            int slot = event.getRawSlot();
            if (slot >= 0 && slot < size && optionNames[slot] != null) {
                OptionClickEvent e = new OptionClickEvent((Player)event.getWhoClicked(), slot, optionNames[slot],privateNames[slot],clickType);
                handler.onOptionClick(e);
                if (e.willClose()) {
                    final Player p = (Player)event.getWhoClicked();
                    p.closeInventory();
                }
                if (e.willDestroy()) {
                    destroy();
                }
            }
        }
    }

    public interface OptionClickEventHandler {
        void onOptionClick(OptionClickEvent event);
    }

    public class OptionClickEvent {
        private Player player;
        private int position;
        private String name;
        private boolean close;
        private boolean destroy;
        private ClickType clickType;
        private String privateName;

        public OptionClickEvent(Player player, int position, String name,String privateName,ClickType clickType) {
            this.player = player;
            this.position = position;
            this.name = name;
            this.close = true;
            this.destroy = false;
            this.clickType=clickType;
            this.privateName=privateName;
        }

        public Player getPlayer() {
            return player;
        }

        public String getPrivateName() {
            return privateName!=null?privateName:"";
        }

        public int getPosition() {
            return position;
        }

        public String getName() {
            return name;
        }

        public boolean willClose() {
            return close;
        }

        public boolean willDestroy() {
            return destroy;
        }

        public ClickType getClickType(){
            return clickType;
        }

        public void setWillClose(boolean close) {
            this.close = close;
        }

        public void setWillDestroy(boolean destroy) {
            this.destroy = destroy;
        }
    }

}