package meteor.util.userInterfaces;// Written by Creeplays on 16.06.2016.

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import java.util.Arrays;

public class IUI implements Listener {
    private ItemStack is;
    private OptionClickEventHandler handler;

    public IUI(Plugin plugin, OptionClickEventHandler handler, ItemStack is, String name, String... info)
    {
        this.is=this.setItemNameAndLore(is,name,info);
        this.handler=handler;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler(priority= EventPriority.HIGH)
    public void onPlayerUse(PlayerInteractEvent event){
        Player player = event.getPlayer();
        ItemStack item;
        if(event.getHand().equals(EquipmentSlot.OFF_HAND)){
            item=player.getInventory().getItemInOffHand(); //Left hand
        }else if(event.getHand().equals(EquipmentSlot.HAND)){
            item=player.getInventory().getItemInMainHand();//Right hand
        }else{
            throw new Error("OMG, LEFT_HAND! Need to ask "+event.getPlayer().getName()+"");
        }
        if(item==null)
            return;
        Material type=item.getType();
        ItemMeta meta=item.getItemMeta();
        if(meta==null)
            return;
        String name=meta.getDisplayName();
        if(name==null)
            return;
        Material neededType=is.getType();
        String neededName=is.getItemMeta().getDisplayName();

        if(neededType.equals(type)&&neededName.equals(name)){
            event.setCancelled(true);
            OptionClickEvent e = new OptionClickEvent(player);
            handler.onOptionClick(e);
        }
    }


    private ItemStack setItemNameAndLore(ItemStack item, String name, String[] lore) {
        ItemMeta im = item.getItemMeta();
        im.setDisplayName(ChatColor.RESET+name);
        im.setLore(Arrays.asList(lore));
        item.setItemMeta(im);
        return item;
    }

    public void giveToPlayer(Player player,Integer slot){
        player.getInventory().setItem(slot, this.is);
    }
    public void takeFromPlayer(Player player,Integer slot){
        if(player==null)
            return;
        if(player.getInventory()==null)
            return;
        if(player.getInventory().getItem(slot)==null)
            return;
        if(player.getInventory().getItem(slot).getType().equals(is.getType()))
            player.getInventory().setItem(slot,new ItemStack(Material.AIR,-1));
    }

    public interface OptionClickEventHandler {
        void onOptionClick(OptionClickEvent event);
    }

    public class OptionClickEvent {
        private Player player;

        public OptionClickEvent(Player player) {
            this.player = player;
        }

        public Player getPlayer() {
            return player;
        }

    }

    public void destroy() {
        HandlerList.unregisterAll(this);
        handler = null;
        handler=null;
    }
}
