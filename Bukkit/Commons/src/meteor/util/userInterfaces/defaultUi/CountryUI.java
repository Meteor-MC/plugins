package meteor.util.userInterfaces.defaultUi;

import meteor.cosmetics.util.ItemFactory;
import meteor.util.constants.C;
import meteor.util.constants.F;
import meteor.util.userInterfaces.ClickableMob;
import meteor.util.userInterfaces.UI;
import meteor.util.userInterfaces.enums.EnumCountry;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

/**
 * Copyright 2016 Meteor-MC
 * All rights reserved
 * Source code for internal use only
 * Created for meteor.util.userInterfaces.defaultUi
 * Created by Creeplays on 03.07.2016.
 */
public class CountryUI {
    private UI ui;

    private int page;
    private int pages;
    private EnumCountry selected;
    private CountryUIResponseHandler handler;

    public CountryUI(Plugin plugin, Player player,CountryUIResponseHandler handler) {
        ui = new UI("Выбор страны", 6 * 9, event -> {
            event.setWillClose(false);
            event.setWillDestroy(false);
            String privateName = event.getPrivateName();
            if (privateName.equals("next")) {
                page++;
                updateUi();
            } else if (privateName.equals("back")) {
                page--;
                updateUi();
            } else if (privateName.startsWith("select_")) {
                EnumCountry temp=EnumCountry.valueOf(privateName.replace("select_", ""));
                System.out.println(temp);
                selected = temp;
                updateUi();
            } else if(privateName.equals("accept")){
                handler.onContryUIResponse(new CountryUIResponse(true,selected));
                event.setWillClose(true);
                event.setWillDestroy(true);
            } else if(privateName.equals("cancel")){
                handler.onContryUIResponse(new CountryUIResponse(false,null));
                event.setWillClose(true);
                event.setWillDestroy(true);
            }
        }, plugin, player);
        this.handler=handler;
        pages = ((Double) Math.ceil(EnumCountry.values().length / 2)).intValue();
        page = 0;
        selected = null;

        updateUi();
    }

    private void updateUi() {

        ui.setPost(C.cGray + "(" + (page + 1) + "/" + (pages + 1) + ")");
        ui.setOption(13, ItemFactory.create(Material.REDSTONE_BLOCK, (byte) 0, C.cRed + "Отмена"), "cancel");
        ui.setOption(40, ItemFactory.create(selected != null ? Material.EMERALD_BLOCK : Material.STONE, (byte) 0, C.cGreen + "Выбрать"), selected != null ? "accept" : "");
        //18        26
        //27        35
        for (int i : new int[]{18, 27}) {
            boolean canUse = page == 0;
            ui.setOption(i, ItemFactory.create(
                    canUse ? Material.GRAVEL : Material.SAND
                    , (byte) (canUse ? 0 : 1), C.cGold + "Назад"), canUse ? "" : "back");
        }
        for (int i : new int[]{26, 35}) {
            boolean canUse = page != pages;
            ui.setOption(i, ItemFactory.create(
                    canUse ? Material.SAND : Material.GRAVEL
                    , (byte) (canUse ? 1 : 0), C.cGold + "Вперёд"), canUse ? "next" : "");
        }
        //1         5
        //10        14
        //19        23
        //28        31
        //37        40
        //46        49

        EnumCountry country;
        try {
            country = EnumCountry.values()[page * 2];
        } catch (ArrayIndexOutOfBoundsException e) {
            country = null;
        }
        drawFlag(country, new int[]{1, 10, 19, 28, 37, 46});
        try {
            country = EnumCountry.values()[page * 2 + 1];
        } catch (ArrayIndexOutOfBoundsException e) {
            country = null;
        }
        drawFlag(country, new int[]{5, 14, 23, 32, 41, 50});
        ui.update();

    }

    public void open() {
        ui.open();
    }

    private void drawFlag(EnumCountry country, int[] slots) {
        if (country != null) {
            boolean isSelected = selected == country;
            int colorIndex = 0;
            for (int i : slots) {
                ItemStack wool = ItemFactory.create(Material.WOOL, country.getColors()[colorIndex].getDyeColor().getWoolData(), country.getName(),
                        "",
                        C.cGold + "Название: " + F.elem(country.getName()),
                        C.cGold + "Префикс номера: " + F.elem("+" + country.getNumberPrefix()),
                        C.cGold + "Длина номера: " + F.elem(country.getNumLen()),
                        ""
                );
                if (isSelected)
                    wool = ItemFactory.addGlow(wool);
                for (int j = 0; j < 3; j++) {
                    int index = i + j;
                    ui.setOption(index, wool, "select_" + country.name());
                }
                colorIndex++;
            }
        } else {
            for (int i : slots) {
                for (int j = 0; j < 3; j++) {
                    int index = i + j;
                    ui.setOption(index, new ItemStack(Material.AIR, 0),"");
                }
            }
        }
    }

    public interface CountryUIResponseHandler {
        void onContryUIResponse(CountryUI.CountryUIResponse event);
    }

    public class CountryUIResponse {
        private EnumCountry enumCountry;
        private boolean accepted;

        public CountryUIResponse(boolean accepted,EnumCountry enumCountry) {
            this.enumCountry = enumCountry;
            this.accepted=accepted;
        }

        public EnumCountry getCountry() {
            return enumCountry;
        }

        public boolean isAccepted(){
            return accepted;
        }

    }
}
