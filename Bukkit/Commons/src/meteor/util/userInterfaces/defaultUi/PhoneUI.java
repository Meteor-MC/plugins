package meteor.util.userInterfaces.defaultUi;

import meteor.cosmetics.util.ItemFactory;
import meteor.util.constants.C;
import meteor.util.userInterfaces.UI;
import meteor.util.userInterfaces.enums.EnumCountry;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

/**
 * Copyright 2016 Meteor-MC
 * All rights reserved
 * Source code for internal use only
 * Created for meteor.util.userInterfaces.defaultUi
 * Created by Creeplays on 02.07.2016.
 */
public class PhoneUI {
    String name;
    String[] lore;
    int enteredNums;
    int[] enteredInt;
    Player player;
    UI ui;
    PhoneEnteredEventHandler handler;
    EnumCountry country;

    public PhoneUI(Player player, Plugin plugin, String name, EnumCountry country, PhoneEnteredEventHandler handler, String... lore) {
        ui=new UI("Ввод номера", 6*9, event -> {
            String pname=event.getPrivateName();
            event.setWillClose(false);
            event.setWillDestroy(false);
            if(pname.startsWith("input_")) {
                if (enteredNums >= country.getNumLen() + 1)
                    return;
                int num = Integer.parseInt(pname.replace("input_", ""));
                enteredInt[enteredNums] = num;
                enteredNums++;
                updateUi();
            }else if(pname.equals("cancel")){
                event.setWillClose(true);
                event.setWillDestroy(true);
                handler.onPhoneEnter(new PhoneEnteredEvent(false,null));
            }else if(pname.equals("clear")){
                if(event.getClickType().equals(ClickType.LEFT)||event.getClickType().equals(ClickType.RIGHT)){
                    if(enteredNums>0){
                        enteredNums--;
                        updateUi();
                    }
                }else if(event.getClickType().equals(ClickType.SHIFT_LEFT)||event.getClickType().equals(ClickType.SHIFT_RIGHT)){
                    enteredNums=0;
                    enteredInt=new int[]{country.getNumLen()+1};
                    updateUi();
                }
            }else if(pname.equals("accept")){
                event.setWillClose(true);
                event.setWillDestroy(true);
                handler.onPhoneEnter(new PhoneEnteredEvent(true,getNumber()));
            }
        }, plugin,player);

        this.country=country;
        this.handler=handler;
        this.name=name;
        this.lore=lore;
        this.enteredNums=0;
        this.enteredInt=new int[country.getNumLen()+1];
        for (int i = 0; i < country.getNumLen(); i++) {
            enteredInt[i]=0;
        }
        //new int[]{0,0,0,0,0,0,0,0,0};

        updateUi();
        //setOption(4, ItemFactory.create(Material.CHEST,(byte)0,name,lore));
    }
    public void open(){
        ui.open();
    }
    public void updateUi(){
        String post="+"+country.getNumberPrefix();
        for (int i = 0; i < country.getNumLen(); i++) {
            boolean visible=i<enteredNums;
            post+=(visible?C.Reset:C.cRed)+(visible?enteredInt[i]:"X");
        }
        ui.setPost(post);
        ui.setOption(4, ItemFactory.create(Material.REDSTONE_BLOCK,(byte)0,C.cRed+"Отмена",
                "",
                C.cGold+"Клик для отмены",
                ""
        ),"cancel");
        {
            //Build entered
            int num=0;
            int[] positions;//=new int[country.getNumLen()];
            if(country.getNumLen()==10)
                positions=new int[]{13,18,19,20,21,22,23,24,25,26};
            else if(country.getNumLen()==9)
                positions=new int[]{18,19,20,21,22,23,24,25,26};
            else
                throw new Error("Unknown position count");
            for (int position : positions) {
                boolean entered=num<enteredNums;
                ui.setOption(position, ItemFactory.create(Material.STAINED_CLAY,(byte)(entered?13:14),entered?Integer.toString(enteredInt[num]):C.cRed+"Не введено"));
                num++;
            }
        }

        //36 37        38 39 40 41 42        43 44
        //45 46        47 48 49 50 51        52 53

        for (int i : new int[]{36, 37, 45, 46}) {
            boolean active=enteredNums==country.getNumLen();
            ui.setOption(i,ItemFactory.create(active?Material.WOOL:Material.STONE,(byte)(active?5:0),(active?C.cGreen:C.cRed)+"Подтвердить"),active?"accept":"");
        }

        for (int i : new int[]{43, 44, 52, 53}) {
            boolean active=enteredNums>0;
            ui.setOption(i,
                    ItemFactory.create(
                            active?Material.WOOL:Material.STONE,
                            (byte)(active?14:0),
                            (active?C.cGreen:C.cRed)+"Стереть",
                            "",
                            C.cGold+"Простой крик для стирания одного символа",
                            C.cGold+"Клик с SHIFTом для очистки поля",
                            ""
                    ),active?"clear":"");
        }

        int num=0;
        for (int i:new int[]{38, 39, 40, 41, 42, 47, 48, 49, 50, 51}){
            if(enteredNums>=country.getNumLen())
                ui.setOption(i,new ItemStack(Material.AIR,0),"");
            else
                ui.setOption(i,ItemFactory.create(Material.PRISMARINE,(byte)0,C.cGold+num),"input_"+num);
            num++;
        }
        ui.update();


    }

    public interface PhoneEnteredEventHandler {
        void onPhoneEnter(PhoneEnteredEvent event);
    }

    public class PhoneEnteredEvent {
        private String phone;
        private boolean accepted;

        public PhoneEnteredEvent(boolean accepted,String phone) {
            this.phone = phone;
            this.accepted=accepted;
        }

        public String getPhone() {
            return phone;
        }
        public boolean isAccepted(){
            return accepted;
        }
    }

    public String getNumber(){
        String out=country.getNumberPrefix();
        for (int i = 0; i < country.getNumLen(); i++) {
            out=out+enteredInt[i];
        }
        return out;
    }
}
