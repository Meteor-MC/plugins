package meteor.util.userInterfaces.enums;

import meteor.util.enums.EnumColor;

/**
 * Copyright 2016 Meteor-MC
 * All rights reserved
 * Source code for internal use only
 * Created for meteor.util.userInterfaces.enums
 * Created by Creeplays on 03.07.2016.
 */
public enum EnumCountry {
    RUSSIA("Россия","7",10, EnumColor.WHITE,EnumColor.LIGHT_CYAN,EnumColor.DARK_RED),
    UKRAINE("Украина","380",9, EnumColor.LIGHT_CYAN,EnumColor.YELLOW),
    BELARUS("Беларусь","375",10,EnumColor.DARK_RED,EnumColor.DARK_RED,EnumColor.DARK_CYAN),
    ISRAIL("Израиль","972",10,EnumColor.LIGHT_CYAN,EnumColor.WHITE,EnumColor.WHITE,EnumColor.WHITE,EnumColor.WHITE,EnumColor.LIGHT_CYAN),
    POLSKA("Польша","48",10,EnumColor.WHITE,EnumColor.DARK_RED);

    String name;
    String numberPrefix;
    int numLen;
    EnumColor c[];

    EnumCountry(String name,String numberPrefix,int numLen, EnumColor c1, EnumColor c2, EnumColor c3, EnumColor c4, EnumColor c5, EnumColor c6){
        this.name=name;
        this.numLen=numLen;
        this.numberPrefix=numberPrefix;
        c=new EnumColor[6];
        c[0]=c1;
        c[1]=c2;
        c[2]=c3;
        c[3]=c4;
        c[4]=c5;
        c[5]=c6;
    }
    EnumCountry(String name, String numberPrefix,int numLen, EnumColor c1, EnumColor c2, EnumColor c3){
        this.name=name;
        this.numLen=numLen;
        this.numberPrefix=numberPrefix;
        c=new EnumColor[6];
        c[0]=c1;
        c[1]=c1;
        c[2]=c2;
        c[3]=c2;
        c[4]=c3;
        c[5]=c3;
    }
    EnumCountry(String name, String numberPrefix,int numLen, EnumColor c1, EnumColor c2){
        this.name=name;
        this.numLen=numLen;
        this.numberPrefix=numberPrefix;
        c=new EnumColor[6];
        c[0]=c1;
        c[1]=c1;
        c[2]=c1;
        c[3]=c2;
        c[4]=c2;
        c[5]=c2;
    }

    public EnumColor[] getColors() {
        return c;
    }
    public String getName() {
        return name;
    }
    public String getNumberPrefix() {
        return numberPrefix;
    }
    public int getNumLen(){
        return numLen;
    }
}
