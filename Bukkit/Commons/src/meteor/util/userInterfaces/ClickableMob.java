package meteor.util.userInterfaces;// Written by Creeplays on 16.06.2016.

import meteor.util.entity.metadata.MetadataType;
import meteor.util.entity.utils.UtilEntity;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import java.util.Arrays;

public class ClickableMob implements Listener {
    private ItemStack is;
    private MobClickEventHandler handler;
    private Entity entity;
    private Plugin plugin;

    public ClickableMob(Plugin plugin, MobClickEventHandler handler, EntityType entityType, String name, Location location,Integer yaw)
    {
        Location tempLocation=location.clone();
        tempLocation.add(0.5, 0, 0.5);
        tempLocation.setYaw(yaw);

        this.handler=handler;
        this.plugin=plugin;
        this.entity=tempLocation.getWorld().spawnEntity(tempLocation,entityType);
        this.entity.teleport(tempLocation);

        UtilEntity.setData(entity, 2, (String)  name, MetadataType.STRING);  //Name
        UtilEntity.setData(entity, 4, (boolean) true, MetadataType.BOOLEAN); //Silent
        UtilEntity.setData(entity, 5, (boolean) true, MetadataType.BOOLEAN); //No gravity
        UtilEntity.setData(entity,11, (byte)    0x01, MetadataType.BYTE);    //No AI

        ((LivingEntity) entity).setCollidable(false);
        ((LivingEntity) entity).setRemoveWhenFarAway(false);

        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler(priority= EventPriority.HIGH)
    public void onAttack(EntityDamageByEntityEvent event){
        if(!(event.getDamager() instanceof Player))
            return;
        Player player = (Player)event.getDamager();
        Entity entity=event.getEntity();
        if(!entity.getCustomName().equals(this.entity.getCustomName()))
            return;

        event.setCancelled(true);
        MobClickEvent e = new MobClickEvent(player,this.entity);
        handler.onMobClick(e);
    }

    @EventHandler(priority= EventPriority.HIGH)
    public void noDamage(EntityDamageEvent event){
        Entity entity=event.getEntity();
        if(entity==null)
            return;
        if(entity.getCustomName()==null)
            return;
        if(!entity.getCustomName().equals(this.entity.getCustomName()))
            return;
        event.setCancelled(true);

    }

    @EventHandler(priority= EventPriority.HIGH)
    public void onRightClick(PlayerInteractEntityEvent event){
        Player player = event.getPlayer();
        Entity entity=event.getRightClicked();
        if(!entity.getCustomName().equals(this.entity.getCustomName()))
            return;

        event.setCancelled(true);
        MobClickEvent e = new MobClickEvent(player,this.entity);
        handler.onMobClick(e);
    }

    private ItemStack setItemNameAndLore(ItemStack item, String name, String[] lore) {
        ItemMeta im = item.getItemMeta();
        im.setDisplayName(ChatColor.RESET+name);
        im.setLore(Arrays.asList(lore));
        item.setItemMeta(im);
        return item;
    }

    public interface MobClickEventHandler {
        void onMobClick(MobClickEvent event);
    }

    public class MobClickEvent {
        private Player player;
        private Entity entity;

        public MobClickEvent(Player player, Entity entity) {
            this.player = player;
            this.entity=entity;
        }

        public Player getPlayer() {
            return player;
        }
        public Entity getEntity() {
            return entity;
        }

    }

    public void destroy() {
        HandlerList.unregisterAll(this);
        entity.remove();
        handler = null;
        plugin = null;
        entity=null;
    }
}
