package meteor.util.utils;

import meteor.util.account.events.AccountPreloadEvent;
import meteor.util.events.PlayerPreloadEvent;
import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright 2016 Meteor-MC
 * All rights reserved
 * Source code for internal use only
 * Created for ${PACKAGE_NAME}
 * Created by Creeplays on 02/07/16.
 */


public class UtilCore{
    private static Plugin plugin;
    private static Map<Listener,Boolean> listeners=new HashMap<>();

    public static void initialize(Plugin plugin){
        UtilCore.plugin=plugin;
    }

	public static Plugin getPlugin(){
		return plugin;
	}

    public static void registerListener(Listener listener,boolean cleanable){
        listeners.put(listener,cleanable);
        getPlugin().getServer().getPluginManager().registerEvents(listener,getPlugin());
    }
    public static void registerListener(Listener listener){
        registerListener(listener,true);
    }

    public static void unregisterListener(Listener listener){
        listeners.remove(listener);
        HandlerList.unregisterAll(listener);
    }
    public static void unregisterAllListeners(boolean force){
        for (Listener listener : listeners.keySet()) {
            if(force)
                unregisterListener(listener);
            else if (listeners.get(listener))
                    unregisterListener(listener);
        }
    }
    public static void unregisterAllListeners(){
        unregisterAllListeners(false);
    }

    public static Collection<? extends Player> getPlayers(){
        return getPlugin().getServer().getOnlinePlayers();
    }

    public static void callEvent(Event event){
        getPlugin().getServer().getPluginManager().callEvent(event);
    }
}
