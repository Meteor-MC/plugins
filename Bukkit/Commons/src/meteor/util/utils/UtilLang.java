package meteor.util.utils;// Written by Creeplays on 20.06.2016.

public class UtilLang {
    private static String plural_ru(Integer count, String form1, String form2, String form5)
    {
        count = Math.abs(count) % 100;
        Integer count1 = count % 10;
        if (count > 10 && count < 20) return form5;
        if (count1 > 1 && count1 < 5) return form2;
        if (count1 == 1) return form1;
        return form5;
    }
    public static String plural_ru(double d,String form1, String form2, String form5){
        return plural_ru(Integer.valueOf((int)d),form1,form2,form5);
    }
}
