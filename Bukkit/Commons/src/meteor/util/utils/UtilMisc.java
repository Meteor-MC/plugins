package meteor.util.utils;// Written by Creeplays on 20.06.2016.

import meteor.cosmetics.version.AAnvilGUI;
import meteor.util.userInterfaces.AnvilGUI;
import org.bukkit.entity.Player;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class UtilMisc {
    public static String getExternalIp() throws IOException {
        URL whatismyip = new URL("http://checkip.amazonaws.com");
        BufferedReader in = new BufferedReader(new InputStreamReader(
                whatismyip.openStream()));
        return in.readLine();
    }

    public static AnvilGUI getAnvilGui(Player player, AAnvilGUI.AnvilClickEventHandler handler){
        return new AnvilGUI(player,handler);
    }

    public static String getYtSubCount(String channelUrl){
        //Fixme: dont work
//        String out="Ошибка";
//        String temp;
//        try {
//            temp=UtilFile.readUrl(channelUrl);
//        } catch (IOException e) {
//            e.printStackTrace();
//            return out;
//        }
//        out=UtilText.getBetween(temp,"id=\"realtime-live-sub-base-count\">","</div>");
        int outInt=0; //Integer.parseInt(out);
        return outInt+" "+UtilLang.plural_ru(outInt,"подписчик","подписчика","подписчиков");
    }
}
