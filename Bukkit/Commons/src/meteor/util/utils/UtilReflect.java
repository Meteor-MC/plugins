package meteor.util.utils;

import java.lang.reflect.Field;

/**
 * Copyright 2016 Meteor-MC
 * All rights reserved
 * Source code for internal use only
 * Created for meteor.util.utils
 * Created by Creeplays on 02.07.2016.
 */
public class UtilReflect {
    public static void setInt(Object object,String targetField,int to) {
        try {
            Field field = object.getClass().getDeclaredField(targetField);
            boolean accessible=field.isAccessible();
            field.setAccessible(true);
            field.setInt(object, to);
            field.setAccessible(accessible);
        }catch (NoSuchFieldException | IllegalAccessException e){
            System.err.println("REFLECTION ERROR!");
            e.printStackTrace();
        }
    }
}
