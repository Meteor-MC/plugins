package meteor.util.utils;// Written by Creeplays on 20.06.2016.

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashSet;

public class UtilGear
{
    private static HashSet<Material> axeSet = new HashSet<Material>();
    private static HashSet<Material> swordSet = new HashSet<Material>();
    private static HashSet<Material> shovelSet = new HashSet<Material>();
    private static HashSet<Material> pickSet = new HashSet<Material>();
    private static HashSet<Material> diamondSet = new HashSet<Material>();
    private static HashSet<Material> goldSet = new HashSet<Material>();
    public static HashSet<Material> scytheSet = new HashSet<Material>();

    public static boolean isAxe(ItemStack item) {
        if (item == null)
            return false;

        if (axeSet.isEmpty())
        {
            axeSet.add(Material.WOOD_AXE);
            axeSet.add(Material.STONE_AXE);
            axeSet.add(Material.IRON_AXE);
            axeSet.add(Material.GOLD_AXE);
            axeSet.add(Material.DIAMOND_AXE);
        }

        return axeSet.contains(item.getType());
    }
    public static boolean isSword(ItemStack item) {

        if (item == null)
            return false;

        if (swordSet.isEmpty())
        {
            swordSet.add(Material.WOOD_SWORD);
            swordSet.add(Material.STONE_SWORD);
            swordSet.add(Material.IRON_SWORD);
            swordSet.add(Material.GOLD_SWORD);
            swordSet.add(Material.DIAMOND_SWORD);
        }

        return swordSet.contains(item.getType());
    }
    public static boolean isShovel(ItemStack item) {
        if (item == null)
            return false;

        if (shovelSet.isEmpty())
        {
            shovelSet.add(Material.WOOD_SPADE);
            shovelSet.add(Material.STONE_SPADE);
            shovelSet.add(Material.IRON_SPADE);
            shovelSet.add(Material.GOLD_SPADE);
            shovelSet.add(Material.DIAMOND_SPADE);
        }

        return shovelSet.contains(item.getType());
    }
    public static boolean isHoe(ItemStack item) {
        if (item == null)
            return false;

        if (scytheSet.isEmpty())
        {
            scytheSet.add(Material.WOOD_HOE);
            scytheSet.add(Material.STONE_HOE);
            scytheSet.add(Material.IRON_HOE);
            scytheSet.add(Material.GOLD_HOE);
            scytheSet.add(Material.DIAMOND_HOE);
        }

        return scytheSet.contains(item.getType());
    }
    public static boolean isPickaxe(ItemStack item) {
        if (item == null)
            return false;

        if (pickSet.isEmpty())
        {
            pickSet.add(Material.WOOD_PICKAXE);
            pickSet.add(Material.STONE_PICKAXE);
            pickSet.add(Material.IRON_PICKAXE);
            pickSet.add(Material.GOLD_PICKAXE);
            pickSet.add(Material.DIAMOND_PICKAXE);
        }

        return pickSet.contains(item.getType());
    }
    public static boolean isDiamond(ItemStack item) {
        if (item == null)
            return false;

        if (diamondSet.isEmpty())
        {
            diamondSet.add(Material.DIAMOND_SWORD);
            diamondSet.add(Material.DIAMOND_AXE);
            diamondSet.add(Material.DIAMOND_SPADE);
            diamondSet.add(Material.DIAMOND_HOE);
        }

        return diamondSet.contains(item.getType());
    }
    public static boolean isGold(ItemStack item){
        if (item == null)
            return false;

        if (goldSet.isEmpty())
        {
            goldSet.add(Material.GOLD_SWORD);
            goldSet.add(Material.GOLD_AXE);
        }

        return goldSet.contains(item.getType());
    }
    public static boolean isBow(ItemStack item){
        if (item == null)
            return false;

        return item.getType() == Material.BOW;
    }
    public static boolean isWeapon(ItemStack item)
    {
        return isAxe(item) || isSword(item);
    }
    public static boolean isRepairable(ItemStack item)
    {
        return (item.getType().getMaxDurability() > 0);
    }

    public static boolean haveInHand(Player player,Material material){
        ItemStack left=player.getInventory().getItemInOffHand(); //Left hand
        ItemStack right=player.getInventory().getItemInMainHand();//Right hand
        return left.getType()==material||right.getType()==material;
    }
}

