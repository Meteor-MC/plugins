package meteor.util.utils;// Written by Creeplays on 20.06.2016.

import meteor.util.entity.utils.UtilPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.util.Collection;

public class UtilServer {
    public static Collection<? extends Player> getPlayers(){
        return Bukkit.getServer().getOnlinePlayers();
    }
    public static Server getServer(){
        return Bukkit.getServer();
    }
    public static void broadcast(String message) {
        for (Player cur : getPlayers())
            UtilPlayer.message(cur, message);
    }
    public static void broadcastSpecial(String event, String message){
        for (Player cur : getPlayers())
        {
            UtilPlayer.message(cur, "§b§l" + event);
            UtilPlayer.message(cur, message);
            cur.playSound(cur.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 2f, 0f);
            cur.playSound(cur.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 2f, 0f);
        }
    }
    public static void broadcast(String sender, String message)
    {
        broadcast("§f§l" + sender + " " + "§b" + message);
    }
    public static void broadcastMagic(String sender, String message)
    {
        broadcast("§2§k" + message);
    }
    public static double getFilledPercent(){
        return (double)getPlayers().size() / (double)UtilServer.getServer().getMaxPlayers();
    }
}
