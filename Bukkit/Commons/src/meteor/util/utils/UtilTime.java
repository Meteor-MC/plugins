package meteor.util.utils;// Written by Creeplays on 20.06.2016.

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class UtilTime
{
    public static final String DATE_FORMAT_NOW = "MM-dd-yyyy HH:mm:ss";
    public static final String DATE_FORMAT_DAY = "MM-dd-yyyy";

    public static String now()
    {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        return sdf.format(cal.getTime());
    }

    public static long nowLong(){
        return System.currentTimeMillis();
    }

    public static String when(long time)
    {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        return sdf.format(time);
    }


    public static String date()
    {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_DAY);
        return sdf.format(cal.getTime());
    }

    public enum TimeUnit
    {
        FIT,
        DAYS,
        HOURS,
        MINUTES,
        SECONDS,
        MILLISECONDS
    }

    public static String since(long epoch)
    {
        return "Прошло " + convertString(System.currentTimeMillis()-epoch, 1, TimeUnit.FIT) + ".";
    }

    public static double convert(long time, int trim, TimeUnit type)
    {
        if (type == TimeUnit.FIT)
        {
            if (time < 60000)				type = TimeUnit.SECONDS;
            else if (time < 3600000)		type = TimeUnit.MINUTES;
            else if (time < 86400000)		type = TimeUnit.HOURS;
            else							type = TimeUnit.DAYS;
        }

        if (type == TimeUnit.DAYS)			return UtilMath.trim(trim, (time)/86400000d);
        if (type == TimeUnit.HOURS)			return UtilMath.trim(trim, (time)/3600000d);
        if (type == TimeUnit.MINUTES)		return UtilMath.trim(trim, (time)/60000d);
        if (type == TimeUnit.SECONDS)		return UtilMath.trim(trim, (time)/1000d);
        else								return UtilMath.trim(trim, time);
    }

    public static String MakeStr(long time)
    {
        return convertString(time, 1, TimeUnit.FIT);
    }

    public static String MakeStr(long time, int trim)
    {
        return convertString(Math.max(0, time), trim, TimeUnit.FIT);
    }

    public static String convertString(long time, int trim, TimeUnit type)
    {
        if (time == -1)						return "Вечность";

        if (type == TimeUnit.FIT)
        {
            if (time < 60000)				type = TimeUnit.SECONDS;
            else if (time < 3600000)		type = TimeUnit.MINUTES;
            else if (time < 86400000)		type = TimeUnit.HOURS;
            else							type = TimeUnit.DAYS;
        }

        String text;
        double num;
        if (trim == 0)
        {
            if (type == TimeUnit.DAYS)			text = (num = UtilMath.trim(trim, time / 86400000d)) + UtilLang.plural_ru(num," День", " Дня"," Дней");
            else if (type == TimeUnit.HOURS)	text = (num = UtilMath.trim(trim, time / 3600000d)) + UtilLang.plural_ru(num," Час", " Часа"," Часов");
            else if (type == TimeUnit.MINUTES)	text = (num = UtilMath.trim(trim, time / 60000d)) + UtilLang.plural_ru(num," Минута", " Минуты"," Минут");
            else if (type == TimeUnit.SECONDS)	text = (int) (num = (int) UtilMath.trim(trim, time / 1000d)) + UtilLang.plural_ru(num," Секунда", " Секунды"," Секунд");
            else								text = (int) (num = (int) UtilMath.trim(trim, time)) + UtilLang.plural_ru(num," Миллисекунда", " Миллисекунды"," Миллисекунд");
        }
        else
        {
            if (type == TimeUnit.DAYS)			text = (num = UtilMath.trim(trim, time / 86400000d)) + UtilLang.plural_ru(num," День", " Дня"," Дней");
            else if (type == TimeUnit.HOURS)	text = (num = UtilMath.trim(trim, time / 3600000d)) + UtilLang.plural_ru(num," Час", " Часа"," Часов");
            else if (type == TimeUnit.MINUTES)	text = (num = UtilMath.trim(trim, time / 60000d)) + UtilLang.plural_ru(num," Минута", " Минуты"," Минут");
            else if (type == TimeUnit.SECONDS)	text = (num = UtilMath.trim(trim, time / 1000d)) + UtilLang.plural_ru(num," Секунда", " Секунды"," Секунд");
            else								text = (int) (num = (int) UtilMath.trim(0, time)) + UtilLang.plural_ru(num," Миллисекунда", " Миллисекунды"," Миллисекунд");
        }

        return text;
    }

    public static long elapsedLong(long from){
        return System.currentTimeMillis() - from;
    }
    public static boolean elapsed(long from, long required)
    {
        return System.currentTimeMillis() - from > required;
    }
    public static String elapsedString(long from, long required){
        return convertString(required - (System.currentTimeMillis() - from),1,TimeUnit.FIT);
    }
}
