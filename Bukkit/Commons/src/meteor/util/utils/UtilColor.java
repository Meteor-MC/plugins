package meteor.util.utils;// Written by Creeplays on 21.06.2016.

import meteor.util.constants.C;

public class UtilColor {
    public static String getChatColorByCode(Integer code){
        switch (code){
            case 0:
                return C.cGold;
            case 1:
                return C.cAqua;
            case 2:
                return C.cDPurple;
            case 3:
                return C.cBlue;
            default:
                return C.cGray;
        }
    }
}
