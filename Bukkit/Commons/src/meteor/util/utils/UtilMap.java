package meteor.util.utils;

import net.minecraft.server.v1_10_R1.BlockPosition;
import net.minecraft.server.v1_10_R1.IBlockData;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_10_R1.CraftWorld;
import org.bukkit.entity.Player;

/**
 * Created by Creeplays on 22.06.2016.
 */
public class UtilMap {
    public static void ChunkBlockChange(World world, int x, int y, int z, int id, byte data, boolean notifyPlayers) {
        int cx=x >> 4; //Chunk x
        int cz=z >> 4; //Chunk z

        changeChunkBlock(x & 15, y, z & 15, ((CraftWorld) world).getHandle().getChunkAt(cx, cz), id, data);
        if (notifyPlayers)
            for (Player player : UtilServer.getPlayers())
                player.getWorld().refreshChunk(cx, cz);

    }
    public static void resendChunkForPlayer(Player player,int x,int z){
        player.getWorld().refreshChunk(x, z);
    }
    public static void resendChunkForPlayers(int x,int z){
        for (Player player : UtilServer.getPlayers()) {
            resendChunkForPlayer(player,x,z);
        }
    }
    private static void changeChunkBlock(int x, int y, int z, net.minecraft.server.v1_10_R1.Chunk chunk, int id, byte data){
        chunk.a(getBlockPosition(x,y,z),getIBlockData(id,data));
    }
    public static IBlockData getIBlockData(int blockId,byte data){
        int combined = blockId + (data << 12);
        IBlockData ibd = net.minecraft.server.v1_10_R1.Block.getByCombinedId(combined);
        return ibd;
    }
    public static BlockPosition getBlockPosition(int x, int y, int z){
        return new BlockPosition(x,y,z);
    }
}
