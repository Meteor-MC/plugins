package meteor.util.utils;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by Creeplays on 15.06.2016.
 */
public class UtilWorld {
    public static String getMapName(World world){
        return getWorldString(world, "name");
    }
    public static String getAuthorName(World world){
        return getWorldString(world,"author");
    }
    public static String getWorldString(World world,String tag){
        File fn=new File(world.getWorldFolder().getPath()+"/"+tag.toUpperCase());
        try {
            return new String(Files.readAllBytes(Paths.get(fn.getPath())), "UTF-8").trim();
        }catch (IOException e){
            e.printStackTrace();
        }
        return "-";
    }

    public static File getWorldsDir(){
        return Bukkit.getWorldContainer();
    }

    public static Map<String,Collection<String>> getMaps(String folderName){
        Map<String,Collection<String>> gameMap=new HashMap<>();
        File folder=new File(getWorldsDir(),folderName);
        if(!folder.exists())
            if(!folder.mkdirs())
                throw new Error("Error on creating worlds folder");
        for (File gameFolder : folder.listFiles()) {
            for (File mapFolder : gameFolder.listFiles()) {
                if(!gameMap.containsKey(gameFolder.getName()))
                    gameMap.put(gameFolder.getName(), new ArrayList<String>());
                gameMap.get(gameFolder.getName()).add(mapFolder.getName());
            }
        }
        return gameMap;
    }
    public static Map<String,Collection<String>> getCompiledMaps(){
        return getMaps("Compiled");
    }
    public static Map<String,Collection<String>> getSourceMaps(){
        return getMaps("Source");
    }

    public static String getWorld(String folder,String gameName,String mapName){
        return folder+"/"+gameName+"/"+mapName;
    }
    public static String getCompiledWorld(String gameName,String mapName){
        return getWorld("Compiled",gameName,mapName);
    }
    public static String getSourceWorld(String gameName,String mapName){
        return getWorld("Source",gameName,mapName);
    }

    public static void clearWorld(World world){
        Set<String> allowed=new HashSet<String>();
        allowed.add("region");
        allowed.add("level.dat");
        allowed.add("INFO");
        for (File file : new File(world.getWorldFolder().getPath() , "./../../../Compiled/" + world.getName()).listFiles()) {
            if(!allowed.contains(file.getName()))
                UtilFile.delete(file);
        }
    }

    public static void setMapName(World world, String name){
        setWorldString(world,"name",name);
    }
    public static void setAuthorName(World world, String name){
        setWorldString(world,"author",name);
    }
    public static void setWorldString(World world,String tag,String str){
        File fn=new File(world.getWorldFolder().getPath()+"/"+tag.toUpperCase());
        try {
            Files.write(Paths.get(fn.getPath()), Collections.singletonList(str), Charset.forName("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void setSignText(Block signBlock, int stringNum, String string){
        signBlock.getChunk().load();
        Sign state=(Sign)signBlock.getState();
        state.setLine(stringNum,string);
        state.update(true);
    }
}
