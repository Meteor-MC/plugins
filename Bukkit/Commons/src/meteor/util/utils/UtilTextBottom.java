package meteor.util.utils;// Written by Creeplays on 20.06.2016.

import meteor.util.constants.C;
import meteor.util.entity.utils.UtilPlayer;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class UtilTextBottom
{
    public static void display(String text, Player... players){
        for (Player player : players) {
            UtilPlayer.messageAboveHotbar(player,text);
        }
    }
    public static void display(Player player,String text){
        UtilPlayer.messageAboveHotbar(player,text);
    }

    public static void displayProgress(double amount, Player... players){
        displayProgress(null, amount, null, players);
    }
    public static void displayProgress(String prefix, double amount, Player... players){
        displayProgress(prefix, amount, null, players);
    }
    public static void displayProgress(String prefix, double amount, String suffix, Player... players){
        displayProgress(prefix, amount, suffix, false, players);
    }
    public static void displayProgress(String prefix, double amount, String suffix, boolean progressDirectionSwap, Player... players){
        if (progressDirectionSwap)
            amount = 1 - amount;

        //Generate Bar
        int bars = 24;
        String progressBar = C.cGreen + "";
        boolean colorChange = false;
        for (int i=0 ; i<bars ; i++)
        {
            if (!colorChange && (float)i/(float)bars >= amount)
            {
                progressBar += C.cRed;
                colorChange = true;
            }

            progressBar += "▌";
        }

        //Send to Player
        display((prefix == null ? "" : prefix + ChatColor.RESET + " ") + progressBar + (suffix == null ? "" : ChatColor.RESET + " " + suffix), players);
    }
}
