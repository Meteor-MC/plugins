package meteor.util.utils;

/**
 * Created by Creeplays on 01.07.2016.
 */
public class UtilAsync {
    public static void runAsync(Runnable runnable){
        UtilCore.getPlugin().getServer().getScheduler().runTaskAsynchronously(UtilCore.getPlugin(), runnable);
    }
}
