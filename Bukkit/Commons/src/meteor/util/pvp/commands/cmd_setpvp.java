package meteor.util.pvp.commands;

import meteor.util.Rank;
import meteor.util.command.bases.CommandBase;
import meteor.util.pvp.PVPManager;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * Copyright 2016 Meteor-MC
 * All rights reserved
 * Source code for internal use only
 * Created for meteor.util.pvp.commands
 * Created by Creeplays on 05.07.2016.
 */
public class cmd_setpvp extends CommandBase {
    public cmd_setpvp(Plugin plugin) {
        super(plugin, "Менеджер pvp", "/setpvp <new/old>", Rank.ADMIN, "setpvp");
    }

    @Override
    public void Execute(Player caller, String[] args) {
        if(args==null){
            printUsage(caller);
            return;
        }
        if(args.length!=1){
            printUsage(caller);
            return;
        }
        boolean newPvp;
        switch (args[0]){
            case "new":
                newPvp=true;
                break;
            case "old":
                newPvp=false;
                break;
            default:
                printUsage(caller);
                return;
        }

        PVPManager.instance.setNewPvp(newPvp);
    }
}
