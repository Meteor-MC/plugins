package meteor.util.pvp;

import meteor.cosmetics.util.ItemFactory;
import meteor.util.MiniPlugin;
import meteor.util.constants.C;
import meteor.util.entity.utils.UtilPlayer;
import meteor.util.pvp.commands.cmd_setpvp;
import meteor.util.reciepes.RecipeManager;
import meteor.util.utils.UtilGear;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.*;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.HashSet;
import java.util.Set;

/**
 * Copyright 2016 Meteor-MC
 * All rights reserved
 * Source code for internal use only
 * Created for meteor.util.pvp
 * Created by Creeplays on 05.07.2016.
 */

public class PVPManager extends MiniPlugin {
    public static PVPManager instance;
    public static void initialize(Plugin plugin){
        instance=new PVPManager(plugin);
    }

    private Scoreboard scoreboard;
    private Team team;

    public void setCollision(boolean collision) {
        this.collision = collision;
        update();
    }

    public void setGodApple(boolean godApple) {
        this.godApple = godApple;
        update();
    }

    public void setFastAttack(boolean fastAttack) {
        this.fastAttack = fastAttack;
        update();
    }

    public void setBlocking(boolean blocking) {
        this.blocking = blocking;
        update();
    }

    public void setAllowOffHand(boolean allowOffHand) {
        this.allowOffHand = allowOffHand;
        update();
    }

    public void setAutoLapis(boolean autoLapis) {
        this.autoLapis = autoLapis;
        Bukkit.getOnlinePlayers().forEach(this::updateInventory);
        update();
    }

    public void setAutoBlaze(boolean autoBlaze) {
        this.autoBlaze = autoBlaze;
        Bukkit.getOnlinePlayers().forEach(this::updateInventory);
        update();
    }

    public void setNoDamage(boolean noDamage) {
        this.noDamage = noDamage;
        update();
    }

    public void setNewPvp(boolean newPvp){
        collision=newPvp;
        godApple=!newPvp;
        fastAttack=!newPvp;
        blocking=!newPvp;
        allowOffHand=newPvp;
        autoLapis=!newPvp;
        autoBlaze=!newPvp;
        update();
        updateReciepes();
        Bukkit.getOnlinePlayers().forEach(this::updatePlayers);
        Bukkit.getOnlinePlayers().forEach(this::updateInventory);
    }

    private void updateInventory(Player player){
        if(player.getOpenInventory()==null)
            return;
        if(player.getOpenInventory().getType().equals(InventoryType.BREWING)){
            if(autoBlaze){
                if(!player.getOpenInventory().getItem(4).equals(blazeStack)){
                    UtilPlayer.giveOrDrop(player,player.getOpenInventory().getItem(4));
                    player.getOpenInventory().setItem(4,new ItemStack(Material.AIR));
                }
                player.getOpenInventory().setItem(4, blazeStack);
            }else{
                if(player.getOpenInventory().getItem(4).equals(blazeStack)){
                    player.getOpenInventory().setItem(4, new ItemStack(Material.AIR));
                }
            }
        }
        if(player.getOpenInventory().getType().equals(InventoryType.ENCHANTING)){
            if(autoLapis){
                if(!player.getOpenInventory().getItem(1).equals(lapisStack)){
                    UtilPlayer.giveOrDrop(player,player.getOpenInventory().getItem(1));
                    player.getOpenInventory().setItem(1,new ItemStack(Material.AIR));
                }
                player.getOpenInventory().setItem(1, lapisStack);
            }else{
                if(player.getOpenInventory().getItem(1).equals(lapisStack)){
                    player.getOpenInventory().setItem(1, new ItemStack(Material.AIR));
                }
            }
        }
    }

    private boolean collision;
    private boolean godApple;
    private boolean fastAttack;
    private boolean blocking;
    private boolean allowOffHand;
    private boolean autoLapis;
    private boolean autoBlaze;
    private ItemStack lapisStack;
    private ItemStack blazeStack;


    private boolean noDamage;

    private Set<Player> blockingPlayers;

    protected PVPManager(Plugin plugin) {
        super("Менеджер боя", plugin);
        collision=true;
        godApple=false;
        fastAttack=false;
        blocking=false;
        allowOffHand=true;
        autoBlaze=false;
        autoLapis=false;
        noDamage=false;


        blockingPlayers=new HashSet<>();

        blazeStack= ItemFactory.create(Material.BLAZE_POWDER,(byte)0,64,"Фикс зельеварения","", C.cGold+"Meteor-MC плагин старой системы PVP","");
        lapisStack= ItemFactory.create(Material.INK_SACK,(byte)4,64,"Фикс зачаровальни","", C.cGold+"Meteor-MC плагин старой системы PVP","");
    }

    @Override
    public void enable() {
        this.scoreboard= Bukkit.getScoreboardManager().getNewScoreboard();
        this.team=scoreboard.registerNewTeam("pvpManager");
        update();
    }

    @Override
    public void disable() {
        this.team.unregister();
    }
    @Override
    public void addCommands() {
        addCommand(new cmd_setpvp(getPlugin()));
    }
    public void updatePlayers(Player... players){
        this.team.setOption(Team.Option.COLLISION_RULE,collision? Team.OptionStatus.ALWAYS: Team.OptionStatus.NEVER);
        for (Player player : players) {
            if(player.getScoreboard()!=null){
                Scoreboard board = player.getScoreboard();
                Team team = board.getTeam("pvpManager") == null ? board.registerNewTeam("pvpManager") : board.getTeam("pvpManager");
                team.setOption(Team.Option.COLLISION_RULE,collision? Team.OptionStatus.ALWAYS: Team.OptionStatus.NEVER);
                team.addPlayer(player);
                player.setScoreboard(board);
            }else{
                this.team.addPlayer(player);
                player.setScoreboard(this.scoreboard);
            }
            player.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(fastAttack?10000.0:4.0);
        }
    }
    public void updateReciepes(){
        ShapedRecipe godAppleRecipe = new ShapedRecipe(new ItemStack(Material.GOLDEN_APPLE, 1,(short)1));
        godAppleRecipe.shape("ggg", "gag", "ggg");
        godAppleRecipe.setIngredient('g', Material.GOLD_BLOCK);
        godAppleRecipe.setIngredient('a', Material.APPLE);
        if(godApple)
            RecipeManager.instance.addRecipe(godAppleRecipe);
        else
            RecipeManager.instance.removeRecipe(godAppleRecipe);
    }
    public void update(){
        updateReciepes();
        Bukkit.getOnlinePlayers().forEach(this::updatePlayers);
    }

    @EventHandler
    public void eat(PlayerItemConsumeEvent e) {
        if(!godApple)
            return;
        Player p = e.getPlayer();
        if (e.getItem().getType() == Material.GOLDEN_APPLE) {
            e.setCancelled(true);
            if (e.getItem().getDurability() == 0) {
                gappleEffect(p, e.getItem());
            }
            if (e.getItem().getDurability() == 1) {
                godEffect(p, e.getItem());
            }
        }
    }
    private void gappleEffect(Player player, ItemStack eaten) {
        eaten.setAmount(1);
        player.getInventory().removeItem(eaten);
        player.updateInventory();
        if (player.getFoodLevel() + 4 <= 20) {
            player.setFoodLevel(player.getFoodLevel() + 20);
        } else {
            player.setFoodLevel(20);
        }
        player.removePotionEffect(PotionEffectType.ABSORPTION);
        player.removePotionEffect(PotionEffectType.REGENERATION);
        player.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 2400, 0));
        PotionEffect pEffect = new PotionEffect(PotionEffectType.REGENERATION, 100, 0);
        pEffect.apply(player);
    }
    private void godEffect(Player player, ItemStack eaten) {
        eaten.setAmount(1);
        player.getInventory().removeItem(eaten);
        player.updateInventory();
        if (player.getFoodLevel() + 4 <= 20) {
            player.setFoodLevel(player.getFoodLevel() + 20);
        } else {
            player.setFoodLevel(20);
        }
        player.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
        player.removePotionEffect(PotionEffectType.FIRE_RESISTANCE);
        player.removePotionEffect(PotionEffectType.ABSORPTION);
        player.removePotionEffect(PotionEffectType.REGENERATION);
        player.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 2400, 0));
        PotionEffect pEffect = new PotionEffect(PotionEffectType.REGENERATION, 600, 4);
        pEffect.apply(player);
        player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 6000, 0));
        player.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 6000, 0));
    }

    @EventHandler
    public void swap(PlayerSwapHandItemsEvent event) {
        if(!allowOffHand)
            event.setCancelled(true);
        if (blockingPlayers.contains(event.getPlayer())) {
            event.setCancelled(true);
        }
    }
    @EventHandler
    public void block(PlayerInteractEvent event) {
        if(!blocking)
            return;
        if(event.getAction() != Action.RIGHT_CLICK_AIR && event.getAction() != Action.RIGHT_CLICK_BLOCK)
            return;
        if(!UtilGear.isSword(event.getPlayer().getInventory().getItemInMainHand()))
            return;
        if(blockingPlayers.contains(event.getPlayer()))
            return;
        blockingPlayers.add(event.getPlayer());
        if (event.getPlayer().getInventory().getItemInOffHand() != null && event.getPlayer().getInventory().getItemInOffHand().getType() != Material.SHIELD) {
            UtilPlayer.giveOrDrop(event.getPlayer(),event.getPlayer().getInventory().getItemInOffHand());
            event.getPlayer().getInventory().setItemInOffHand(null);
        }
        event.getPlayer().getInventory().setItemInOffHand(new ItemStack(Material.SHIELD));
        event.getPlayer().updateInventory();
    }
    @EventHandler
    public void move(PlayerMoveEvent event) {
        updatePlayers(event.getPlayer());
    }
    @EventHandler
    public void join(PlayerJoinEvent event){
        updatePlayers(event.getPlayer());
    }
    @EventHandler
    public void onClose(InventoryCloseEvent e) {
        if (autoBlaze && e.getInventory().getType() == InventoryType.BREWING) {
            e.getInventory().setItem(4, new ItemStack(Material.AIR));
        }
        if (autoLapis && e.getInventory().getType() == InventoryType.ENCHANTING) {
            e.getInventory().setItem(1, new ItemStack(Material.AIR));
        }
    }
    @EventHandler
    public void InventoryMove(InventoryClickEvent e) {
        if (autoBlaze && e.getClickedInventory() != null && e.getClickedInventory().getType() == InventoryType.BREWING && e.getCurrentItem().equals(blazeStack)) {
            e.setCancelled(true);
        }
        if (autoLapis && e.getClickedInventory() != null && e.getClickedInventory().getType() == InventoryType.ENCHANTING && e.getCurrentItem().equals(lapisStack)) {
            e.setCancelled(true);
        }
    }
    @EventHandler
    public void openInv(InventoryOpenEvent e) {
        updateInventory((Player) e.getPlayer());
    }
    @EventHandler
    public void damage(EntityDamageByEntityEvent event){
        if(noDamage)
            event.setCancelled(true);
    }
}
