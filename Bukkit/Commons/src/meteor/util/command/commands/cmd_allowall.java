package meteor.util.command.commands;

import meteor.util.Rank;
import meteor.util.command.CommandManager;
import meteor.util.command.bases.CommandBase;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * Copyright 2016 Meteor-MC
 * All rights reserved
 * Source code for internal use only
 * Created for meteor.util.command.commands
 * Created by Creeplays on 02.07.2016.
 */
public class cmd_allowall extends CommandBase {
    private CommandManager commandManager;
    public cmd_allowall(Plugin plugin, CommandManager commandManager) {
        super(plugin, "Команды", "/allowall", Rank.ADMIN, "allowall");
        this.commandManager=commandManager;
    }

    @Override
    public void Execute(Player caller, String[] args) {
        if(args!=null) {
            printUsage(caller);
            return;
        }
        commandManager.addAllowed(caller.getName());
        answerPlayer(caller,"Доступ открыт!");
    }
}
