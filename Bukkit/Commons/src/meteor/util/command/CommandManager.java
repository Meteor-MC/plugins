package meteor.util.command;// Written by Creeplays on 20.06.2016.

import meteor.util.Rank;
import meteor.util.WrappedHashMap;
import meteor.util.command.bases.ICommand;
import meteor.util.command.commands.cmd_allowall;
import meteor.util.constants.F;
import meteor.util.entity.utils.UtilPlayer;
import meteor.util.recharge.Recharge;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.TabCompleteEvent;
import org.bukkit.plugin.Plugin;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CommandManager implements Listener
{
    public static CommandManager instance;
    public static void initialize(Plugin plugin){
        instance=new CommandManager(plugin);
    }

    private Plugin plugin;
    private WrappedHashMap<String, ICommand> commands;
    private Set<String> allowedAll;

    public void setBlockAll(boolean blockAll) {
        this.blockAll = blockAll;
    }

    private boolean blockAll;

    private CommandManager(Plugin plugin){
        this.plugin = plugin;

        commands = new WrappedHashMap<>();
        allowedAll=new HashSet<>();
        blockAll =false;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);

        addCommand(new cmd_allowall(plugin,this));
    }

    @EventHandler
    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event){
        if(blockAll){
            event.getPlayer().sendMessage(F.main("Команды", "Доступ к командам закрыт!"));
            return;
        }
        String commandName = event.getMessage().substring(1);
        String[] args = null;

        if (commandName.contains(" "))
        {
            commandName = commandName.split(" ")[0];
            args = event.getMessage().substring(event.getMessage().indexOf(' ') + 1).split(" ");
        }

        ICommand command = commands.get(commandName.toLowerCase());

        if (command != null)
        {
            event.setCancelled(true);

            if (Rank.getRank(event.getPlayer()).checkRank(event.getPlayer(), command.getRequiredRank(), command.getSpecificRanks(), true))
            {
                if (!Recharge.instance.use(event.getPlayer(), "Команды", 500, false, false))
                {
                    event.getPlayer().sendMessage(F.main("Команды", "Не спамь!"));
                    return;
                }

                command.setAliasUsed(commandName.toLowerCase());
                command.Execute(event.getPlayer(), args);
            }
        }else{
            if(allowedAll.contains(event.getPlayer().getName())){
                UtilPlayer.sendCommandOutput(event.getPlayer(), "МЕТЕОР", "Перехват команды выполнен!");
            }else {
                event.setCancelled(true);
                UtilPlayer.sendCommandOutput(event.getPlayer(), "МЕТЕОР", "Неизвестная команда!");
            }
        }
    }
    @EventHandler
    public void onTabComplete(TabCompleteEvent event){
        String[] commandArr=event.getBuffer().substring(1).split(" ");
        ICommand command = commands.get(commandArr[0].toLowerCase());
        String[] args = Arrays.copyOfRange(commandArr, 1, commandArr.length);

        if (command != null)
        {
            List<String> suggestions = command.onTabComplete(event.getSender(),commandArr[0].toLowerCase() ,args);

            if (suggestions != null)
                event.setCompletions(suggestions);
        }
    }

    public void addAllowed(String player){
        allowedAll.add(player);
    }

    public void addCommand(ICommand command){
        for (String commandRoot : command.getAliases())
        {
            commands.put(commandRoot.toLowerCase(), command);
            command.setCommandManager(this);
        }
    }
    public void removeCommand(ICommand command){
        for (String commandRoot : command.getAliases())
        {
            commands.remove(commandRoot.toLowerCase());
            command.setCommandManager(null);
        }
    }
}