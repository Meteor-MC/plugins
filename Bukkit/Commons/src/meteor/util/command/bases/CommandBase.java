package meteor.util.command.bases;// Written by Creeplays on 20.06.2016.


import meteor.util.Rank;
import meteor.util.command.CommandManager;
import meteor.util.entity.utils.UtilPlayer;
import meteor.util.recharge.Recharge;
import meteor.util.utils.UtilServer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public abstract class CommandBase implements ICommand
{
    private Rank requiredRank;
    private Rank[] specificRank;

    private List<String> aliases;

    protected Plugin plugin;
    protected String aliasUsed;
    protected CommandManager commandManager;
    protected String pluginName;
    protected String usage;

    public CommandBase(Plugin plugin, String pluginName, String usage,Rank requiredRank, String...aliases)
    {
        this.plugin = plugin;
        this.requiredRank = requiredRank;
        this.aliases = Arrays.asList(aliases);
        this.pluginName=pluginName;
        this.usage=usage;
    }

    public CommandBase(Plugin plugin, String pluginName, String usage,Rank requiredRank, Rank[] specificRank, String...aliases)
    {
        this.plugin = plugin;
        this.requiredRank = requiredRank;
        this.specificRank = specificRank;
        this.aliases = Arrays.asList(aliases);
        this.pluginName=pluginName;
        this.usage=usage;
    }

    public Collection<String> getAliases()
    {
        return aliases;
    }

    public void setAliasUsed(String alias)
    {
        aliasUsed = alias;
    }

    public Rank getRequiredRank()
    {
        return requiredRank;
    }

    public Rank[] getSpecificRanks()
    {
        return specificRank;
    }

    public void setCommandManager(CommandManager commandManager)
    {
        this.commandManager = commandManager;
    }

    protected void resetCommandCharge(Player caller)
    {
        Recharge.instance.recharge(caller, "Command");
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, String commandLabel, String[] args)
    {
        return null;
    }

    protected List<String> getMatches(String start, List<String> possibleMatches)
    {
        List<String> matches = new ArrayList<>();

        for (String possibleMatch : possibleMatches)
        {
            if (possibleMatch.toLowerCase().startsWith(start.toLowerCase()))
                matches.add(possibleMatch);
        }

        return matches;
    }

    @SuppressWarnings("rawtypes")
    protected List<String> getMatches(String start, Enum[] numerators)
    {
        List<String> matches = new ArrayList<>();

        for (Enum e : numerators)
        {
            String s = e.toString();
            if (s.toLowerCase().startsWith(start.toLowerCase()))
                matches.add(s);
        }

        return matches;
    }

    protected List<String> getPlayerMatches(Player sender, String start)
    {
        List<String> matches = new ArrayList<>();

        for (Player player : UtilServer.getPlayers())
        {
            if (sender.canSee(player) && player.getName().toLowerCase().startsWith(start.toLowerCase()))
            {
                matches.add(player.getName());
            }
        }

        return matches;
    }
    protected void answerPlayer(Player player, String answer){
        UtilPlayer.sendCommandOutput(player,this.pluginName,answer);
    }
    protected void printUsage(Player player){
        UtilPlayer.sendCommandOutput(player,this.pluginName,usage);
    }

}

