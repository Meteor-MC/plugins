package meteor.util.command.bases;// Written by Creeplays on 20.06.2016.

import meteor.util.Rank;
import meteor.util.command.CommandManager;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.List;

public interface ICommand
{
    void setCommandManager(CommandManager commandManager);
    void Execute(Player caller, String[] args);

    Collection<String> getAliases();

    void setAliasUsed(String name);

    Rank getRequiredRank();
    Rank[] getSpecificRanks();

    List<String> onTabComplete(CommandSender sender, String commandLabel, String[] args);
}

