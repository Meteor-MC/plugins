package meteor.util;// Written by Creeplays on 16.06.2016.

import org.bukkit.Location;
import org.bukkit.World;

public class SimpleLocation {
    public double x;
    public double y;
    public double z;

    public SimpleLocation(double x, double y, double z) {
        this.x=x;
        this.y=y;
        this.z=z;
    }

    public static Location fromSimpleLocation(SimpleLocation sl,World w){
        return new Location(w,sl.x,sl.y,sl.z);
    }
    public static SimpleLocation toSimpleLocation(Location l){
        return new SimpleLocation(l.getX(),l.getY(),l.getZ());
    }
}
