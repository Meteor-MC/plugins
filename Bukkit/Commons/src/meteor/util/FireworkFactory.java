package meteor.util;

import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * Created by Creeplays on 29.06.2016.
 */
public class FireworkFactory {
    public static void spawn(Location location, FireworkEffect effect, Player... players) {
        CustomEntityFirework.spawn(location , effect , players);
    }
}

