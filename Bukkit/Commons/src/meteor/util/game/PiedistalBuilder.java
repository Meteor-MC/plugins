package meteor.util.game;

import meteor.util.WorldRestorer;
import meteor.util.enums.EnumColor;
import meteor.util.userInterfaces.ClickableMob;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.plugin.Plugin;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Creeplays on 22.06.2016.
 */
public class PiedistalBuilder {
    Plugin plugin;
    WorldRestorer worldRestorer;
    World world;
    Set<ClickableMob> mobs;

    public PiedistalBuilder(Plugin plugin, World world){
        this.plugin=plugin;
        this.mobs=new HashSet<>();
        this.worldRestorer=new WorldRestorer(world);
        this.world=world;
    }

    public void buildPiedistal(Location location,EnumColor color){
        //Center of first layer
        final Double x=location.getX();
        final Double y=location.getY();
        final Double z=location.getZ();
        final Byte data=color.getDyeColor().getData();

        //First layer
        Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
            @Override
            public void run() {
                worldRestorer.setWall(x - 1, y, z - 1, x + 1, y, z + 1, Material.STAINED_GLASS,data);
                worldRestorer.setBlock(x,y,z,Material.GLOWSTONE);
            }
        },40);
        Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
            @Override
            public void run() {
                worldRestorer.setWall(x-1,y+1,z-1,x+1,y+1,z+1,Material.STONE_SLAB2);
                worldRestorer.setBlock(x,y+1,z,Material.NETHER_BRICK);
            }
        },80);
    }
    public void buildPiedistal(Double x,Double y,Double z, EnumColor color){
        buildPiedistal(new Location(world,x,y,z),color);
    }

    /**
     * Builds a piedistal with a mob on it.
     * @param location Location of a center of piedistal
     * @param color Color of a piedistal
     * @param teamName Name of a team. If not declared, default will be used
     * @param handler function that will be executed on mob click
     * @param entityType Type of entity
     */
    public void buildTeamJoinPiedistal(final Location location,final EnumColor color, final String teamName, final ClickableMob.MobClickEventHandler handler, final EntityType entityType){
        buildPiedistal(location,color);
        Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
            @Override
            public void run() {
                mobs.add(new ClickableMob(plugin, handler, entityType, color.getChatColor()+(teamName!=null?teamName:color.getTeamName()),location.clone().add(0,2,0),180));
            }
        },120);
    }
    public void buildTeamJoinPiedistal(Double x,Double y,Double z,EnumColor color, String teamName, ClickableMob.MobClickEventHandler handler, EntityType entityType){
        buildTeamJoinPiedistal(new Location(world,x,y,z),color,teamName,handler,entityType);
    }

    public void destroy(){
        worldRestorer.restore();
        for (ClickableMob mob : mobs) {
            mob.destroy();
        }
        worldRestorer.destroy();
        this.worldRestorer=null;
        this.world=null;
        this.plugin=null;
    }
}
