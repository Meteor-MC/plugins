package meteor.util.game;


import meteor.util.utils.UtilBlockText;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Creeplays on 23.06.2016.
 */
public class TextBuilder {
	Map<String,Collection<Block>> texts=new HashMap<>();
	World world;

	public TextBuilder(World world){
		this.world=world;
	}

	public void buildText(String label,String text,Location location,BlockFace blockFace,int id,byte data,UtilBlockText.TextAlign textAlign){
		texts.put(label, UtilBlockText.MakeText(text, location, blockFace, id, data, textAlign, true));
	}
	public void remove(String label){
		if(texts.get(label)==null)
			return;
		for (Block block : texts.get(label)) {
			block.setType(Material.AIR,false);
		}
	}
	public void remove(){
		for (String s : texts.keySet()) {
			remove(s);
		}
		texts.clear();
	}
}
