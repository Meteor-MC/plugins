package meteor.util.game.leaders;

import meteor.util.utils.UtilMisc;

/**
 * Created by Creeplays on 24.06.2016.
 */
public enum LeaderDescType {
    ALL("Обычный","игрок"),
    YOUTUBE("Ютюбер","");

    String line1;
    String line2;

    LeaderDescType(String line1,String line2){
        this.line1=line1;
        this.line2=line2;
    }
    public String getLine1(){
        return line1;
    }
    public String getLine2(String data){
        if(this==LeaderDescType.ALL)
            return line2;
        if(this==LeaderDescType.YOUTUBE)
            return UtilMisc.getYtSubCount(data);
        throw new Error("Unknown!");
    }
}
