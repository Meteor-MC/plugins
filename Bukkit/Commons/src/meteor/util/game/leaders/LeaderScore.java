package meteor.util.game.leaders;

/**
 * Created by Creeplays on 24.06.2016.
 */
public class LeaderScore<T> {
    String name;
    T score;

    public LeaderScore(String name, T score){
        this.name=name;
        this.score=score;
    }

    public String getName(){
        return name;
    }
    public String getScore(){
        return score.toString();
    }
}
