package meteor.util.game.leaders;

import meteor.util.constants.C;
import meteor.util.update.UpdateEvent;
import meteor.util.update.UpdateType;
import meteor.util.utils.UtilWorld;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;

/**
 * Created by Creeplays on 24.06.2016.
 */
public class LeaderPiedistal implements Listener {
    private Location playerLoc;
    private Location sign1;
    private Location sign2;
    private NPC npc;
    private LeaderDescType type;
    private String typeLink;
    private int scoreCount;
    private int currentScore=0;
    private LeaderScore<Integer>[] scores;
    private String name;

    /**
     * Spawns a new leader throne
     * @param playerLoc Location of fake player
     * @param sign1 First sign that displays stats
     * @param sign2 Second, that displays player info
     * @param name Name of a player
     * @param type if YOUTUBE paste https://socialblade.com/youtube/user/{@link #name}/realtime in typeLink
     * @param typeLink see {@link #type}
     * @param scores Array of {@link LeaderScore}
     */

    public LeaderPiedistal(Location playerLoc, Location sign1, Location sign2,String name,LeaderDescType type,String typeLink,LeaderScore<Integer>... scores) {
        this.playerLoc = playerLoc;
        this.sign1 = sign1;
        this.sign2 = sign2;
        this.type=type;
        this.scoreCount=scores.length;
        this.typeLink=typeLink;
        this.scores=scores;
        this.name=name;

        npc=CitizensAPI.getNPCRegistry()
                .createNPC(EntityType.PLAYER,"_CreeplayS_");
        npc.spawn(playerLoc);
        npc.setName("name");

        throw new Error("FixME!");

        //UtilCore.registerListener(this);
    }


    @EventHandler
    public void changeSign1(UpdateEvent event){
        if(!event.getType().equals(UpdateType.SLOW))
            return;
        UtilWorld.setSignText(sign1.getBlock(),0, C.BoldStrike+"===============");
        UtilWorld.setSignText(sign1.getBlock(),1, scores[currentScore].getName());
        UtilWorld.setSignText(sign1.getBlock(),2, scores[currentScore].getScore());
        UtilWorld.setSignText(sign1.getBlock(),3, C.BoldStrike+"===============");

        currentScore++;
        if(currentScore>=scoreCount)
            currentScore=0;
    }
    @EventHandler
    public void changeSign2(UpdateEvent event){
        if(!event.getType().equals(UpdateType.SLOWER))
            return;
        UtilWorld.setSignText(sign2.getBlock(),0, C.BoldStrike+"===============");
        UtilWorld.setSignText(sign2.getBlock(),1, type.getLine1());
        //Fetch info, if exists. //TODO: make async
        UtilWorld.setSignText(sign2.getBlock(),2, type.getLine2(typeLink));
        UtilWorld.setSignText(sign2.getBlock(),3, C.BoldStrike+"===============");
    }

    /**
     * Sets a yaw of leaderThrone NPC
     * @param yaw needed yaw
     */
    public void setYaw(float yaw){
        Location location=npc.getStoredLocation();
        location.setYaw(yaw);
        npc.teleport(location, PlayerTeleportEvent.TeleportCause.PLUGIN);
    }
}
