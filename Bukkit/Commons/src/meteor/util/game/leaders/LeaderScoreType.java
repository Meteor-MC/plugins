package meteor.util.game.leaders;

/**
 * Created by Creeplays on 25.06.2016.
 */
public enum LeaderScoreType {
    KILLS("Убийств"),
    DEATHS("Смертей"),
    WINS("Побед"),
    SCORE("Очков");

    private String name;

    LeaderScoreType(String name){
        this.name=name;
    }

    public String getName() {
        return name;
    }
}
