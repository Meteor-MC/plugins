package meteor.util;// Written by Creeplays on 16.06.2016.

import meteor.util.utils.UtilMap;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.BlockState;

import java.util.ArrayList;
import java.util.Collections;

public class WorldRestorer {
    ArrayList<BlockState> restoreList=new ArrayList<>();
    World world;

    public WorldRestorer(World world){
        this.world=world;
    }
    public void setBlock(Location location,Material material,byte data){
        System.out.println("Set block at "+location.toString());
        restoreList.add(this.world.getBlockAt(location).getState());
        UtilMap.ChunkBlockChange(world,location.getBlockX(),location.getBlockY(),location.getBlockZ(),material.getId(),data,true);
        //Block block=this.world.getBlockAt(location);
        //block.setType(material);
        //block.setData(data);
    }
    public void setBlock(Location location,Material material){
        setBlock(location,material, (byte)0);
    }
    public void setBlock(double x,double y, double z, Material material, byte data){
        setBlock(new Location(this.world,x,y,z),material,data);
    }
    public void setBlock(double x,double y, double z, Material material){
        setBlock(new Location(this.world,x,y,z),material,(byte)0);
    }

    public void setCuboid(double x1,double y1, double z1,double x2,double y2, double z2,Material material, byte data){
        double minx=Math.min(x1,x2);
        double maxx=Math.max(x1,x2);

        double miny=Math.min(y1,y2);
        double maxy=Math.max(y1,y2);

        double minz=Math.min(z1,z2);
        double maxz=Math.max(z1,z2);

        for (double y=miny;y<=maxy;y++)
            for (double x=minx;x<=maxx;x++)
                for (double z=minz;z<=maxz;z++)
                    setBlock(x,y,z,material,data);
    }
    public void setCuboid(double x1,double y1, double z1,double x2,double y2, double z2,Material material){
        setCuboid(x1,y1,z1,x2,y2,z2,material,(byte)0);
    }

    public void setWall(double x1,double y1, double z1,double x2,double y2, double z2,Material material, byte data){
        double minx=Math.min(x1,x2);
        double maxx=Math.max(x1,x2);

        double miny=Math.min(y1,y2);
        double maxy=Math.max(y1,y2);

        double minz=Math.min(z1,z2);
        double maxz=Math.max(z1,z2);

        for (double y=miny;y<=maxy;y++)
            for (double x=minx;x<=maxx;x++)
                for (double z=minz;z<=maxz;z++)
                    if(x==maxx||x==minx||z==maxz||z==minz)
                        setBlock(x,y,z,material,data);
    }
    public void setWall(double x1,double y1, double z1,double x2,double y2, double z2,Material material){
        setWall(x1,y1,z1,x2,y2,z2,material,(byte)0);
    }

    public void restore(){
        Collections.reverse(restoreList);
        for (BlockState blockState : restoreList) {
            blockState.update(true,false);
        }
        restoreList.clear();
    }
    public void set0(){
        for (BlockState blockState : restoreList) {
            blockState.setType(Material.AIR);
            blockState.update(true,false);
        }
        restoreList.clear();
    }

    public void destroy() {
        world = null;
        restoreList=null;
    }
}
