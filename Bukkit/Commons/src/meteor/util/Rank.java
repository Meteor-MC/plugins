package meteor.util;

import meteor.util.account.AccountManager;
import meteor.util.constants.C;
import meteor.util.constants.F;
import meteor.util.entity.utils.UtilPlayer;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Creeplays on 15.06.2016.
 */
public enum Rank {

    ADMIN   (999999999,     C.cRed,         "Админ"),
    DEV     (999999999,     C.cBlue,        "Разработчик"),
    BUILDER (13,            C.cDPurple,     "Строитель"),
    ALL     (1,             C.cWhite,       "Игрок"),
    BANNED  (0,             C.cDGray,       "Забанен");

    private Integer privNum;
    private String color;
    private String name;

    Rank(int privNum, String color, String name) {
        this.privNum=privNum;
        this.color=color;
        this.name=name;
    }

    public Integer getNum(){
        return this.privNum;
    }
    public String getColor(){return this.color;}
    public String getName(){return this.name;}
    public boolean checkRank(Player player, Rank rank, Rank[] specific, boolean inform){
        if (specific != null)
        {
            for (Rank curRank : specific)
            {
                if (compareTo(curRank) == 0)
                {
                    return true;
                }
            }
        }

        if (compareTo(rank) <= 0)
            return true;

        if (inform)
        {
            UtilPlayer.message(player, F.main("Права","Это действие требует более высокого ранга!"));
        }

        return false;
    }

    public static Map<String,Rank> rankMap=new HashMap<>();
    public static Rank getRank(Player player){
        return AccountManager.instance.getRank(player.getName());
    }
    public static String getColor(Player player){
        return getRank(player).getColor();
    }
}
