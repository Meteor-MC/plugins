package meteor.util.chat.commands;// Written by Creeplays on 20.06.2016.

import meteor.util.Rank;
import meteor.util.command.bases.CommandBase;
import meteor.util.utils.UtilServer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class cmd_broadcast extends CommandBase {
    public cmd_broadcast(Plugin plugin) {
        super(plugin,"Чат","/broadcast <сообщение>",Rank.ADMIN,"broadcast","bc","b");
    }

    @Override
    public void Execute(Player caller, String[] args) {
        if (args.length == 0) {
            printUsage(caller);
            return;
        }

        String announcement = "";

        for (String arg : args)
            announcement += arg + " ";

        if (announcement.length() > 0)
            announcement = announcement.substring(0, announcement.length() - 1);

        UtilServer.broadcast(caller.getName(), announcement);
    }
}
