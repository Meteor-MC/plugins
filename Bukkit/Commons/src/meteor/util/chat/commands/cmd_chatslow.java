package meteor.util.chat.commands;// Written by Creeplays on 20.06.2016.

import meteor.util.Rank;
import meteor.util.chat.ChatManager;
import meteor.util.command.bases.CommandBase;
import meteor.util.constants.F;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class cmd_chatslow extends CommandBase
{
    public cmd_chatslow(Plugin plugin) {
        super(plugin,"Чат","/chatslow <время в секундах>",Rank.ADMIN,"chatslow","cs");
    }

    @Override
    public void Execute(Player caller, String[] args) {
        if (args.length != 1) {
            printUsage(caller);
            return;
        }
        try {
            int seconds = Integer.parseInt(args[0]);
            if (seconds < 0) {
                answerPlayer(caller,"Количество секунд должно быть положительным");
                return;
            }
            ChatManager.instance.setChatSlow(seconds,true);
            answerPlayer(caller,"Замедление установлено на " + F.time(seconds + " сек"));
        } catch (Exception e) {
            printUsage(caller);
        }
    }
}

