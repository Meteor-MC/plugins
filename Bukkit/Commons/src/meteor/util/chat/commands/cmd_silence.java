package meteor.util.chat.commands;// Written by Creeplays on 20.06.2016.

import meteor.util.Rank;
import meteor.util.chat.ChatManager;
import meteor.util.command.bases.CommandBase;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class cmd_silence extends CommandBase
{
    public cmd_silence(Plugin plugin) {
        super(plugin,"Чат","/silence <время в секундах>",Rank.ADMIN,"silence","s");
    }

    @Override
    public void Execute(Player caller, String[] args) {
        try
        {
            if (args.length == 0)
            {
                if (ChatManager.instance.Silenced() != 0)
                {
                    ChatManager.instance.Silence(0, true);
                }
                else
                {
                    ChatManager.instance.Silence(-1, true);
                }
            }

            else
            {
                long time = (long) (Double.valueOf(args[0]) * 3600000);

                ChatManager.instance.Silence(time, true);
            }
        }
        catch (Exception e)
        {
            answerPlayer(caller,"Неверное время.");
        }
    }
}
