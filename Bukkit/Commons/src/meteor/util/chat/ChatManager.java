package meteor.util.chat;// Written by Creeplays on 20.06.2016.

import meteor.util.MiniPlugin;
import meteor.util.Rank;
import meteor.util.account.AccountManager;
import meteor.util.chat.commands.cmd_broadcast;
import meteor.util.chat.commands.cmd_chatslow;
import meteor.util.chat.commands.cmd_silence;
import meteor.util.chat.data.MessageData;
import meteor.util.constants.C;
import meteor.util.constants.F;
import meteor.util.entity.utils.UtilPlayer;
import meteor.util.recharge.Recharge;
import meteor.util.update.UpdateEvent;
import meteor.util.update.UpdateType;
import meteor.util.utils.UtilServer;
import meteor.util.utils.UtilText;
import meteor.util.utils.UtilTime;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;
import java.util.UUID;

public class ChatManager extends MiniPlugin {
    public static ChatManager instance;
    public static void initialize(Plugin plugin){
        instance=new ChatManager(plugin);
    }

    private String[] hackusations = {"hack", "hax", "hacker", "hacking", "cheat", "cheater", "cheating", "forcefield", "flyhack", "flyhacking", "autoclick", "aimbot", "аим","killaura","аура","хакер","чит"};
    private int chatSlow = 0;
    private long silenced = 0;
    private boolean threeSecondDelay = true;
    private HashMap<UUID, MessageData> playerLastMessage = new HashMap<>();

    private ChatManager(Plugin plugin) {
        super("Чат", plugin);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void filterChat(AsyncPlayerChatEvent event) {
        if (event.isCancelled())
            return;

        if (event.isAsynchronous()) {
            String filteredMessage = getFilteredMessage(event.getPlayer(), event.getMessage());
            System.out.println(filteredMessage);
            for (Player player : UtilServer.getPlayers()) {
                UtilPlayer.message(player,F.chat(event.getPlayer(),filteredMessage));
            }

            event.setCancelled(true);
        }
    }
    @EventHandler(priority = EventPriority.LOWEST)
    public void HandleChat(AsyncPlayerChatEvent event) {
        if (event.isCancelled())
            return;

        Player sender = event.getPlayer();

        if (SilenceCheck(sender)) {
            event.setCancelled(true);
            return;
        } else if (AccountManager.instance.getRank(sender.getName()).equals(Rank.ALL)&&!Recharge.instance.use(sender, "Чат", 3000, false, false)&&threeSecondDelay) {
            UtilPlayer.message(sender, C.cYellow + "Ты можешь писать сообщение только раз в 3 секунды.");
            UtilPlayer.message(sender, C.cYellow + "Купи ранг на "+F.elem("meteor-mc.ru")+" для того что бы убрать задержку");
            event.setCancelled(true);
        } else if (playerLastMessage.containsKey(sender.getUniqueId())) {
            MessageData lastMessage = playerLastMessage.get(sender.getUniqueId());
            long chatSlowTime = 1000L * chatSlow;
            long timeDiff = System.currentTimeMillis() - lastMessage.getTimeSent();
            if (timeDiff < chatSlowTime) {
                UtilPlayer.message(sender, F.main("Чат", "Задержка чата включена. Пожалуйста подожди " + F.time(UtilTime.convertString(chatSlowTime - timeDiff, 1, UtilTime.TimeUnit.FIT))));
                event.setCancelled(true);
            } else if (UtilText.isStringSimilar(event.getMessage(), lastMessage.getMessage(), 0.8f)) {
                UtilPlayer.message(sender, F.main("Чат", "Не повторяйся!"));
                event.setCancelled(true);
            }
        }

        if (!event.isCancelled()) {
            playerLastMessage.put(sender.getUniqueId(), new MessageData(event.getMessage()));
        }
    }
    @EventHandler
    public void playerQuit(PlayerQuitEvent event) {
        playerLastMessage.remove(event.getPlayer().getUniqueId());
    }

    @EventHandler
    public void SilenceUpdate(UpdateEvent event) {
        if (event.getType() != UpdateType.FAST)
            return;

        SilenceEnd();
    }

    public void setChatSlow(int seconds, boolean inform) {
        if (seconds < 0)
            seconds = 0;

        chatSlow = seconds;

        if (inform) {
            if (seconds == 0)
                UtilServer.broadcast(F.main("Чат", "Замедление чата отключено"));
            else
                UtilServer.broadcast(F.main("Чат", "Замедление чата установлено на " + F.time(UtilTime.convertString(seconds*1000,1, UtilTime.TimeUnit.FIT))));
        }
    }
    public void Silence(long duration, boolean inform) {
        // Set Silenced
        if (duration > 0)
            silenced = System.currentTimeMillis() + duration;
        else
            silenced = duration;

        if (!inform)
            return;

        // Announce
        if (duration == -1)
            UtilServer.broadcast(F.main("Чат", "Чат отключён " + F.time("навсегда") + "."));
        else if (duration == 0)
            UtilServer.broadcast(F.main("Чат", "Чат включён."));
        else
            UtilServer.broadcast(F.main("Чат", "Чат отключён на " + F.time(UtilTime.MakeStr(duration, 1))
                    + "."));
    }
    public void SilenceEnd() {
        if (silenced <= 0)
            return;

        if (System.currentTimeMillis() > silenced)
            Silence(0, true);
    }
    public boolean SilenceCheck(Player player) {
        SilenceEnd();

        if (silenced == 0)
            return false;
        if(Rank.getRank(player).checkRank(player, Rank.ADMIN, new Rank[]{}, false))
            return false;

        if (silenced == -1)
            UtilPlayer.message(player, F.main(getName(), "Чат отключён."));
        else
            UtilPlayer.message(
                    player,
                    F.main(getName(),
                            "Чат отключён на "
                                    + F.time(UtilTime.MakeStr(silenced - System.currentTimeMillis(), 1)) + "."));

        return true;
    }
    public String getFilteredMessage(Player player, String originalMessage) {
        return originalMessage;
    }
    public long Silenced() {
        return silenced;
    }
    public void setThreeSecondDelay(boolean b) {
        threeSecondDelay = b;
    }

    @Override
    public void enable() {

    }
    @Override
    public void disable() {

    }
    @Override
    public void addCommands() {
        addCommand(new cmd_silence(getPlugin()));
        addCommand(new cmd_broadcast(getPlugin()));
        addCommand(new cmd_chatslow(getPlugin()));
    }
}
