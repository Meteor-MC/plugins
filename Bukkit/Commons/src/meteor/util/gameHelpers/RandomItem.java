package meteor.util.gameHelpers;// Written by Creeplays on 17.06.2016.

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

public class RandomItem
{
    private int amount;
    private ItemStack item;
    private int min, max;

    public RandomItem(ItemStack item, int amount)
    {
        this(item, amount, item.getAmount(), item.getAmount());
    }

    public RandomItem(ItemStack item, int amount, int minStackSize, int maxStackSize)
    {
        this.amount = amount;
        this.item = item;
        this.min = minStackSize;
        this.max = maxStackSize;
    }

    public RandomItem(Material material, int amount)
    {
        this(material, amount, 1, 1);
    }

    public RandomItem(Material material, int amount, int minStackSize, int maxStackSize)
    {
        this.amount = amount;
        this.item = new ItemStack(material);
        this.min = minStackSize;
        this.max = maxStackSize;
    }

    public int getAmount()
    {
        return amount;
    }

    public ItemStack getItemStack()
    {
        item.setAmount((new Random().nextInt(Math.max(1, (max - min) + 1)) + min));

        return item;
    }
}
