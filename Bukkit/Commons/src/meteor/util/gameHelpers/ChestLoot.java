package meteor.util.gameHelpers;// Written by Creeplays on 17.06.2016.

import meteor.util.utils.UtilMath;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class ChestLoot
{
    private ArrayList<RandomItem> randomItems = new ArrayList<>();
    private int totalLoot;
    private boolean unbreakableLoot;

    public ChestLoot()
    {
        this(false);
    }

    public ChestLoot(boolean unbreakableLoot)
    {
        this.unbreakableLoot = unbreakableLoot;
    }

    public void cloneLoot(ChestLoot loot)
    {
        this.totalLoot += loot.totalLoot;
        this.randomItems.addAll(loot.randomItems);
    }

    public ItemStack getLoot()
    {
        int no = UtilMath.r(totalLoot);

        for (RandomItem item : randomItems)
        {
            no -= item.getAmount();

            if (no < 0)
            {
                ItemStack itemstack = item.getItemStack();

                if (unbreakableLoot && itemstack.getType().getMaxDurability() > 16)
                {
                    ItemMeta meta = itemstack.getItemMeta();
                    meta.spigot().setUnbreakable(true);
                    itemstack.setItemMeta(meta);
                }

                return itemstack;
            }
        }

        return null;
    }

    public void addLoot(ItemStack item, int amount)
    {
        addLoot(item, amount, item.getAmount(), item.getAmount());
    }

    public void addLoot(ItemStack item, int amount, int minStackSize, int maxStackSize)
    {
        addLoot(new RandomItem(item, amount, minStackSize, maxStackSize));
    }

    public void addLoot(Material material, int amount)
    {
        addLoot(material, amount, 1, 1);
    }

    public void addLoot(Material material, int amount, int minStackSize, int maxStackSize)
    {
        addLoot(new ItemStack(material), amount, minStackSize, maxStackSize);
    }

    public void addLoot(RandomItem item)
    {
        totalLoot += item.getAmount();
        randomItems.add(item);
    }
}

