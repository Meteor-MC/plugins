package meteor.util.database;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

//REDIS

public class FDatabase {
    private static JedisPool pool;
    private static Jedis resource;
    static {
        String host=System.getenv("REDIS_HOST");
        String port=System.getenv("REDIS_PORT");
        String password=System.getenv("REDIS_PASSWORD");
        if(port==null||host==null||password==null){
            System.err.println("Env vars REDIS_HOST/REDIS_PORT/REDIS_PASSWORD is not defined!");
            System.exit(0);
        }
        pool = new JedisPool(new JedisPoolConfig(),host,Integer.valueOf(port),1000,password);
        resource=pool.getResource();
        resource.select(1);
    }
    public static Jedis getResource(){
        return resource;
    }
    public static JedisPool getPool(){
        return pool;
    }

}
