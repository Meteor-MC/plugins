package meteor.util.database;

import com.mongodb.async.client.MongoClient;
import com.mongodb.async.client.MongoClients;
import com.mongodb.async.client.MongoCollection;
import com.mongodb.async.client.MongoDatabase;
import org.bson.Document;

/**
 * Copyright 2016 Meteor-MC
 * All rights reserved
 * Source code for internal use only
 * Created for ${PACKAGE_NAME}
 * Created by Creeplays on 02/07/16.
 */


public class Database {
    private static MongoDatabase database;
    static {
        String url=System.getenv("MONGO_URL");
        String db=System.getenv("MONGO_DB");
        if(url==null||db==null) {
            System.err.println("Env vars MONGO_URL/MONGO_DB is not defined!");
            System.exit(0);
        }
        MongoClient mongoClient = MongoClients.create(url);
        database= mongoClient.getDatabase(db);
    }

    private static MongoCollection<Document> getCollection(String name){
        return database.getCollection(name);
    }

    private static MongoCollection<Document> accountCollection;

    public static MongoCollection<Document> getAccountCollection(){
        if(accountCollection==null)
            accountCollection=getCollection("accountData");
        return accountCollection;
    }
}
