package meteor.util.reciepes;

import meteor.util.MiniPlugin;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.plugin.Plugin;

import java.util.Iterator;
import java.util.Map;

import static meteor.util.utils.UtilServer.getServer;

/**
 * Copyright 2016 Meteor-MC
 * All rights reserved
 * Source code for internal use only
 * Created for meteor.util.reciepes
 * Created by Creeplays on 05.07.2016.
 */
public class RecipeManager extends MiniPlugin {

    public static RecipeManager instance;
    public void initialize(Plugin plugin){
        instance=new RecipeManager(plugin);
    }

    protected RecipeManager(Plugin plugin) {
        super("Менеджер рецептов", plugin);
    }

    @Override
    public void enable() {

    }

    @Override
    public void disable() {

    }

    @Override
    public void addCommands() {

    }

    public void removeRecipe(ShapedRecipe inputRecipe){
        Iterator<Recipe> it = getServer().recipeIterator();
        while(it.hasNext()){
            Recipe itRecipe = it.next();
            if(itRecipe instanceof ShapedRecipe){
                ShapedRecipe itShaped = (ShapedRecipe) itRecipe;

                Map<Character, ItemStack> m = itShaped.getIngredientMap();
                Map<Character, ItemStack> n = inputRecipe.getIngredientMap();

                if(m.values().containsAll(n.values())){
                    String[] list = itShaped.getShape();
                    String listString = list[0] + list[1] + list[2];

                    String[] list2 = inputRecipe.getShape();
                    String listString2 = list2[0] + list2[1] + list2[2];

                    for(int i = 0; i < listString.length(); i++){
                        if(!m.get(listString.charAt(i)).equals(n.get(listString2.charAt(i)))){
                            return;
                        }
                    }
                    it.remove();
                }
            }
        }
    }
    public void addRecipe(ShapedRecipe recipe){
        removeRecipe(recipe);
        getServer().addRecipe(recipe);
    }

}
