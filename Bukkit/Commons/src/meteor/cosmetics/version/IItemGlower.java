package meteor.cosmetics.version;

import org.bukkit.inventory.ItemStack;

public interface IItemGlower {
    ItemStack glow(ItemStack itemStack);
}
