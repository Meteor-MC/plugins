package meteor.cosmetics.version;

import meteor.cosmetics.cosmetics.mounts.Mount;

public interface IMounts {
    Class<? extends Mount> getSquidClass();
    Class<? extends Mount> getSpiderClass();
    Class<? extends Mount> getSlimeClass();
}
