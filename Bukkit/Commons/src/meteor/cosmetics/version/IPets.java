package meteor.cosmetics.version;

import meteor.cosmetics.cosmetics.pets.Pet;

public interface IPets {
    Class<? extends Pet> getPumplingClass();
    Class<? extends Pet> getCompanionCubeClass();
}
