package meteor.cosmetics.version;

public interface IModule {
    void enable();
    void disable();
}
