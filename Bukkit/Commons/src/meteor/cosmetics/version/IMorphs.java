package meteor.cosmetics.version;

import meteor.cosmetics.cosmetics.morphs.Morph;

public interface IMorphs {
    Class<? extends Morph> getElderGuardianClass();
}
