package meteor.cosmetics;

import meteor.cosmetics.config.MessageManager;
import meteor.cosmetics.config.SettingsManager;
import meteor.cosmetics.cosmetics.Category;
import meteor.cosmetics.cosmetics.emotes.EmoteType;
import meteor.cosmetics.cosmetics.gadgets.GadgetDiscoBall;
import meteor.cosmetics.cosmetics.gadgets.GadgetExplosiveSheep;
import meteor.cosmetics.cosmetics.gadgets.GadgetType;
import meteor.cosmetics.cosmetics.hats.Hat;
import meteor.cosmetics.cosmetics.morphs.MorphType;
import meteor.cosmetics.cosmetics.mounts.MountType;
import meteor.cosmetics.cosmetics.particleeffects.ParticleEffectType;
import meteor.cosmetics.cosmetics.pets.PetType;
import meteor.cosmetics.cosmetics.suits.SuitType;
import meteor.cosmetics.listeners.MainListener;
import meteor.cosmetics.listeners.PlayerListener;
import meteor.cosmetics.listeners.v1_9.PlayerSwapItemListener;
import meteor.cosmetics.manager.*;
import meteor.cosmetics.mysql.MySQLConnection;
import meteor.cosmetics.mysql.Table;
import meteor.cosmetics.run.FallDamageManager;
import meteor.cosmetics.run.InvalidWorldManager;
import meteor.cosmetics.util.BlockUtils;
import meteor.cosmetics.util.CustomConfiguration;
import meteor.cosmetics.util.SQLUtils;
import meteor.util.MiniPlugin;
import meteor.util.utils.UtilCore;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.*;

/**
 * Created by sacha on 03/08/15.
 */
public class UltraCosmetics extends MiniPlugin {

    public static UltraCosmetics instance;
    public static void initialize(Plugin plugin){
        instance=new UltraCosmetics("персонализация",plugin);
    }

    public List<GadgetDiscoBall> discoBalls = Collections.synchronizedList(new ArrayList<GadgetDiscoBall>());

    public List<GadgetExplosiveSheep> explosiveSheep = Collections.synchronizedList(new ArrayList<GadgetExplosiveSheep>());

    public boolean placeHolderColor;

    private boolean vaultLoaded;

    /**
     * Menu Listeners.
     */
    private Listener mainMenuListener,morphMenuListener;

    /**
     * Determines if Treasure Chest Money Loot enabled.
     */
    public static boolean moneyTreasureLoot,

    /**
     * Determines if Gadget Cooldown should be shown in action bar.
     */
    cooldownInBar,

    /**
     * Determines if Pet Renaming required Money.
     */
    petRenameMoney,

    /**
     * Should the GUI close after Cosmetic Selection?
     */
    closeAfterSelect;
    public static String customBackMenuCommand;

    /**
     * List of enabled categories.
     */
    public static List<Category> enabledCategories = new ArrayList<>();

    /**
     * The Configuration. (config.yml)
     */
    public static CustomConfiguration config;

    /**
     * Config File.
     */
    public static File file;

    /**
     * Economy, used only if Vault is enabled.
     */

    public static SQLUtils sqlUtils;

    /**
     * If true, plugin is outdated.
     */
    public static boolean outdated;

    /**
     * Last Version published on spigotmc.org.
     */
    public static String lastVersion;

    /**
     * If true, debug messages will be shown.
     */
    static boolean debug = false;

    /**
     * If true, the server is using Spigot and not CraftBukkit/Bukkit.
     */
    private static boolean usingSpigot = true,

    /**
     * True -> should execute custom commands when going back to main menu.
     */
    customCommandBackArrow;

    /**
     * {@code true} if NoteBlockAPI can be used, {@code false} otherwise.
     */
    private static boolean noteBlockAPIEnabled;

    /**
     * Determines if Ammo Use is enabled.
     */
    private static boolean ammoEnabled,

    /**
     * Determines if File Storage is enabled.
     */
    fileStorage = true;


    /**
     * Instance.
     */
    private static UltraCosmetics core;

    /**
     * Player Manager instance.
     */
    private static PlayerManager playerManager;

    /**
     * MySQL Connection.
     */
    public Connection co;

    /**
     * MySQL Table.
     */
    public Table table;

    /**
     * SQLLoader Manager instance
     */
    private static SQLLoaderManager sqlloader;

    /**
     * MySQL Stuff.
     */
    private MySQLConnection sql;

    protected UltraCosmetics(String moduleName, Plugin plugin) {
        super(moduleName, plugin);
    }

    /**
     * Called when plugin is enabled.
     */
    @Override
    public void enable() {
        String mcVersion = "1.8.8";

        try {
            mcVersion = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
        } catch (ArrayIndexOutOfBoundsException whatVersionAreYouUsingException) {
        }

        usingSpigot = true;

        playerManager = new PlayerManager();

        file = new File(UtilCore.getPlugin().getDataFolder(),"config.yml");

        if (!file.exists()) {
            file.getParentFile().mkdirs();
            //FileUtils.copy(getResource("config.yml"), file);
            log("Config file doesn't exist yet.");
            log("Creating Config File and loading it.");
        }

        config = CustomConfiguration.loadConfiguration(file);

        List<String> enabledWorlds = new ArrayList<>();
        for (World world : Bukkit.getWorlds())
            enabledWorlds.add(world.getName());
        config.addDefault("Enabled-Worlds", enabledWorlds, "List of the worlds", "where cosmetics are enabled!");

        config.set("Disabled-Items", null);

        if (!config.contains("TreasureChests.Loots.Gadgets")) {
            config.createSection("TreasureChests.Loots.Gadgets", "Chance of getting a GADGET", "This is different from ammo!");
            config.set("TreasureChests.Loots.Gadgets.Enabled", true);
            config.set("TreasureChests.Loots.Gadgets.Chance", 20);
            config.set("TreasureChests.Loots.Gadgets.Message.enabled", false);
            config.set("TreasureChests.Loots.Gadgets.Message.message", "%prefix% &6&l%name% found gadget %gadget%");
        }
        if (!config.contains("TreasureChests.Loots.Suits")) {
            config.createSection("TreasureChests.Loots.Suits");
            config.set("TreasureChests.Loots.Suits.Enabled", true);
            config.set("TreasureChests.Loots.Suits.Chance", 10);
            config.set("TreasureChests.Loots.Suits.Message.enabled", false);
            config.set("TreasureChests.Loots.Suits.Message.message", "%prefix% &6&l%name% found suit part: %suitw%");
        }

        if (!config.contains("Categories.Suits")) {
            config.createSection("Categories.Suits");
            config.set("Categories.Suits.Main-Menu-Item", "299:0");
            config.set("Categories.Suits.Go-Back-Arrow", true);
        }

        config.addDefault("Categories.Clear-Cosmetic-Item", "152:0", "Item where user click to clear a cosmetic.");
        config.addDefault("Categories.Previous-Page-Item", "368:0", "Previous Page Item");
        config.addDefault("Categories.Next-Page-Item", "381:0", "Next Page Item");
        config.addDefault("Categories.Back-Main-Menu-Item", "262:0", "Back to Main Menu Item");
        config.addDefault("Categories.Self-View-Item.When-Enabled", "381:0", "Item in Morphs Menu when Self View enabled.");
        config.addDefault("Categories.Self-View-Item.When-Disabled", "368:0", "Item in Morphs Menu when Self View disabled.");
        config.addDefault("Categories.Gadgets-Item.When-Enabled", "351:10", "Item in Gadgets Menu when Gadgets enabled.");
        config.addDefault("Categories.Gadgets-Item.When-Disabled", "351:8", "Item in Gadgets Menu when Gadgets disabled.");
        config.addDefault("Categories.Rename-Pet-Item", "421:0", "Item in Pets Menu to rename current pet.");
        config.addDefault("Categories.Close-GUI-After-Select", true, "Should GUI close after selecting a cosmetic?");
        config.addDefault("No-Permission.Custom-Item.Lore", Arrays.asList("", "&c&lYou do not have permission for this!", ""));
        config.addDefault("Categories.Back-To-Main-Menu-Custom-Command.Enabled", false);
        config.addDefault("Categories.Back-To-Main-Menu-Custom-Command.Command", "cc open custommenu.yml {player}");

        config.addDefault("Categories-Enabled.Suits", true, "Do you want to enable Suits category?");

        config.addDefault("Categories.Gadgets.Cooldown-In-ActionBar", true, "You wanna show the cooldown of", "current gadget in action bar?");

        //saveConfig();

        customCommandBackArrow = config.getBoolean("Categories.Back-To-Main-Menu-Custom-Command.Enabled");
        customBackMenuCommand = config.getString("Categories.Back-To-Main-Menu-Custom-Command.Command").replace("/", "");

        closeAfterSelect = config.getBoolean("Categories.Close-GUI-After-Select");

        log("Configuration loaded.");
        log("");

        core = this;

        /*log("Initializing module " + serverVersion);
        versionManager = new VersionManager(serverVersion);
        try {
            versionManager.load();
        } catch (ReflectiveOperationException e) {
            e.printStackTrace();
            log("No module found for " + serverVersion + " disabling");
        }
        versionManager.getModule().enable();*/
        log("Module initialized");
        log("");


        if (Bukkit.getPluginManager().getPlugin("NoteBlockAPI") != null) {
            log("");
            log("NoteBlockAPI loaded and hooked.");
            log("");
            noteBlockAPIEnabled = true;
        }


        log("");
        log("Registering Messages...");
        new MessageManager();
        log("Messages registered.");
        log("");

        registerListener(new PlayerListener());
        registerListener(new PlayerSwapItemListener());

        log("");

        String s = SettingsManager.getConfig().getString("Ammo-System-For-Gadgets.System");
        fileStorage = s.equalsIgnoreCase("file");
        placeHolderColor = SettingsManager.getConfig().getBoolean("ChatManager-Cosmetic-PlaceHolder-Color");
        ammoEnabled = SettingsManager.getConfig().getBoolean("Ammo-System-For-Gadgets.Enabled");
        cooldownInBar = SettingsManager.getConfig().getBoolean("Categories.Gadgets.Cooldown-In-ActionBar");

        Collections.addAll(enabledCategories, Category.values());

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        checkTreasureChests();

        log("Registering Cosmetics...");
        setupCosmeticsConfigs();

        enabledCategories.clear();
        for (Category c : Category.values()) {
            if (c == Category.MORPHS)
                if (!Bukkit.getPluginManager().isPluginEnabled("LibsDisguises"))
                    continue;
            if (c.isEnabled())
                enabledCategories.add(c);
        }

        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        log("Cosmetics Registered.");

        if (!Bukkit.getPluginManager().isPluginEnabled("LibsDisguises")) {
            log("");
            log("Morphs require Lib's Disguises!");
            log("");
            log("Morphs are disabling..");
            log("");
        }

        petRenameMoney = true;//SettingsManager.getConfig().getBoolean("Pets-Rename.Requires-Money.Enabled");

        initPlayers();

        Bukkit.getScheduler().runTaskTimerAsynchronously(UtilCore.getPlugin(), new FallDamageManager(), 0, 1);
        Bukkit.getScheduler().runTaskTimerAsynchronously(UtilCore.getPlugin(), new InvalidWorldManager(), 0, 5);

        log("");
        log("Registering listeners...");
        mainMenuListener = new MainMenuManager();
        registerListener(mainMenuListener);
        registerListener(new GadgetManager());
        registerListener(new PetManager());
        registerListener(new MountManager());
        registerListener(new ParticleEffectManager());
        registerListener(new PetManager());
        registerListener(new HatManager());
        registerListener(new SuitManager());
        registerListener(new EmoteManager());
        registerListener(new TreasureChestManager());
        registerListener(new MainListener());
        if (Bukkit.getPluginManager().isPluginEnabled("LibsDisguises")) {
            morphMenuListener = new MorphManager();
            registerListener(morphMenuListener);
        }
        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        log("Listeners registered.");
        log("");
        log("");
        log("UltraCosmetics successfully finished loading and is now enabled!");

        log("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
    }

    @Override
    public void addCommands() {

    }

    /**
     * Logs a message in console.
     *
     * @param object The message to log.
     */

    /**
     * Get a collection of all the CustomPlayers.
     *
     * @return
     */
    public static Collection<CustomPlayer> getCustomPlayers() {
        return playerManager.getPlayers();
    }

    /**
     * @return if ammo system is enabled, or not.
     */
    public boolean isAmmoEnabled() {
        return ammoEnabled;
    }

    /**
     * @return if NoteBlockAPI is loaded.
     */
    public boolean isNoteBlockAPIEnabled() {
        return noteBlockAPIEnabled;
    }

    /**
     * @return if file storage is used.
     */
    public boolean usingFileStorage() {
        return false;
    }

    /**
     * Debugs something.
     *
     * @param message The message to print.
     * @return if debug is turned on or off.
     */
    public static boolean debug(Object message) {
        if (debug) Bukkit.broadcastMessage("§c§lUC-DEBUG> §f" + message.toString());
        return debug;
    }

    /**
     * Gets the UltraCosmetics Plugin Object.
     *
     * @return
     */
    public static Plugin getInstance() {
        return UtilCore.getPlugin();
    }

    /**
     * Registers a listener.
     *
     * @param listenerClass The listener to register.
     */
    public void registerListener(Listener listenerClass) {
        Bukkit.getPluginManager().registerEvents(listenerClass, UtilCore.getPlugin());
    }

    /**
     * Gets the custom player of a player.
     *
     * @param player The player.
     * @return The CustomPlayer of player.
     */
    public static CustomPlayer getCustomPlayer(Player player) {
        return playerManager.getCustomPlayer(player);
    }

    /**
     * Gets the Custom Player Manager.
     *
     * @return the Custom Player Manager.
     */
    public static PlayerManager getPlayerManager() {
        return playerManager;
    }

    /**
     * Gets the SQLloader Manager
     *
     * @return the SQLloader Manager
     */
    public static SQLLoaderManager getSQLLoader() {
        return sqlloader;
    }


    /**
     * Removes color in a text.
     *
     * @param toFilter The text to filter.
     * @return The filtered text.
     */

    public static boolean usingSpigot() {
        return usingSpigot;
    }

    /**
     * Overrides config saving to keep comments.
     */

    /**
     * Initialize players.
     */
    private void initPlayers() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            playerManager.create(p);
            playerManager.getCustomPlayer(p).giveMenuItem();
        }
    }

    /**
     * Setup default Cosmetics config.
     */
    private void setupCosmeticsConfigs() {

        for (GadgetType gadgetType : GadgetType.values())
            GadgetType.gadgetTypes.add(gadgetType);
        for (MountType mountType : MountType.values())
            MountType.mountTypes.add(mountType);
        for (ParticleEffectType particleEffectType : ParticleEffectType.values())
            ParticleEffectType.enabled.add(particleEffectType);
        for (PetType petType : PetType.values())
            PetType.enabled.add(petType);
        for (MorphType morphType : MorphType.values())
             MorphType.enabled.add(morphType);
        for (Hat hat : Hat.values())
            Hat.enabled.add(hat);
        for (SuitType suit : SuitType.values())
            SuitType.enabled.add(suit);
        for (EmoteType emoteType : EmoteType.values())
            EmoteType.ENABLED.add(emoteType);
    }

    /**
     * Check Treasure Chests requirements.
     */
    private void checkTreasureChests() {
        moneyTreasureLoot = true;

    }


    /**
     * Called when plugin disables.
     */
    @Override
    public void disable() {
        if (morphMenuListener != null)
            ((MorphManager) morphMenuListener).dispose();

        if (playerManager != null)
            playerManager.dispose();
        try {
            BlockUtils.forceRestore();
        } catch (Exception e) {
        }
    }

    /**
     * Checks for new update.
     */
    private void checkForUpdate() {
        outdated = false;
    }

    public static void openMainMenuFromOther(Player whoClicked) {
        if (customCommandBackArrow)
            Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), customBackMenuCommand.replace("{player}", whoClicked.getName()));
        else
            MainMenuManager.openMenu(whoClicked);
    }
}
