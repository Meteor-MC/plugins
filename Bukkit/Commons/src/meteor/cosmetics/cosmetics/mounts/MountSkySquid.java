package meteor.cosmetics.cosmetics.mounts;

import meteor.cosmetics.util.MathUtils;
import org.bukkit.Effect;

import java.util.Random;
import java.util.UUID;

/**
 * Created by Sacha on 11/10/15.
 */
public class MountSkySquid extends MountCustomEntity {

    public MountSkySquid(UUID owner) {
        super(owner, MountType.SKYSQUID);

    }

    @Override
    protected void onUpdate() {
        Random random = new Random();
        for (int i = 0; i < 5; i++)
            getPlayer().getWorld().spigot().playEffect(getCustomEntity().getBukkitEntity().getLocation().add(MathUtils.randomDouble(-2, 2),
                  MathUtils.randomDouble(-1, 1.3), MathUtils.randomDouble(-2, 2)), Effect.POTION_BREAK, 0, 0, random.nextFloat(),
                  random.nextFloat(), random.nextFloat(), 1, 20, 64);
    }
}
