package meteor.cosmetics.cosmetics.mounts;

import meteor.cosmetics.util.PlayerUtils;
import meteor.util.entity.utils.UtilEntity;

import java.util.UUID;

public class MountHypeCart extends Mount {

    public MountHypeCart(UUID owner) {
        super(
                owner, MountType.HYPECART
        );
    }

    @Override
    protected void onUpdate() {
        if (entity.isOnGround())
            entity.setVelocity(PlayerUtils.getHorizontalDirection(getPlayer(), 7.6));
        UtilEntity.setClimb(entity);
    }

    @Override
    public void onClear() {
    }
}
