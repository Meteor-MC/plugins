package meteor.cosmetics.cosmetics.mounts;

import meteor.cosmetics.util.Particles;
import meteor.cosmetics.util.UtilParticles;
import meteor.util.entity.utils.UtilEntity;
import org.bukkit.entity.Horse;

import java.util.UUID;

/**
 * Created by sacha on 10/08/15.
 */
public class MountInfernalHorror extends Mount {

    public MountInfernalHorror(UUID owner) {
        super(owner, MountType.INFERNALHORROR);
    }

    @Override
    protected void onEquip() {
        if (entity instanceof Horse) {
            Horse horse = (Horse) entity;
            horse.setVariant(Horse.Variant.SKELETON_HORSE);
            variant = Horse.Variant.SKELETON_HORSE;
            horse.setVariant(Horse.Variant.SKELETON_HORSE);
            horse.setJumpStrength(0.7);
            UtilEntity.setHorseSpeed(horse, 0.4d);
        }
    }

    @Override
    protected void onUpdate() {
        UtilParticles.display(Particles.FLAME, 0.4f, 0.2f, 0.4f, entity.getLocation().clone().add(0, 1, 0), 5);
    }
}
