package meteor.cosmetics.cosmetics.mounts;

import org.bukkit.entity.Entity;

/**
 * Created by Sacha on 15/03/16.
 */
public interface IMountCustomEntity {

    Entity getEntity();

}
