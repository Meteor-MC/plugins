package meteor.cosmetics.cosmetics.suits;

/**
 * Created by Sacha on 20/12/15.
 */
public enum ArmorSlot {

    HELMET,
    CHESTPLATE,
    LEGGINGS,
    BOOTS

}
