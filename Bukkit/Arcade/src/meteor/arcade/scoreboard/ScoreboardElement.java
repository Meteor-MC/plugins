package meteor.arcade.scoreboard;// Written by Creeplays on 21.06.2016.

import java.util.ArrayList;

public abstract class ScoreboardElement
{
    public abstract ArrayList<String> GetLines();
}

