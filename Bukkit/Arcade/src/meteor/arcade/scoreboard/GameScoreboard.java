package meteor.arcade.scoreboard;// Written by Creeplays on 21.06.2016.

import meteor.util.constants.C;
import meteor.util.utils.UtilMath;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.ArrayList;
import java.util.HashSet;

public class GameScoreboard {
    private Scoreboard scoreboard;
    private Objective sideObjective;

    private ArrayList<ScoreboardElement> elements = new ArrayList<ScoreboardElement>();
    private String[] current = new String[15];

    private String title;
    private int shineIndex;
    private boolean shineDirection = true;

    public GameScoreboard() {
        title = "   METEOP   ";

        //Scoreboard
        scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();

        sideObjective = scoreboard.registerNewObjective("Obj" + UtilMath.r(999999999), "dummy");
        sideObjective.setDisplaySlot(DisplaySlot.SIDEBAR);
        sideObjective.setDisplayName(C.Bold + title);
    }
    public void setTitle(String title){
        if(this.title.equals(title))
            return;
        this.title=title;
        if(this.title.length()==title.length())
            return;
        this.shineIndex=0;
        this.shineDirection=true;
        //sideObjective.setDisplayName(C.Bold + title);
    }
    public void resetTitle(){
        sideObjective.setDisplayName(C.Bold + "   METEOP   ");
    }
    public Scoreboard getScoreboard() {
        return scoreboard;
    }
    public Objective getObjectiveSide() {
        return sideObjective;
    }
    public void updateTitle() {
        String out;

        if (shineDirection) {
            out = C.cGold + C.Bold;
        } else {
            out = C.cWhite + C.Bold;
        }

        for (int i = 0; i < title.length(); i++) {
            char c = title.charAt(i);

            if (shineDirection) {
                if (i == shineIndex)
                    out += C.cRed + C.Bold;

                if (i == shineIndex + 1)
                    out += C.cWhite + C.Bold;
            } else {
                if (i == shineIndex)
                    out += C.cRed + C.Bold;

                if (i == shineIndex + 1)
                    out += C.cGold + C.Bold;
            }


            out += c;
        }

        sideObjective.setDisplayName(out);

        shineIndex++;

        if (shineIndex == title.length() * 2) {
            shineIndex = 0;
            shineDirection = !shineDirection;
        }
    }
    public String parseTeamName(String name) {
        return name.substring(0, Math.min(16, name.length()));
    }
    public void createTeams() {

        scoreboard.registerNewTeam(parseTeamName("SPEC")).setPrefix(ChatColor.GRAY + "");

        //Team Groups
        throw new Error("Teams!");
        /*for (GameTeam team : Game.GetTeamList()) {
            System.out.println("Scoreboard Team: " + team.GetName().toUpperCase());
            if (team.GetDisplaytag()) {
                scoreboard.registerNewTeam(parseTeamName(team.GetName().toUpperCase())).setPrefix(team.GetColor() + C.Bold + team.GetName() + team.GetColor() + " ");
            } else {
                scoreboard.registerNewTeam(parseTeamName(team.GetName().toUpperCase())).setPrefix(team.GetColor() + "");
            }
        }*/

		/*
        //Base Groups
		for (Rank rank : Rank.values())
		{
			//scoreboard.registerNewTeam(parseTeamName(rank.Name + "SPEC")).setPrefix(ChatColor.GRAY + "");
		}

		//Team Groups
		for (GameTeam team : Game.GetTeamList())
		{
			System.out.println("Scoreboard Team: " + team.GetName().toUpperCase());

			for (Rank rank : Rank.values())
			{
				scoreboard.registerNewTeam(parseTeamName(rank.Name + team.GetName().toUpperCase())).setPrefix(team.GetColor() + "");
			}
		}
		*/
    }
    public void setPlayerTeam(Player player, String teamName) {
        for (Team team : scoreboard.getTeams())
            team.removePlayer(player);

        if (teamName == null)
            teamName = "";

        String team = parseTeamName(teamName);

        try {
            scoreboard.getTeam(team).addPlayer(player);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("ERROR ADDING PLAYER TO TEAM: " + team);
        }
    }
    public void resetScore(String line) {
        scoreboard.resetScores(line);
    }
    public String clean(String line) {
        if (line.length() > 16)
            line = line.substring(0, 16);

        return line;
    }
    public void write(String line) {
        line = clean(line);

        elements.add(new ScoreboardElementText(line));
    }
    public void writeOrdered(String key, String line, Integer value, boolean prependScore) {
        if (prependScore)
            line = C.cGold+value + " " + line;

        line = clean(line);

        for (ScoreboardElement elem : elements) {
            if (elem instanceof ScoreboardElementScores) {
                ScoreboardElementScores scores = (ScoreboardElementScores) elem;

                if (scores.IsKey(key)) {
                    scores.AddScore(line, value);
                    return;
                }
            }
        }

        elements.add(new ScoreboardElementScores(key, line, value, true));
    }
    public void writeBlank() {
        if(elements.size()<=15)
            elements.add(new ScoreboardElementText(" "));
    }
    public void fillEmpty(){
        while(elements.size()<=15)
            elements.add(new ScoreboardElementText(" "));
    }
    public void draw() {
        //Generate Lines
        ArrayList<String> newLines = new ArrayList<String>();

        for (ScoreboardElement elem : elements) {
            for (String line : elem.GetLines()) {
                //Ensure no duplicate lines
                while (true) {
                    boolean matched = false;

                    for (String otherLine : newLines) {
                        if (line.equals(otherLine)) {
                            line += ChatColor.RESET;
                            matched = true;
                        }
                    }

                    if (!matched)
                        break;
                }

                newLines.add(line);
            }
        }

        //Find Changes
        HashSet<Integer> toAdd = new HashSet<Integer>();
        HashSet<Integer> toDelete = new HashSet<Integer>();

        for (int i = 0; i < 15; i++) {
            //Delete Old Excess Row
            if (i >= newLines.size()) {
                if (current[i] != null) {
                    toDelete.add(i);
                }

                continue;
            }

            //Update or Add Row
            if (current[i] == null || !current[i].equals(newLines.get(i))) {
                toDelete.add(i);
                toAdd.add(i);
            }
        }

        //Delete Elements - Must happen before Add
        for (int i : toDelete) {
            //Remove Old Line at Index
            if (current[i] != null) {
                resetScore(current[i]);
                current[i] = null;
            }
        }

        //Add Elements
        for (int i : toAdd) {
            //Insert New Line
            String newLine = newLines.get(i);
            getObjectiveSide().getScore(newLine).setScore(15 - i);
            current[i] = newLine;
        }
    }
    public void reset() {
        elements.clear();
    }
}

