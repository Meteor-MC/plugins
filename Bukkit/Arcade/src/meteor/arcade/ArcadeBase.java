package meteor.arcade;// Written by Creeplays on 16.06.2016.

import meteor.arcade.managers.MapVoteManager;
import meteor.arcade.managers.PvpVoteManager;
import meteor.arcade.scoreboard.GameScoreboard;
import meteor.util.WrappedHashMap;
import meteor.util.constants.C;
import meteor.util.constants.F;
import meteor.util.entity.utils.UtilPlayer;
import meteor.util.enums.EnumColor;
import meteor.util.enums.EnumResourcePack;
import meteor.util.game.*;
import meteor.util.game.leaders.LeaderDescType;
import meteor.util.game.leaders.LeaderPiedistal;
import meteor.util.game.leaders.LeaderScore;
import meteor.util.update.UpdateType;
import meteor.util.userInterfaces.ClickableMob;
import meteor.util.MapInfo;
import meteor.util.userInterfaces.Title;
import meteor.util.utils.*;
import net.citizensnpcs.api.CitizensAPI;
import net.minecraft.server.v1_10_R1.*;
import org.bukkit.*;
import org.bukkit.World;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scoreboard.*;
import org.bukkit.scoreboard.Scoreboard;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class ArcadeBase implements Listener {
    private String gameName;

    private Plugin plugin;

    //Map voting stuff
    private String mapsFolder;
    private File[] mapFiles;
    private MapVoteManager mapVoteManager;
    private MapInfo mapInfo;
    private File mapFile;

    //PVP vote
    private PvpVoteManager pvpVoteManager;
    private boolean isOldPvp=true;

    //Scoreboards
    private ScoreboardManager scoreboardManager;
    private WrappedHashMap<Player,Scoreboard> playerScoreboards;

    //Teams
    private Integer teamCount=2;

    //Spectators
    private HashSet<Player> spectators=new HashSet<>();

    //Piedistals
    private PiedistalBuilder piedistalBuilder;

    //Lobby
    private TextBuilder lobbyTextBuilder;

    private GameState gameState;

    private Location spawnLocation;

    private GameScoreboard scoreboard;

    private String serverName;

    private Long timer;
    private Long mapVoteTime=(long)1000*30;

    private World hubWorld;

    public ArcadeBase(Plugin plugin, String gameName, String mapsFolder) {
        hubWorld=Bukkit.getWorld("Hub/Arcade");

        this.gameName = gameName;
        this.plugin=plugin;
        this.gameState=GameState.WaitingForPlayers;
        this.serverName="DEV0";
        this.playerScoreboards=new WrappedHashMap<>();
        this.scoreboardManager= UtilServer.getServer().getScoreboardManager();
        this.scoreboard=new GameScoreboard();
        this.spawnLocation=new Location(hubWorld,0,120,0);
        this.piedistalBuilder=new PiedistalBuilder(plugin,hubWorld);
        this.lobbyTextBuilder=new TextBuilder(hubWorld);

        CitizensAPI.getNPCRegistry().deregisterAll();
        while (CitizensAPI.getNPCRegistry().iterator().hasNext()){
            CitizensAPI.getNPCRegistry().iterator().next().destroy();
        }

        registerVoteManager(mapsFolder);
        getExternalIp();
        cleanLobbyFromMobs();
        startLeaderBoard();
    }

    public void getLeader(int id){

    }

    void updateScoreboard(GameScoreboard scoreboard){
        if(gameState==GameState.WaitingForPlayers) {
            scoreboard.setTitle("Ожидаем игроков...");
            scoreboard.writeOrdered("server_del", "", 1, false);
            scoreboard.writeOrdered("server_title", C.cDAqua+C.Bold+"Сервер:", 2, false);
            scoreboard.writeOrdered("server_name", C.Bold+this.serverName, 3, false);
            scoreboard.writeOrdered("game_title", C.cDAqua+C.Bold+"Игра:", 2, false);
            scoreboard.writeOrdered("game_name", C.Bold+this.gameName, 3, false);
            scoreboard.writeOrdered("minpl_del", "", 1, false);
            scoreboard.writeOrdered("minpl_title", C.cGreen+C.Bold+"Минимум:", 2, false);
            scoreboard.writeOrdered("minpl_name", Integer.toString(getMinPlayers()), 3, false);
            scoreboard.writeOrdered("cpl_del", "", 1, false);
            scoreboard.writeOrdered("cpl_title", C.cYellow+C.Bold+"Сейчас:", 2, false);
            scoreboard.writeOrdered("cpl_name", Integer.toString(getPlayerCount()), 3, false);
            scoreboard.writeOrdered("maxpl_del", "", 1, false);
            scoreboard.writeOrdered("maxpl_title", C.cRed+C.Bold+"Максимум:", 2, false);
            scoreboard.writeOrdered("maxpl_name", Integer.toString(UtilServer.getServer().getMaxPlayers()), 3, false);
            scoreboard.writeOrdered("fin_del", "", 1, false);
        }
        else if(gameState==GameState.Waiting){
            scoreboard.reset();
            MapInfo[] infos=mapVoteManager.getMapInfos();
            Integer[] votes=mapVoteManager.getMapVotes();
            scoreboard.setTitle("Голосование");
            scoreboard.writeOrdered("maplist_del", "", 1, false);
            scoreboard.writeOrdered("maplist_title",C.cYellow+C.Bold+"Карты:",2,false);
            for(Integer i=0;i<4;i++){
                scoreboard.writeOrdered("map", EnumColor.getById(i+5).getChatColor() + infos[i].name, votes[i], true);
            }
            scoreboard.writeOrdered("timer_del", "", 1, false);
            scoreboard.writeOrdered("timer_title", C.cYellow+C.Bold+"До окончания:", 1, false);
            scoreboard.writeOrdered("timer_name", F.time(UtilTime.elapsedString(timer,mapVoteTime)), 1, false);
            scoreboard.writeOrdered("fin_del", "", 1, false);
        }
    }
    void gameStateChange(){
        scoreboard.reset();
    }
    void stateUpdateCheck(){
        if(gameState==GameState.WaitingForPlayers&&getMinPlayers()<=getPlayerCount()) {
            timer=UtilTime.nowLong();
            gameState = GameState.Waiting;
            gameStateChange();
            giveVoteItem();
        }
        if(gameState==GameState.Waiting&&UtilTime.elapsed(timer,mapVoteTime)){
            isOldPvp=pvpVoteManager.finishVoting();
            Tuple<MapInfo,File> winMap=mapVoteManager.finishVoting();
            mapInfo=winMap.a();
            mapFile=winMap.b();
            teamCount=mapInfo.teamSpawn.size();
            Title title=new Title(C.cAqua+"Победила карта "+mapInfo.name);
            title.setSubtitle(C.cBlue+(!isOldPvp ? "Со старым режимом PVP" : "С новым режимом PVP"));
            title.broadcast();
            gameState=GameState.PreGame;
            gameStateChange();
            spawnPiedistals();
        }
    }
    void startLeaderBoard(){

        //5
        new LeaderPiedistal(
                new Location(hubWorld,-0.5,113.5,-2.5),
                new Location(hubWorld,-2,113,-3),
                new Location(hubWorld,0,113,-3),
                "F6CF",
                LeaderDescType.YOUTUBE,
                "",
                new LeaderScore<>("Общий счёт",12000),
                new LeaderScore<>("Убийств",13),
                new LeaderScore<>("Смертей",8)
        );

        //4
        new LeaderPiedistal(
                new Location(hubWorld,3.5,113.5,4.5),
                new Location(hubWorld,3,113,3),
                new Location(hubWorld,2,113,4),
                "F6CF",
                LeaderDescType.YOUTUBE,
                "",
                new LeaderScore<>("Общий счёт",12000),
                new LeaderScore<>("Убийств",13),
                new LeaderScore<>("Смертей",8)
        ).setYaw(145f);

        //3
        new LeaderPiedistal(
                new Location(hubWorld,5.5,113.5,-4.5),
                new Location(hubWorld,3,113,-5),
                new Location(hubWorld,5,113,-3),
                "F6CF",
                LeaderDescType.YOUTUBE,
                "",
                new LeaderScore<>("Общий счёт",12000),
                new LeaderScore<>("Убийств",13),
                new LeaderScore<>("Смертей",8)
        ).setYaw(45f);

        //2
        new LeaderPiedistal(
                new Location(hubWorld,-0.5,113.5,5.5),
                new Location(hubWorld,0,113,3),
                new Location(hubWorld,-2,113,3),
                "F6CF",
                LeaderDescType.YOUTUBE,
                "",
                new LeaderScore<>("Общий счёт",12000),
                new LeaderScore<>("Убийств",13),
                new LeaderScore<>("Смертей",8)
        ).setYaw(180f);

        //1
        new LeaderPiedistal(
                new Location(hubWorld,6.5,113.5,0.5),
                new Location(hubWorld,5,113,-1),
                new Location(hubWorld,5,113,1),
                "F6CF",
                LeaderDescType.YOUTUBE,
                "",
                new LeaderScore<>("Общий счёт",12000),
                new LeaderScore<>("Убийств",13),
                new LeaderScore<>("Смертей",8)
        ).setYaw(90f);
//        Location leader4=new Location(hubWorld,-1,113,-3);
//        Location leader3=new Location(hubWorld,5,113,-5);
//        Location leader2=new Location(hubWorld,-1,113,5);
//        Location leader1=new Location(hubWorld,6,113,0);
//
//        CitizensAPI.getNPCRegistry()
//                .createNPC(EntityType.PLAYER,"F6CF")
//                .spawn(leader1);
//        CitizensAPI.getNPCRegistry()
//                .createNPC(EntityType.PLAYER,"F6CF")
//                .spawn(leader2);
//        CitizensAPI.getNPCRegistry()
//                .createNPC(EntityType.PLAYER,"F6CF")
//                .spawn(leader3);
//        CitizensAPI.getNPCRegistry()
//                .createNPC(EntityType.PLAYER,"F6CF")
//                .spawn(leader4);
    }

    int advertStage=0;
    void writeAdvert(){
        Location advert1=spawnLocation.clone().add(35, 30, 0);
        Location advert2=spawnLocation.clone().add(40, 25, 0);
        switch (advertStage){
            case 0:
                lobbyTextBuilder.remove("advert1");
                lobbyTextBuilder.remove("advert2");
                lobbyTextBuilder.buildText("advert1", "Meteor MC", advert1, BlockFace.SOUTH, 87, (byte)0, UtilBlockText.TextAlign.CENTER);
                lobbyTextBuilder.buildText("advert2", "Уникальные миниигры!", advert2, BlockFace.SOUTH, 89, (byte)0, UtilBlockText.TextAlign.CENTER);
                break;
            case 1:
                lobbyTextBuilder.remove("advert1");
                lobbyTextBuilder.remove("advert2");
                lobbyTextBuilder.buildText("advert1", "Зови друзей!", advert1, BlockFace.SOUTH, 133, (byte)0, UtilBlockText.TextAlign.CENTER);
                lobbyTextBuilder.buildText("advert2", "Вместе веселей!", advert2, BlockFace.SOUTH, 201, (byte)0, UtilBlockText.TextAlign.CENTER);
                break;

            case 2:
                lobbyTextBuilder.remove("advert1");
                lobbyTextBuilder.remove("advert2");
                lobbyTextBuilder.buildText("advert1", "Никаких лагов!", advert1, BlockFace.SOUTH, 174, (byte)0, UtilBlockText.TextAlign.CENTER);
                lobbyTextBuilder.buildText("advert2", "Мощные сервера!", advert2, BlockFace.SOUTH, 169, (byte)0, UtilBlockText.TextAlign.CENTER);
                break;

            case 3:
                lobbyTextBuilder.remove("advert1");
                lobbyTextBuilder.remove("advert2");
                lobbyTextBuilder.buildText("advert1", "Поддержи нас!", advert1, BlockFace.SOUTH, 213, (byte)0, UtilBlockText.TextAlign.CENTER);
                lobbyTextBuilder.buildText("advert2", "meteor-mc.ru", advert2, BlockFace.SOUTH, 35, (byte)14, UtilBlockText.TextAlign.CENTER);
                break;

            default:
                advertStage=0;
                writeAdvert();
                return;
        }
        advertStage++;
    }

    void resetAll(){
        lobbyTextBuilder.remove();
        mapVoteManager.destroy();
        piedistalBuilder.destroy();
        scoreboard.reset();
    }

    void spawnPiedistals(){

        Location location=this.spawnLocation.clone().add(0, 0, 10);

        Integer piedistalLength=3;
        Integer piedistalInterval=3;

        Integer totalLength=piedistalLength*teamCount+piedistalInterval*(teamCount-1);

        Integer offset=totalLength/2;

        location.add(offset,0,0);

        location.add(-1,0,0);

        final EnumColor[] colors=mapInfo.getColors();

        for(Integer i=0;i<teamCount;i++){
            final EnumColor color=colors[i];
            final Location loc=location.clone();
            final Integer l=i;
            piedistalBuilder.buildTeamJoinPiedistal(
                    loc,
                    color,
                    null, //Todo: use set on map creation
                    new ClickableMob.MobClickEventHandler() {
                        @Override
                        public void onMobClick(ClickableMob.MobClickEvent event) {
                            joinCommand(event.getPlayer(),l);
                        }
                    },
                    EntityType.ARMOR_STAND //Todo: Should be declared in map
            );

            location=location.add(-(piedistalInterval+piedistalLength),0,0);
        }
    }

    Integer getPlayerCount(){
        Integer i=0;
        for (Player player : UtilServer.getPlayers()) {
            if(!isSpectator(player))
                i++;
        }
        return i;
    }

    void joinCommand(Player player,Integer id){

    }

    void displayScoreboard(Player player){
        player.setScoreboard(this.scoreboard.getScoreboard());
        /*if(!playerScoreboards.containsKey(player)) {
            player.kickPlayer("Internal error: Player does not have scoreboard!");
        }
        Scoreboard scoreboard=playerScoreboards.get(player);
        Objective objective;
        if(inHub()){
            objective=scoreboard.getObjective("lobby");
            objective.setDisplaySlot(DisplaySlot.SIDEBAR);
            objective.setDisplayName(C.cAqua+C.Bold+"МЕТЕОР");
            objective.getScore("Монетки").setScore(0);
            //objective.getScore(Double.toString(Math.random()*10000)).setScore(0);
        }else{
            objective=scoreboard.getObjective("game");
            objective.setDisplaySlot(DisplaySlot.SIDEBAR);
            objective.setDisplayName("Display Name");
        }
        player.setScoreboard(scoreboard);*/
    }

    void registerVoteManager(String mapsFolder){
        //Map voting
        this.mapsFolder = mapsFolder;
        this.mapFiles= new File(mapsFolder).listFiles();
        Collections.shuffle(Arrays.asList(this.mapFiles));
        this.mapVoteManager=new MapVoteManager(plugin,gameName,mapsFolder,mapFiles);
        this.pvpVoteManager=new PvpVoteManager(plugin);
    }

    void getExternalIp(){
        String externalIp;
        try {
            externalIp = UtilMisc.getExternalIp();
        }catch (IOException e){
            e.printStackTrace();
            System.exit(0);
            return;
        }

        System.out.println("Found external ip as: " + externalIp);
    }

    void cleanLobbyFromMobs(){
        for (Entity entity : this.spawnLocation.getWorld().getEntities()) {
            if(entity instanceof Player)
                continue;
            entity.remove();
        }
    }

    void addPlayer(Player player){
        UtilPlayer.sendCommandOutput(player,"МЕТЕОР","Просьба сообщать обо всех неполадках администраторам сервера!");
        player.teleport(spawnLocation.clone().add(0,19,0), PlayerTeleportEvent.TeleportCause.PLUGIN);
        UtilPlayer.sendResourcePack(player, EnumResourcePack.SKYWARS);
        if(isVotingEnabled())
            giveVoteItem(player);
        this.scoreboard.reset();
    }
    void respawn(Player player){
        if(inHub()) {
            player.setHealth(20);
            player.setExhaustion(20);
            player.setFoodLevel(20);
            player.teleport(spawnLocation, PlayerTeleportEvent.TeleportCause.PLUGIN);
        }
        if(isVotingEnabled())
            giveVoteItem(player);
    }
    void dead(Player player){
        if(inHub()){
            player.setHealth(20);
            player.setExhaustion(20);
            player.setFoodLevel(20);
            player.teleport(spawnLocation, PlayerTeleportEvent.TeleportCause.PLUGIN);
        }
    }

    public void makeSpectator(Player player){
        player.setHealth(20);
        player.setGameMode(GameMode.SPECTATOR);
        player.getInventory().clear();
        spectators.add(player);
    }
    boolean isSpectator(Player player){
        return spectators.contains(player);
    }
    void update(UpdateType type){
        switch (type){
            case FASTER:
                this.scoreboard.draw();
                for (Player player : Bukkit.getOnlinePlayers()) {
                    displayScoreboard(player);
                }
                this.scoreboard.updateTitle();
                break;
            case FASTEST:
                updateScoreboard(this.scoreboard);
                break;
            case TICK:
                stateUpdateCheck();
                break;
            case SLOW:
                writeAdvert();
                break;
        }
    }
    boolean allowInventoryChange(Player player){
        return player.getGameMode().equals(GameMode.CREATIVE);
    }
    boolean is1_8PVP(Player player){
        return isOldPvp;
    }
    void giveVoteItem(){
        this.mapVoteManager.giveItem();
        this.pvpVoteManager.giveItem();
    }
    void giveVoteItem(Player player){
        this.mapVoteManager.giveItem(player);
        this.pvpVoteManager.giveItem(player);
    }
    boolean inHub(){
        if(gameState.equals(GameState.WaitingForPlayers)||gameState.equals(GameState.Waiting))
            return true;
        return false;
    }
    boolean isVotingEnabled(){
        return this.gameState==GameState.Waiting;
    }
    boolean isHungerEnabled(Player player){
        if(inHub())
            return false;
        return true;
    }
    Integer getMinPlayers(){
        //FIXME: 1 only for debug
        return 1;
        //return this.teamCount;
    }
}
