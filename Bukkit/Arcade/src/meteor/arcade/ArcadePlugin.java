package meteor.arcade;

import meteor.util.Rank;
import meteor.util.chat.ChatManager;
import meteor.util.command.CommandManager;
import meteor.util.constants.F;
import meteor.util.events.AnyInventoryEvent;
import meteor.util.events.ArcadeResetEvent;
import meteor.util.events.PlayerAddedEvent;
import meteor.util.fixes.FixMemory;
import meteor.util.fixes.FixTablist;
import meteor.util.recharge.Recharge;
import meteor.util.update.UpdateEvent;
import meteor.util.update.UpdateType;
import meteor.util.update.Updater;
import meteor.util.utils.UtilCore;
import meteor.util.utils.UtilServer;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.player.*;
import org.bukkit.plugin.java.JavaPlugin;

public class ArcadePlugin extends JavaPlugin implements Listener {

    private static ArcadePlugin main;

    private ArcadeBase arcadeGame;
    private Recharge recharge;

    @Override
    public void onEnable() {
        ArcadePlugin.main=this;


        UtilCore.initialize(this);
        CommandManager.initialize(this);
        Recharge.initialize(this);
        ChatManager.initialize(this);
        FixMemory.initialize(this);
        FixTablist.initialize(this);
        Updater.initialize(this);

        //arcadeGame=new SkyWarsGame(this);
        //getServer().getPluginManager().registerEvents(arcadeGame,this);

        //Register this plugin as listener
        UtilCore.registerListener(this, false);

        //If restarted, add all players
        for (Player player : UtilCore.getPlayers()) {
            UtilCore.callEvent(new PlayerAddedEvent(player));
        }
    }

    @Override
    public void onDisable() {
        UtilCore.callEvent(new ArcadeResetEvent());
    }

    //Disable hunger in hub
    @EventHandler
    public void disableHunger(FoodLevelChangeEvent event){
        if(arcadeGame.isHungerEnabled((Player)event.getEntity()))
        event.setCancelled(true);
    }
    @EventHandler
    public void newPlayer(PlayerJoinEvent event){
        this.getServer().getPluginManager().callEvent(new PlayerAddedEvent(event.getPlayer()));
    }

    //All things to disable all inventory actions
    @EventHandler
    public void disableInventory(InventoryClickEvent event){
        AnyInventoryEvent invEvent=new AnyInventoryEvent((Player) event.getWhoClicked(),event.getInventory());
        this.getServer().getPluginManager().callEvent(invEvent);
        event.setCancelled(invEvent.isCancelled());
    }
    @EventHandler
    public void disableInventory(InventoryMoveItemEvent event){
        AnyInventoryEvent invEvent=new AnyInventoryEvent((Player)event.getSource().getHolder(),event.getSource().getHolder().getInventory());
        this.getServer().getPluginManager().callEvent(invEvent);
        event.setCancelled(invEvent.isCancelled());
    }
    @EventHandler
    public void disableInventory(PlayerDropItemEvent event){
        AnyInventoryEvent invEvent=new AnyInventoryEvent(event.getPlayer(),event.getPlayer().getInventory());
        this.getServer().getPluginManager().callEvent(invEvent);
        event.setCancelled(invEvent.isCancelled());
    }
    @EventHandler
    public void disableInventory(PlayerPickupItemEvent event){
        AnyInventoryEvent invEvent=new AnyInventoryEvent(event.getPlayer(),event.getPlayer().getInventory());
        this.getServer().getPluginManager().callEvent(invEvent);
        event.setCancelled(invEvent.isCancelled());
    }


    @EventHandler
    public void disableInventory(PlayerPickupArrowEvent event){
        AnyInventoryEvent invEvent=new AnyInventoryEvent(event.getPlayer(),event.getPlayer().getInventory());
        this.getServer().getPluginManager().callEvent(invEvent);
        event.setCancelled(invEvent.isCancelled());
    }



    @EventHandler
    public void disableInventory(PlayerSwapHandItemsEvent event){
        AnyInventoryEvent invEvent=new AnyInventoryEvent(event.getPlayer(),event.getPlayer().getInventory());
        this.getServer().getPluginManager().callEvent(invEvent);
        event.setCancelled(invEvent.isCancelled());
    }

    //Change join/leave messages
    @EventHandler
    public void changeMessage(PlayerQuitEvent event){
        Player player=event.getPlayer();
        if(arcadeGame.inHub())
            event.setQuitMessage(F.sys("Вышел", Rank.getColor(player) + player.getName()));
        else
            event.setQuitMessage(null);
    }
    @EventHandler
    public void changeMessage(PlayerJoinEvent event){
        Player player=event.getPlayer();
        if(arcadeGame.inHub())
            event.setJoinMessage(F.sys("Вошёл", Rank.getColor(player) + player.getName()));
        else
            event.setJoinMessage(null);
    }

    @EventHandler
    public void update(UpdateEvent event){
        arcadeGame.update(event.getType());
    }

    @EventHandler
    public void respawn(PlayerRespawnEvent event){
        arcadeGame.respawn(event.getPlayer());
    }

    @EventHandler
    public void death(PlayerDeathEvent event){
        arcadeGame.dead(event.getEntity());
    }

    //Old pvp
    @EventHandler
    public void disableNewPvp(UpdateEvent event){
        if(event.getType()!= UpdateType.SEC)
            return;
        for (Player player : UtilServer.getPlayers()) {
            boolean enable=arcadeGame.is1_8PVP(player);
            AttributeInstance attribute = player.getAttribute(Attribute.GENERIC_ATTACK_SPEED);
            double baseValue = attribute.getBaseValue();
            if(enable){
                if (baseValue == 1024) {
                    attribute.setBaseValue(4);
                    player.saveData();
                }
            }else{
                if (baseValue != 1024) {
                    attribute.setBaseValue(1024);
                    player.saveData();
                }
            }
        }
    }
}
