package meteor.arcade;// Written by Creeplays on 16.06.2016.

public enum GameState {
    WaitingForPlayers,
    Waiting,
    PreGame,
    Game
}
