package meteor.arcade.managers;// Written by Creeplays on 16.06.2016.

import meteor.util.userInterfaces.IUI;
import meteor.util.userInterfaces.UI;
import meteor.util.entity.utils.UtilPlayer;
import meteor.util.utils.UtilServer;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.util.HashSet;
import java.util.logging.Level;

public class PvpVoteManager {
    private UI pvpVoteUI;
    private IUI pvpVoteItem;
    private Integer[] votes=new Integer[]{0,0};
    private HashSet<String> votedPlayers=new HashSet<String>();

    public PvpVoteManager(Plugin plugin){
        pvpVoteUI =new UI("Голосование за режим PVP", 9, new UI.OptionClickEventHandler() {
            @Override
            public void onOptionClick(UI.OptionClickEvent event) {
                voteFor(event.getPlayer(),event.getPosition());
                updateVotes();
            }
        },plugin);
        pvpVoteItem =new IUI(plugin, new IUI.OptionClickEventHandler() {
            @Override
            public void onOptionClick(IUI.OptionClickEvent event) {
                Player player=event.getPlayer();
                if(votedPlayers.contains(player.getName()))
                    UtilPlayer.sendCommandOutput(player, "Голосование", "Ты уже проголосовал за режим!");
                else
                    pvpVoteUI.open(player);
            }
        },new ItemStack(Material.DIAMOND_AXE,1),"Голосование за режим PVP");

        updateVotes();
    }

    public void giveItem(){
        for (Player player : Bukkit.getOnlinePlayers()) {
            this.pvpVoteItem.giveToPlayer(player, 7);
        }
    }
    public void giveItem(Player player){
        this.pvpVoteItem.giveToPlayer(player, 7);
    }
    public void voteFor(Player player,Integer option){
        Integer mapNum=0;
        switch (option){
            case 2:
                mapNum=0;
                break;
            case 6:
                mapNum=1;
                break;
        }
        votedPlayers.add(player.getName());
        Bukkit.getLogger().log(Level.INFO,"Player "+player.getName()+" voted for "+mapNum+", map now have "+votes[mapNum]+" votes");
        votes[mapNum]++;
    }

    public void updateVotes(){
        this.pvpVoteUI.setOption(2, new ItemStack(Material.DIAMOND_SWORD, votes[0]),
                "Старая система PVP");
        this.pvpVoteUI.setOption(6, new ItemStack(Material.DIAMOND_AXE, votes[1]),
                "Новая система PVP");
        this.pvpVoteUI.update();
    }

    //Is old pvp win
    public boolean finishVoting(){
        for (Player player : UtilServer.getPlayers()) {
            this.pvpVoteItem.takeFromPlayer(player, 7);
        }
        return votes[0]<votes[1];
    }
}
