package meteor.arcade.managers;// Written by Creeplays on 16.06.2016.

import com.google.gson.Gson;
import meteor.cosmetics.util.ItemFactory;
import meteor.util.*;
import meteor.util.constants.C;
import meteor.util.constants.F;
import meteor.util.userInterfaces.IUI;
import meteor.util.userInterfaces.UI;
import meteor.util.utils.UtilFile;
import meteor.util.entity.utils.UtilPlayer;
import meteor.util.utils.UtilServer;
import net.minecraft.server.v1_10_R1.Tuple;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.TreeMap;
import java.util.logging.Level;

public class MapVoteManager {
    private UI mapVoteUI;
    private IUI mapVoteItem;
    private MapInfo[] mapInfos=new MapInfo[4];
    private Integer[] votes=new Integer[]{0,0,0,0};
    private HashSet<String> votedPlayers=new HashSet<String>();
    private File[] mapFiles;

    public MapVoteManager(Plugin plugin,String gameName, String mapsFolder, File[] mapFiles){
        this.mapFiles=mapFiles;

        mapVoteUI=new UI("Голосование за карту", 9, event -> {
            voteFor(event.getPlayer(),event.getPosition());
            updateVotes();
        },plugin);
        mapVoteItem=new IUI(plugin, event -> {
            Player player=event.getPlayer();
            if(votedPlayers.contains(player.getName()))
                UtilPlayer.sendCommandOutput(player, "Голосование", "Ты уже проголосовал за карту!");
            else
                mapVoteUI.open(player);
        },new ItemStack(Material.COMPASS,1),"Голосование за карту");

        fetchInfo();
        updateVotes();
    }

    public void giveItem(){
        for (Player player : Bukkit.getOnlinePlayers()) {
            this.mapVoteItem.giveToPlayer(player,0);
        }
    }
    public void giveItem(Player player){
        this.mapVoteItem.giveToPlayer(player,0);
    }
    public void voteFor(Player player,Integer option){
        Integer mapNum=0;
        switch (option){
            case 1:
                mapNum=0;
                break;
            case 3:
                mapNum=1;
                break;
            case 5:
                mapNum=2;
                break;
            case 7:
                mapNum=3;
                break;
        }
        votedPlayers.add(player.getName());
        Bukkit.getLogger().log(Level.INFO,"Player "+player.getName()+" voted for "+mapNum+", map now have "+votes[mapNum]+" votes");
        votes[mapNum]++;
    }
    public void fetchInfo(){
        for(Integer i=0;i<4;i++){
            File dataFile=new File(mapFiles[i].getPath()+"/INFO");
            Bukkit.getLogger().log(Level.INFO,"Loading info for map "+i+", path="+mapFiles[i].getPath());
            Gson gsonReader=new Gson();
            try {
                String fileContent= UtilFile.readFile(dataFile);
                Bukkit.getLogger().log(Level.INFO,fileContent);
                MapInfo mapInfo=gsonReader.fromJson(fileContent, MapInfo.class);
                Bukkit.getLogger().log(Level.INFO,"Adding info for "+mapInfo.name+","+mapInfo.author);
                this.mapInfos[i]=mapInfo;
            }catch (IOException e){
                e.printStackTrace();
            }
        }
        if(mapInfos[1]==null)
            throw new Error("Map info is not loaded!");
    }
    public void updateVotes(){
        if(mapInfos[0]==null)
            throw new Error("Map info is not loaded!");
        for(Integer i=0;i<4;i++){
            if(mapInfos[i]==null) {
                return;
            }
            this.mapVoteUI.setOption(1+i*2,
                    ItemFactory.create(
                            Material.DIAMOND,
                            (byte)0,
                            votes[i],
                            mapInfos[i].name,
                            "",
                            C.cGold+"Строитель: " + F.elem(mapInfos[i].author),
                            ""));
        }
        this.mapVoteUI.update();
    }
    public MapInfo[] getMapInfos(){
        return this.mapInfos;
    }
    public Integer[] getMapVotes(){
        return this.votes;
    }
    public TreeMap<Integer,MapInfo> getTop(){
        TreeMap<Integer,MapInfo> top=new TreeMap<>();
        for(Integer i=0;i<4;i++)
            top.put(votes[i],mapInfos[i]);
        return top;
    }
    public Integer getWinId(){
        Integer win=0;
        for(Integer i=0;i<4;i++)
            if(votes[win]<votes[i])
                win=i;
        return win;
    }
    public Tuple<MapInfo,File> finishVoting(){
        Integer winner=getWinId();
        for (Player player : UtilServer.getPlayers()) {
            this.mapVoteItem.takeFromPlayer(player,0);
        }
        return new Tuple<>(mapInfos[winner],mapFiles[winner]);
    }

    public void destroy(){
        votes=null;
        mapInfos=null;
        mapVoteUI.destroy();
        mapVoteItem.destroy();
    }
}
