package meteor.auth;

import meteor.cosmetics.util.ItemFactory;
import meteor.cosmetics.version.AAnvilGUI;
import meteor.util.account.AccountManager;
import meteor.util.account.Auth;
import meteor.util.chat.ChatManager;
import meteor.util.command.CommandManager;
import meteor.util.constants.C;
import meteor.util.constants.F;
import meteor.util.database.Database;
import meteor.util.entity.utils.UtilPlayer;
import meteor.util.recharge.Recharge;
import meteor.util.update.UpdateEvent;
import meteor.util.update.UpdateType;
import meteor.util.update.Updater;
import meteor.util.userInterfaces.AnvilGUI;
import meteor.util.userInterfaces.Title;
import meteor.util.userInterfaces.UI;
import meteor.util.userInterfaces.defaultUi.CountryUI;
import meteor.util.userInterfaces.defaultUi.PhoneUI;
import meteor.util.utils.UtilCore;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import static sun.audio.AudioPlayer.player;

/**
 * Copyright 2016 Meteor-MC
 * All rights reserved
 * Source code for internal use only
 * Created for meteor.auth
 * Created by Creeplays on 02.07.2016.
 */
public class AuthPlugin extends JavaPlugin implements Listener {

    @Override
    public void onEnable() {
        UtilCore.initialize(this);
        CommandManager.initialize(this);
        Recharge.initialize(this);
        ChatManager.initialize(this);
        AccountManager.initialize(this);
        AuthManager.initialize(this);
        Updater.initialize(this);

        CommandManager.instance.setBlockAll(true);

    }

    @Override
    public void onDisable() {
    }
}
