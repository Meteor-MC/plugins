package meteor.auth;

import meteor.cosmetics.util.ItemFactory;
import meteor.cosmetics.version.AAnvilGUI;
import meteor.util.FireworkFactory;
import meteor.util.MiniPlugin;
import meteor.util.account.Auth;
import meteor.util.constants.C;
import meteor.util.constants.F;
import meteor.util.entity.utils.UtilPlayer;
import meteor.util.update.UpdateEvent;
import meteor.util.update.UpdateType;
import meteor.util.userInterfaces.AnvilGUI;
import meteor.util.userInterfaces.Title;
import meteor.util.userInterfaces.UI;
import meteor.util.userInterfaces.defaultUi.CountryUI;
import meteor.util.userInterfaces.defaultUi.PhoneUI;
import meteor.util.userInterfaces.enums.EnumCountry;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

/**
 * Copyright 2016 Meteor-MC
 * All rights reserved
 * Source code for internal use only
 * Created for meteor.auth
 * Created by Creeplays on 05.07.2016.
 */
public class AuthManager extends MiniPlugin {
    public static AuthManager instance;

    public static void initialize(Plugin plugin) {
        instance = new AuthManager(plugin);
    }

    private AuthManager(Plugin plugin) {
        super("Авторизация", plugin);
    }

    @Override
    public void enable() {

    }

    @Override
    public void disable() {

    }

    @Override
    public void addCommands() {

    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        player.getInventory().clear();
        player.setGameMode(GameMode.SPECTATOR);
        player.setWalkSpeed(0f);
        player.setFlySpeed(0f);
        player.setFlying(true);
        player.setLevel(1000);

        player.sendMessage(F.main("Авторизация", "Антибот проверка..."));
        new Title(C.cDPurple + "Антибот", C.cDGray + "Проверка...").send(player);
        Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), () -> {
            new Title(C.cDPurple + "Антибот", C.cDGray + "Проверка выполнена!").send(player);
            Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), () -> {
                Auth.isRegistered(player.getName(), registered -> {
                    new Title(C.cDGreen + "Привет!", "").send(player);
                    Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), () -> {
                        if (registered) {
                            new Title(C.cDGreen + "Регистрация", C.cGreen + "Ты уже зарегистрирован!").send(player);
                            Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), () -> {
                                displayMainAuthUi(player);
                            }, 20 * 3);
                        } else {
                            new Title(C.cDGreen + "Привет!", C.cGreen + "Т.к ты тут впервые, тебе нужно зарегистрироваться!").send(player);
                            regStage1(player);
                        }
                    }, 20 * 3);
                });
            }, 20 * 5);
        }, 20 * 3);
    }

    public void regStage1(Player player) {
        Auth.insertIfNotExists(player.getName(), inserted -> {
            Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), () -> {
                if (inserted) {
                    new Title(C.cDGreen + "Регистрация", C.cGreen + "Выбери свою страну").send(player);
                    Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), () -> {
                        CountryUI countryUI = new CountryUI(getPlugin(), player, response -> {
                            if (response.isAccepted()) {
                                regStage2(player, response.getCountry());
                            } else {
                                new Title(C.cDGreen + "Регистрация", C.cDRed + "Выбор страны необходим!").send(player);
                                regStage1(player);
                            }
                        });
                        countryUI.open();
                    }, 20 * 3);
                } else {
                    new Title(C.cDGreen + "Регистрация", C.cRed + "Произошла ошибка!").send(player);
                    Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), () -> {
                        player.kickPlayer(C.cRed + "Ошибка! AuthManager.java:97 : document==null");
                    }, 20 * 3);
                }
            }, 20 * 3);
        });
    }

    public void regStage2(Player player, EnumCountry country) {
        Auth.setCountry(player.getName(), country, success -> {
            if (success) {
                new Title(C.cDGreen + "Регистрация", C.cGreen + "Теперь номер телефона (Это можно пропустить)").send(player);
                Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), () -> {
                    PhoneUI phoneUI = new PhoneUI(player, getPlugin(), "Ввод номера", country, event -> {
                        if(event.isAccepted()){
                            regStage3(player,event.getPhone());
                        }else{
                            regStage3(player,null);
                        }
                    });
                    phoneUI.open();
                }, 20 * 3);
            } else {
                new Title(C.cDGreen + "Регистрация", C.cRed + "Произошла ошибка!").send(player);
                Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), () -> {
                    player.kickPlayer(C.cRed + "Ошибка! AuthManager.java:122 : document.country==null");
                }, 20 * 3);
            }
        });
    }

    public void regStage3(Player player,String phone){
        Auth.setPhone(player.getName(),phone,success->{
            if(success){
                new Title(C.cDGreen + "Регистрация", C.cGreen + "Остался пароль!").send(player);
                Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), () -> {
                    AnvilGUI anvilGUI=new AnvilGUI(player,event -> {
                        if(event.getSlot()== AAnvilGUI.AnvilSlot.OUTPUT){
                            String password=event.getName();
                            if(password.length()>5){
                                regStage4(player,password);
                            }else{
                                new Title(C.cDGreen + "Регистрация", C.cDRed + "Пароль должен быть длиннее 5 символов!").send(player);
                                regStage3(player,phone);
                            }
                        }
                    });
                    anvilGUI.setSlot(AAnvilGUI.AnvilSlot.INPUT_LEFT,ItemFactory.create(Material.PAPER,(byte)0,"Пароль"));
                    anvilGUI.open();
                }, 20*3);
            }else{
                new Title(C.cDGreen + "Регистрация", C.cRed + "Произошла ошибка!").send(player);
                Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), () -> {
                    player.kickPlayer(C.cRed + "Ошибка! AuthManager.java:146 : document.phone==null");
                }, 20 * 3);
            }
        });
    }

    public void regStage4(Player player,String password){
        Auth.setPassword(player.getName(),password,success->{
            if(success){
                new Title(C.cDGreen + "Регистрация",C.cGreen+"Регистрация завершена!").send(player);
                Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), () -> {
                    new Title(C.cDGreen + "Регистрация",C.cGreen+"Теперь войди на сервер").send(player);
                    Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), () -> {
                        displayMainAuthUi(player);
                    },20*3);
                }, 20 * 3);
            }else{
                new Title(C.cDGreen + "Регистрация", C.cRed + "Произошла ошибка!").send(player);
                Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), () -> {
                    player.kickPlayer(C.cRed + "Ошибка! AuthManager.java:174 : document.password==null");
                }, 20 * 3);
            }
        });
    }

    public void displayMainAuthUi(Player player) {
        player.sendMessage(F.main("Авторизация", "Загрузка главного меню"));
        Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), () -> {
            //UtilPlayer.fakeBorder(player, login.getBlockX(), login.getBlockZ(), 100, 0);

            ItemStack passwordItem = ItemFactory.create(Material.PAPER, (byte) 0, "Пароль",
                    "",
                    C.cBlue + "          METEOR-MC  AUTH          ",
                    C.cRed + "       ======= " + C.cDBlue +
                            "v0.13" + C.cRed +
                            " =======       ",
                    C.cDPurple + "              BY F6CF              ",
                    "",
                    C.cGold + " Клик для просмотра дополнительных ",
                    C.cGold + "возможностей  (напр. восст. пароля)",
                    "");

            AnvilGUI loginUI = new AnvilGUI(player, clickEvent -> {
                if (clickEvent.getSlot() == null) {
                    clickEvent.setWillClose(false);
                    clickEvent.setWillDestroy(false);
                    return;
                }
                clickEvent.setWillClose(true);
                clickEvent.setWillDestroy(true);
                switch (clickEvent.getSlot()) {
                    case INPUT_LEFT:
                        displaySecondaryUi(player);
                        return;
                    case INPUT_RIGHT:
                        displayMainAuthUi(player);
                        return;
                    case OUTPUT:
                        if (clickEvent.getName().equals("#develop")) {
                            player.setWalkSpeed(0.5f);
                            player.getPlayer().setFlySpeed(0.5f);
                        } else
                            processLogin(player, clickEvent.getName());
                }
            });
            loginUI.setSlot(AAnvilGUI.AnvilSlot.INPUT_LEFT, passwordItem);
            loginUI.open();
        }, 5);
    }

    public void processLogin(Player player, String password) {
        player.sendMessage(F.main("Авторизация", "Выполняется вход"));

        Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), () -> {
            Auth.auth(player.getName(), password, ok -> {
                if (!ok) {
                    player.playSound(player.getLocation(), Sound.ENTITY_ENDERDRAGON_DEATH, 5, 5);
                    new Title(C.cRed + "Неверный пароль!").send(player);
                    Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), () -> {
                        displayMainAuthUi(player);
                    }, 5 * 20);
                } else {
                    new Title(C.cGreen + "Вход выполнен!").send(player);
                    Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), () -> {
                        Auth.isMigrated(player.getName(), migrated -> {
                            if (migrated) {
                                new Title(C.cDRed+"Хм...",C.cDPurple+"Так ты ещё со старого сервера?").send(player);
                                Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), () -> {
                                    new Title(C.cDGreen+"С возвращением!","").send(player);
                                    Bukkit.getScheduler().runTask(getPlugin(),()->{
                                        FireworkFactory.spawn(player.getLocation(), FireworkEffect.builder().with(FireworkEffect.Type.BALL_LARGE).withFlicker().withTrail().withColor(Color.ORANGE).build(),player);
                                    });
                                    Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), () -> {
                                        new Title(C.cDGreen+"Тут многое поменялось","").send(player);
                                        Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), () -> {
                                            new Title(C.cDGreen+"Тебе надо",C.cDGreen+"повторно зарегистрироваться").send(player);
                                            regStage1(player);
                                        },20*3);
                                    },20*3);
                                },20*3);
                            }else{
                                Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), () -> {
                                    finishLogin(player);
                                },5);
                            }
                        });
                    },20*3);
                }
            });
        }, 5);
    }
    public void finishLogin(Player player){
        for (int i = 0; i < 5; i++) {
            String out="";
            for (int i1 = 0; i1 < 5 - i; i1++) {
                out+="▌";
            }
            final String o=out;
            final int fi=i;
            Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), () -> {
                new Title(C.cAqua+o+" "+C.cDGreen+(5-fi)+" "+C.cAqua+o).send(player);
            },20*i);
        }
    }

    @EventHandler
    public void update(UpdateEvent event) {
        if (event.getType() != UpdateType.SEC) {
            Bukkit.getOnlinePlayers().forEach(player -> {
                if (player.getOpenInventory() == null) {
                    displayMainAuthUi(player);
                }
            });
        }
    }

    public void displaySecondaryUi(Player player) {
        Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), () -> {
            UI ui = new UI("Дополнительные возможности", 27, event -> {
                switch (event.getPosition()) {
                    case 10:
                        displayRecoveryUi(event.getPlayer());
                        return;
                    case 16:
                        displayMainAuthUi(event.getPlayer());
                        return;
                    default:
                        event.setWillDestroy(false);
                        event.setWillClose(false);
                }
            }, getPlugin(), player);
            ui.setOption(10, ItemFactory.create(Material.BOOK, (byte) 0, C.cGreen + "Восстановление пароля",
                    "",
                    C.cGold + "Если при регистрации ты",
                    C.cGold + "указал свой номер телефона,",
                    C.cGold + "эта кнопка сгенерирует",
                    C.cGold + "временный пароль для входа",
                    C.cGold + "на сервер и отправит его тебе",
                    C.cGold + "в СМС",
                    ""));
            ui.setOption(16, ItemFactory.create(Material.REDSTONE_BLOCK, (byte) 0, C.cRed + "В главное меню",
                    "",
                    "Клик для возврата в главное меню",
                    ""));
            ui.open();
        }, 5);
    }

    public void displayRecoveryUi(Player player) {
        //player.sendMessage(F.main("Авторизация", "Загрузка меню восстановления пароля"));
        Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), () -> {
            CountryUI countryUI = new CountryUI(getPlugin(), player, event -> {
                if (!event.isAccepted()) {
                    displaySecondaryUi(player);
                    //player.sendMessage(F.main("Авторизация", "Восстановление отменено"));
                } else {
                    Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), () -> {
                        PhoneUI phoneUI = new PhoneUI(player, getPlugin(), "", event.getCountry(), event1 -> {
                            if (event1.isAccepted()) {
                                player.sendMessage(F.main("Авторизация", "Если номер " + F.elem("+" + event1.getPhone()) + " действительно твой, то на него сейчас придёт новый пароль."));
                                displayMainAuthUi(player);
                            } else {
                                Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), () -> {
                                    displayRecoveryUi(player);
                                }, 5);
                            }
                        }, "", C.cGold + "На этот номер будет отправлен временный пароль для входа на сервер", "");
                        phoneUI.open();
                    }, 5);
                }
            });
            countryUI.open();
        }, 5);
    }
}
