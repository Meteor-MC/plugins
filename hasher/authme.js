var password = "Kilbot123";
var bruteForce = require("node-bruteforce");

function sha256(pwd) {
  return require("crypto").createHash('sha256').update(pwd).digest('hex');
}

var salt = "36cec68d29f870f4";
var toBrute =
  "$SHA$36cec68d29f870f4$c92edace9657ea5d342663354064e5ae39b805a30913848dd4d84cd78c62d177";

var tryed = 0;
var hz,
  period,
  startTime = new Date,
  runs = 0;

bruteForce("Kilbot123".split(''), function(value) {
  runs++;

  if (runs % 10000 == 0 && value.length > 3) {
    totalTime = new Date - startTime;
    totalTime /= 1000;
    period = totalTime / runs;
    hz = 1 / period;
    console.log(runs, hz);
    console.log(value.toString());
  }


  //if (value.length < 9)
  //return false;
  if (("$SHA$" + salt + "$" + sha256(sha256(value) + salt)) == toBrute) {
    console.log(value);
    return true;
  }
  //console.log("Wrong: " + value);
  return false;
});


//var encryptedPassword = "$SHA$" + salt + "$" + sha256(sha256(password) + salt);
//console.log(encryptedPassword);
