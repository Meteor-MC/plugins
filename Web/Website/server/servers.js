/**
 * Created by Creeplays on 05.07.2016.
 */
var redisCollection = new Meteor.RedisCollection("redis");
redisCollection.set("site.work",true);

Meteor.publish("server", function () {
    return redisCollection.matching("server*");
});

redisCollection.set("server:mg1:players","1");

redisCollection.allow({
    exec: function (userId, command, args) {
        if (_.contains(['set', 'setex', 'del'], command))
            return false;
        return true;
    }
});