#Jenkins created successfully.  Please make note of these credentials:
#
#   User: admin
#   Password: 67lyUUEPC2En
#
#Note:  You can change your password at: https://jenkins-creepworks.rhcloud.com/me/configure

export UNAME=`uname`
if [[ "$UNAME" == 'MINGW32_NT-10.0-WOW' ]]; then
   echo "Dev env, configuring path"
   export PATH=$PATH:~/.meteor
   alias meteor="meteor.bat"
else
   echo "Unknown env!"
   exit
fi

echo "Configuring redis"
#Redis specific
export REDIS_CONFIGURE_KEYSPACE_NOTIFICATIONS="1"
#Server
export REDIS_HOST="169.50.100.13"
export REDIS_PORT="6379"
#Auth data
export REDIS_USER="f6cf"
export REDIS_PASSWORD="UwndaKAwieNseUEajISlwNdgWUrb2asdw4tgs54u29"
#All in one
export REDIS_URL="redis://$REDIS_USER:$REDIS_PASSWORD@$REDIS_HOST:$REDIS_PORT"

echo "Configuring mongo"
#Mongo specific
export MONGO_DB="db2"
#Server
export MONGO_HOST="ds028679.mlab.com"
export MONGO_PORT="28679"
#Auth data
export MONGO_USER="meteor"
export MONGO_PASSWORD="Kilbot123"
#All in one
export MONGO_URL="mongodb://$MONGO_USER:$MONGO_PASSWORD@$MONGO_HOST:$MONGO_PORT/$MONGO_DB"
